<?php
$html_title = '奧羅拉後台管理系統';
$html_copyright = '- Copyright © 2014 Aurora -';
$html_resource =    '<link rel="stylesheet" type="text/css" href="backend/css/template.css">'.
                    '<link rel="stylesheet" type="text/css" href="backend/js/jquery-te/jquery-te-1.4.0.css">'.
                    '<link rel="shortcut icon" type="image/png" href="backend/img/favicon.png"/>'.
                    '<script type="text/javascript" src="backend/js/jquery-1.11.1.min.js"></script>'.
                    '<script type="text/javascript" src="backend/js/jquery-ui.effect.min.js"></script>'.
                    '<script type="text/javascript" src="backend/js/jquery-te/jquery-te-1.4.0.min.js"></script>'.
                    '<script type="text/javascript" src="backend/js/template.js"></script>'.
					'<script type="text/javascript" src="backend/js/jac.validate.js"></script>'.
					'<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>';
