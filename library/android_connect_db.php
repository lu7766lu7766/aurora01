<?php
	set_time_limit(0);
	include 'dba.php';
 $dba = new dba();
 $imghost="http://app.55104.com.tw:8080/images/";

 foreach($_POST as $key=>$val)
 	if($val!='')
 		$_POST[$key]=clean($val);
 
	//$sql = $_POST['query_string'];//$_POST['query_string'];//"SELECT distinct fv_city FROM t_area"
	$table	= $_POST['table']==""?"type":$_POST['table'];
	
	$type	= $_POST['type']==""?"1":$_POST['type'];
	
	$brand	= $_POST['brand'];
	$city	= $_POST['city'];
	$dist	= $_POST['dist'];
	$area	= $city.$dist;//目標查詢位址
	$lat	= $_POST['lat']==""?0:$_POST['lat'];//使用者目前緯度
	$lng	= $_POST['lng']==""?0:$_POST['lng'];//使用者目前經度
	
	$page	= $_POST['page']==""?"1":$_POST['page'];//頁數
	$one_len = 15;//一頁顯示比數
	$show_num = ($page-1)*$one_len;//目前已顯示幾筆
	//緯度1度 = 大约111km
	$multiplier = 111000;
	
	//echo $table."^^".$type."^^".$brand."^^".$city."^^".$dist."^^".$lat."^^".$lng."^^".$page;
	
	switch($table){
		case 'type':
			$sql = "select fi_id,fv_name,fv_logo 
					from t_store_type 
					where fi_delete=0 and fi_active<>0 
					order by fi_weights desc";
			echo json_encode($dba->getAll($sql));
			break;

		case 'brand':
			$sql = "select fi_id,fv_brand_name 
					from t_brand 
					where fi_delete=0 and fi_active<>0 and fi_type='$type'
					order by fi_weights desc";
			echo json_encode($dba->getAll($sql));
			break;

		case 'city':
			$sql = "SELECT distinct fv_city FROM t_area";
			echo json_encode($dba->getAll($sql));
			break;

		case 'dist':
			$sql = "SELECT fv_dist FROM t_area where fv_city='$city'";
			echo json_encode($dba->getAll($sql));
			break;

		case 'store':
			//fi_id,fv_brand_name,fv_subname,fv_phone,fv_address,ff_latitude,ff_longitude,fv_logo
			$where_val = "";
			if($brand!=""){
				$where_val.=is_numeric($brand)?" and fi_brand='$brand' ":" and fv_brand_name='$brand' ";
			}
			if($area==""){
				if( $lat=="" || $lng=="" )die(json_encode(array()));
				$sql ="SELECT
					fi_id
					,fv_subname
					,fv_logo
					,fv_menu
					,fv_address
					,fv_brand_name
					,fv_phone
					,fi_level
					,fi_special_price
					,fv_special_info
					,(GLength(
						LineStringFromWKB(
							LineString(
								fp_geo,GeomFromText('POINT({$lat} {$lng})')
							)
						)
					)) * {$multiplier} AS distance
					FROM v_store
					where fi_delete='0' and fi_type='$type' and ff_latitude>0 and ff_longitude>0 $where_val
					ORDER BY distance ASC limit $show_num,$one_len";
			}else{
				$area2 = strtr($area,array("臺"=>"台"));
				$sql = "select fi_id
							 ,fv_subname
							 ,fv_logo
							 ,fv_menu
							 ,fv_address
							 ,fv_brand_name
							 ,fv_phone
							 ,fi_level
							 ,fi_special_price
							 ,fv_special_info
							 ,0 AS distance
					from v_store
					where fi_delete='0' and fi_type='$type' and (fv_address like '%$area%' or fv_address like '%$area2%') $where_val
					order by fi_level desc, fi_like desc, fi_dial desc, rand()
					limit $show_num,$one_len";
			}
			//echo "<pre>";
			//print_r($dba->getAll($sql));
			echo json_encode($dba->getAll($sql));
			break;

		case 'applyChk':
			//檢查電話是否存在
			$phone = $_POST['phone'];
			$apply_over = 600;//十分鐘內註冊
			$sql = "select 1 from t_history 
						where 
						fv_member_phone='$phone' and 
						fi_store_id='-1' and 
						timestampdiff(SECOND,fdt_start_time,now())<$apply_over limit 1;";
			$result = $dba->getAll($sql);
			if(count($result)>0){
				$result = $dba->getAll("select 1 from t_member where fv_phone='$phone' limit 1;");
				if(count($result)>0){
					echo json_encode( array("result"=>"member_exists") );
				}else{
					echo json_encode( array("result"=>"success") );
				}
			}else{
				echo json_encode( array("result"=>"unapply") );
			}
			break;

		case 'memberChk':
			//檢查電話是否存在
			$phone = $_POST['phone'];
			$result = $dba->getAll("select 1 from t_member where fv_phone='$phone' limit 1;");
			if(count($result)>0){
				echo json_encode( array("result"=>"member_exists") );
			}else{
				echo json_encode( array("result"=>"success") );
			}
			break;

		case 'memberJoin':
			//$apply_phone='55104#1';
			$phone = $_POST['phone'];

			if($phone==""){echo json_encode( array("result"=>"phone_is_empty") );break;}
			//檢查驗證電話
			$apply_result = $dba->getAll("select 1 from t_history where fv_member_phone = '$phone' and fi_store_id='-1';");
			if(count($apply_result)==0){
				echo json_encode( array("result"=>"unapply") );
				break;
			}

			$member_result = $dba->getAll("select 1 from t_member where fv_member_phone = '$phone' and fi_delete<>'0';");
			if(count($member_result)==0){
				echo json_encode( array("result"=>"member_exists") );
				break;
			}

			$passwd = $_POST['passwd'];
			$email = $_POST['email'];
			$promote_code = $_POST['introducer_phone'];

			$dt = date("Y-m-d H:i:s",time());
			$sql = "select fv_promotecode from t_member where fv_promotecode<>'55104' order by fv_promotecode desc limit 1;";
		 $code_result = $dba->getAll($sql);
		 $code = $code_result[0]["fv_promotecode"];
		 $code++;
		 if(substr($code,-4)=='0000'){$code++;}
 	
			$sql = "insert into t_member (
									fv_phone
									,fdt_create_time
									,fi_introducer
									,fv_email
									,fv_passwd
									,fv_promotecode
									) 
							values ( 
									'$phone'
									,'$dt'
									,'$intro_id'
									,'$email'
									,md5(md5('$passwd')) 
									,'$code'
									);";
			$dba->query($sql);
			$dba->query("insert into t_bonus_log (fi_type,fv_phone,fi_bonus,ft_time,fv_memo) values('1','$phone','10',now(),'加入會員');");
 
			intro_add($phone,$promote_code);

 //echo $sql;
			echo json_encode( array("result"=>"success") );
			break;
		
		case 'introducerAdd':
			$phone = $_POST['phone'];

			if($phone==""){echo json_encode( array("result"=>"phone_is_empty") );}

			$promote_code = $_POST['introducer_phone'];

			echo intro_add($phone,$promote_code);
			
			break;

		case 'uploadStore':
			$intro_phone = $_POST['intro_phone'];
			$store_phone = $_POST['store_phone'];
			$subname = $_POST['subname'];
			$brand = $_POST['brand'];
			$address = $_POST['address'];
			if($intro_phone=="" || $store_phone=="" || $brand=="" || $address==""){
				echo json_encode( array("result"=>"data_incomplete") );
				break;
			}
			$sql = "insert into t_store_pending 
								(fv_brand_name,fv_subname,fv_address,fv_store_phone,fv_intro_phone)
						values ('$brand','$subname','$address','$store_phone','$intro_phone');";
			$result = $dba->query($sql);
			if(!$result){
				echo json_encode( array("result"=>"upload_error") );
				//echo $sql;
			}else{
				echo json_encode( array("result"=>'success') );
			};
			break;

		case 'getMemberInfo':
			$phone = $_POST['phone'];
			$me = $dba->getAll("select count(1) from t_member where fv_phone='$phone' limit 1;");
			if(count($me)>0){
 //$id = $me[0]['fi_id'];
				$sql = "select fv_promotecode
							 ,(select count(1) from t_history where fv_member_phone='$phone' and fi_store_id>0 ) as fi_deal
							 ,(select count(1) from t_member where fi_introducer=a.fi_id) as fi_intro
							 ,fi_bonus 
							 ,(select fv_promotecode from t_member where fi_id=a.fi_introducer limit 1) as fv_intro_phone
						from t_member as a where fv_phone = '$phone' limit 1;";
				$result = $dba->getAll($sql);
				echo json_encode($result[0]);
			}
			break;

		case 'favoriteJoin':
			$phone = $_POST["phone"];
			$store_id = $_POST["store_id"];

			$result = $dba->getAll("select 1 from t_favorite where fv_phone='$phone' and fi_store_id='$store_id';");
			if(count($result)>0){
				echo json_encode( array("result"=>"already_exists") );
			}else{
				$result = $dba->query("insert into t_favorite (fv_phone,fi_store_id) values ('$phone','$store_id' );");
				if(!$result){
					echo json_encode( array("result"=>"get_error") );
				}else{
					echo json_encode( array("result"=>"success") );
				}
			}

			break;

		case 'favoriteRemove':
			$phone = $_POST["phone"];
			$store_id = $_POST["store_id"];
			$result = $dba->query("delete from t_favorite where fv_phone='$phone' and fi_store_id='$store_id';");
			if(!$result){
				echo json_encode( array("result"=>"remove_error") );
			}else{
				echo json_encode( array("result"=>"success") );
			}

			break;

		case 'getFavorite':
			$phone = $_POST["phone"];
			$sql = "select b.fi_id
						 ,b.fv_subname
						 ,b.fv_logo
						 ,b.fv_menu
						 ,b.fv_address
						 ,b.fv_brand_name
						 ,b.fv_phone
						 ,b.fi_level
						 ,b.fi_special_price
						 ,fv_special_info
						 ,0 AS distance
					from t_favorite as a
					left join v_store as b on a.fi_store_id=b.fi_id
					where a.fv_phone='$phone' and 
						b.fi_id is not null
					limit $show_num,$one_len";
			$result = $dba->getAll($sql);
			if(!is_array($result)){
				echo json_encode( array("result"=>"get_error") );
			}else{
				echo json_encode( $result );
			}

			break;

		case 'getPromote':
			$type = $_POST["type"];
			$where = $type==""?"":" and fi_type='$type' ";
			//$limit = $page=="0"?"":"limit $show_num,$one_len";
			$sql = "select fi_id,fv_name,fv_describe,fv_logo,fi_bonus,fv_activity_picture
					from t_promote
					where fi_delete<>1 
					and fi_active = 1
					and (fi_date_active=0 or (fi_date_active=1 and CURDATE() between fd_start_date and fd_end_date))
					$where
					order by fi_type,fi_weights desc
					$limit ;";
			$result = $dba->getAll($sql);
			if(!is_array($result)){
				echo json_encode( array("result"=>"get_error","sql"=>$sql) );
			}else{
				echo json_encode( $result );
			}

			break;
		
		case 'getAdvertisement':
			//type 0 半版, 1 選單
			$address = $_POST['address'];
			$address = strtr($address,array("台"=>"臺"));
			$area_result = $dba->getAll("SELECT * FROM t_area");
			foreach ($area_result as $area) {
				$city = $area["fv_city"];
				$dist = $area["fv_dist"];
				if( strpos($address, $city.$dist)!==false || $address==$city ){
					break;
				}
			}
			$sql = "select fi_id
							,fv_ad 
							,fv_describe
							,fv_url
							,fv_area_list
							,case 
								when fi_brand=0 then fv_brand_name
								when fi_brand<>0 then fv_sbrand_name
								end as fv_brand_name
							,fv_phone
						from v_advertisement
						where fi_delete='0'
							and fi_active='1'
							and ( fi_date_active='0' or (fi_date_active='1' and fd_start_date<=CURDATE() and fd_end_date>=CURDATE()) )
							and fi_type='$type' 
							and ( fv_area_list like '%{$city},%' || fv_area_list like '%{$city}{$dist},%' )
						order by rand()";
			$result = $dba->getAll($sql);
			if(is_array($result)){
				echo json_encode( $result );
				//echo json_encode( array("result"=>$sql) );
			}else{
				echo json_encode( array("result"=>"error") );
			}

			break;

		case 'favoriteChk':
			//檢查電話是否存在
			$phone = $_POST['phone'];
			$store_id = $_POST['store_id'];
			$result = $dba->getAll("select 1 from t_favorite where fv_phone='$phone' and fi_store_id='$store_id' limit 1;");
			if(count($result)>0){
				echo json_encode( array("result"=>"1") );
			}else{
				echo json_encode( array("result"=>"0") );
			}
			break;

		case 'getNews4Cell':
			$sql = "select fi_id,CONCAT('$imghost','news4cell/',fv_images) as fv_images
					from t_news4cell
					where fi_delete<>1 
					and fi_active = 1
					and (fi_date_active=0 or (fi_date_active=1 and CURDATE() between fd_start and fd_end));";
			$result = $dba->getAll($sql);
			if(!is_array($result)){
				echo json_encode( array("result"=>"get_error") );
			}else{
				echo json_encode( $result );
			}

			break;

		case 'chkAndroidVersion':
			$phoneVersion = $_POST["version"];

			$allurl = "https://play.google.com/store/apps/details?id=mac.jacwang.aurora20150610&hl=zh_TW";

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $allurl);
			curl_setopt($ch, CURLOPT_HEADER, 1);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$all = curl_exec($ch);
			curl_close($ch);

			// echo $all;
			preg_match('|data-dropdown-value="(.*)".*最新版本|sU',$all, $match);
			$all = end(explode("data-dropdown-value",$match[0]));
			preg_match('/"(.*)"/sU',$all, $match);
			$onlineVersion = $match[1];

			if($onlineVersion>$phoneVersion){
				echo json_encode( array("result"=>"success") );
			}
			//
			break;

		case 'chkIosVersion':
			$phoneVersion = $_POST["version"];

			$allurl = "https://itunes.apple.com/tw/app/55104cha-hao-tai/id998083729?l=zh&mt=8";

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $allurl);
			curl_setopt($ch, CURLOPT_HEADER, 1);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$all = curl_exec($ch);
			curl_close($ch);


			preg_match('/<span itemprop="softwareVersion">(.*)<\/span>/U',$all, $match);
			$onlineVersion = $match[1];

			if($onlineVersion>$phoneVersion){
				echo json_encode( array("result"=>"success") );
			}
			//
			break;

        case 'setRegId':
            $phone = $_POST["phone"];
            $reg_id = $_POST["reg_id"];
            $result = $dba->query("update t_member set fv_reg_id='$reg_id' where fv_phone='$phone';");
            if(!$result){
                echo json_encode( array("result"=>"error") );
            }else{
                echo json_encode( array("result"=>"success") );
            }
            break;
	}

	function clean($data) {//防止隱碼
		if(!get_magic_quotes_gpc()) {$data = addslashes($data);}
		$data = str_replace("_", "\_", $data);
		$data = str_replace("%", "\%", $data);
		$data = str_replace("null", "", $data);
		$data = nl2br((string)$data);
		$data = htmlspecialchars($data);
		return $data;
	}

	function intro_add($phone,$promote_code){
		global $dba;
		if($promote_code!=""){
			$introducer = $dba->getAll("select fi_id,fv_phone from t_member where fv_promotecode='$promote_code' or fv_phone='$promote_code' limit 1;");
			if(count($introducer)>0){
                 $intro_id = $introducer[0]['fi_id'];
                 $intro_phone = $introducer[0]['fv_phone'];
                 if($intro_phone == $phone){
                return json_encode( array("result"=>"intro_is_youself") );
            }
                $dba->query("insert into t_bonus_log (fi_type,fv_phone,fi_bonus,ft_time,fv_memo) values('1','$intro_phone','10',now(),'介紹 {$phone} 會員');");
                $dba->query("insert into t_bonus_log (fi_type,fv_phone,fi_bonus,ft_time,fv_memo) values('1','$phone','10',now(),'推薦 {$promote_code} 會員');");
                $dba->query("update t_member set fi_introducer='$intro_id' where fv_phone='$phone';");
                return json_encode( array("result"=>"success","add_bonus"=>"10") );
            }else{
                return json_encode( array("result"=>"intro_not_exists") );
            }
		}else{
			return json_encode( array("result"=>"intro_is_empty") );
		}
	}
	
	//echo "<pre>";
	
	/*$result = $dba->getAll("select fi_id,fv_address,ff_latitude,ff_longitude from t_store where ff_latitude<>0 and ff_longitude<>0 ");
	foreach($result as $data){
		$sql = "update t_store set 
			fp_geo = GeomFromText( 'POINT(".$data["ff_latitude"].' '.$data["ff_longitude"].")')
			where fi_id = ".$data["fi_id"];
		$uresult = $dba->query($sql);
		if(!$uresult)
			echo $sql."<br>";
		else
			echo $data["fi_id"]."^".$data["fv_address"]."^done<br>";
		
	}/**/
		
	/*批次取得經緯度
	 *
	 *
	 */
	//$result = $dba->getAll("select fi_id,fv_address from t_store where ff_latitude=0 or ff_longitude=0 ");
	
	//print_r($result);
	/*foreach($result as $data){
		echo $data["fi_id"];
		echo "^address:".$data["fv_address"]."<br>";
	}/**/
	/*foreach($result as $data){
		echo $data["fi_id"];
		$pos = getLatLng(trim(strtr($data["fv_address"],array(" "=>""))));
		if(!$pos){
			echo $data["fv_address"]."<br>";
			continue;
		}
		$sql = "update t_store set 
			ff_latitude = '".$pos["lat"]."' , 
			ff_longitude = '".$pos["lng"]."' 
			where fi_id='".$data["fi_id"]."'";
		$upresult = $dba->query($sql);
		if($upresult)
			echo "^address:".$data["fv_address"]."^lat:".$pos["lat"]."^lng:".$pos["lng"]."^<br>";
		else
			echo $sql."<br>";
		sleep(1);
		ob_flush();
		flush();
	}/**/
	
	/////
	//$address = "台北市青島東路21之10號";
	//$all = getLatLng($address);
	//echo "lat:".$lat."^^lng:".$lng."<br>";
	//print_r($all);
	
	//echo rawurlencode($address)."<br>".urlencode("台中市神岡區神岡路八十八巷八號");
	/////
	
	
	
?>
