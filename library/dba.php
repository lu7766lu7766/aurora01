
<?php
//ini_set("display_errors", "On");
//error_reporting(E_ALL);
class dba {
	
	private $conn,$result;
	
	private $server;
	private $user;
	private $pwd;
	private $db_name;
	private $port;
	
	function dba($_server="localhost",$_user="aurora01",$_pwd="auroraaurora01",$_db_name="aurora",$_port=33060){
    	$this->server = $_server;
    	$this->user = $_user;
    	$this->pwd = $_pwd;
    	$this->db_name = $_db_name;
    	$this->port = $_port;
    }
    
    /**
		 * 過濾多餘空白，sql小寫(方便判斷)
		 * @param string $sql     sql語句
		 */
	public function filter_sql($sql){
		$sql = preg_replace("/\n/"," ",preg_replace("/\r/"," ",preg_replace("/\r\n/"," ",preg_replace("/\t/"," ",trim($sql)))));
		
		while(substr_count($sql, "  "))
			$sql=strtr($sql,array("  "=>" "));
		$sql = strtr($sql , array(" WHERE "=>" where "));
		return $sql;
		
	}
	    
	public function query($sql) {
		if(!$this->conn)
			$this->connect();
		$this->result = mysqli_query($this->conn, $sql);
		return $this->result;
	}
	
	public function getAll($sql) {
		$this->result = $this->query($sql);
		$arr=array();
		if(!$this->result)
		{
			return false;
		}
		else
		{
			while($row = mysqli_fetch_assoc($this->result))
			{
				$arr[]=$row;
			}
			return $arr;
		}
	}
	
	function get_rows(){
		if(!$this->result){
			return false;
		}else{
			return mysqli_num_rows($this->result);
		}	
	}

	function mysqli_fetch_row(){
		return mysqli_fetch_row($this->result);
	}
		
	function mysqli_num_fields(){
		return mysqli_num_fields($this->result);
	}
	
	function insert_id(){
		return mysqli_insert_id($this->conn);
	}
	
	function affected_rows(){
		return mysqli_affected_rows($this->conn);
	}
	
	function select_db($db_name){
		mysqli_select_db($this->conn,$db_name);
	}
	
	function connect(){
		try{
			$this->conn=mysqli_connect($this->server,$this->user,$this->pwd,$this->db_name,$this->port);
		}catch (Exception $e) {
		    if($this->conn){
			    die("Could not connect to MySQL".mysqli_error($this->conn));
		    }else{
				die("Could not connect to MySQL");
		    }
		}
		$this->query("SET NAMES 'UTF8'"); 
		$this->query("SET CHARACTER SET UTF8");
		$this->query("SET CHARACTER_SET_RESULTS=UTF8");
		return $this->conn;
	}
	
	/**
	 * 從sql語句中分析取得where條件回傳(不含where，若沒有where則回傳"no where"字串)
	 * @param string $sql  sql語句
	 */	
	public function get_where($sql){//取ㄍwhere字串
		$sql = $this->filter_sql($sql);
		$b_str=" where ";
		$start_pos=strpos($sql,$b_str);
		if($start_pos!==false){
			
			$sql=substr($sql,$start_pos+strlen($b_str));
			//過濾view的"資料表."
			$rows=substr_count($sql, ".");
			$after_str=$sql;
			
			for($i=0;$i<$rows;$i++){
				
				$dot_pos=strpos($tmp_sql,".");
				
				if($dot_pos===false)break;
				
				$before_str=substr($tmp_sql,0,$dot_pos+1);
				$sql=strtr( $sql, array( substr($before_str,strpos($before_str," ")+1) =>"") );
				$after_str=substr($after_str,$dot_pos+1);
				
			}
			$a_str=array(" order by " ," limit " ,";" ," union " );
			
			$rows=count($a_str);
			$str_length=strlen($b_str);
			$after_pos=strlen($sql);
			
			foreach($a_str as $val){
				
				$tmp_pos=strpos($sql,$val);
				
				if( $tmp_pos!==false && $tmp_pos<$after_pos ){
					
					$after_pos=$tmp_pos;
					
				}
				
			}
			
			return  $sql;
			
		}else{
			
			return "no_where";
			
		}
		
	}//get_where() end
	/**
	 * 從sql語句中分析取得table名稱
	 * @param string $sql  sql語句
	 */
	public function get_table_name($sql){
		
		$key_word=array("from","update","into");
		
		foreach($key_word as $value){
			
			if(strpos( $sql, $value )!==false){
				
				$key_word_pos = strpos( $sql, $value );
				$key_word_len = strlen($value);
				
				$end_word	  = $value=="into"?"(":" ";
				$tmp_table	  = trim(substr($sql,$key_word_pos+$key_word_len));
				
				if(strpos( $tmp_table, "(" )===0){
					
					return $this->correspond_parentheses($tmp_table);
					
				}
				
				$table_name	  = explode( $end_word , $tmp_table );
				$del_char 	  = array(',',';');  //需要濾掉的字元
				
				foreach($del_char as $val)
					$table_name[0] = strtr($table_name[0],array($val=>"","`"=>""));
					//$table_name[0] = str_replace($val,"",$table_name[0]); 
				return trim($table_name[0]);
				
			}
			
		}
		
		return false;
		
	}//end get_table_name
	
	function backup_store_table()
	{
		/*
		0	`fi_id` int(11) NOT NULL ,
		1	`fv_subname` varchar(15) NOT NULL DEFAULT '',
		2	`fv_phone` varchar(15) NOT NULL,
		3	`fv_source_phone` varchar(15) DEFAULT NULL,
		4	`fv_address` varchar(80) NOT NULL,
		5	`fi_brand` int(5) DEFAULT NULL ,
		6	`ft_create` datetime DEFAULT NULL,
		7	`ft_update` datetime DEFAULT NULL,
		8	`ff_latitude` float(8,5) DEFAULT '0.00000' ,
		9	`ff_longitude` float(8,5) DEFAULT '0.00000' ,
		10	`fi_delete` int(1) DEFAULT '0',
		11	`fv_del_user` varchar(20) DEFAULT '' ,
		12	`fd_del` date DEFAULT NULL,
		13	`fp_geo` point DEFAULT NULL,
		14	`fi_level` int(1) DEFAULT '0' ,
		15	`fi_like` int(10) DEFAULT '0' ,
		16  `fi_special_price` int(1) DEFAULT '0' 
		17  `fv_special_info` varchar(200) DEFAULT ''
		18	`fv_passwd` varchar(100) DEFAULT ''
		19	`fi_voice_ad` int(1) DEFAULT '0' 
		20	`fi_msg_ad` int(1) DEFAULT '0' 
		*/
		if(!$this->conn)
			$this->connect();
		//get all of the tables
		$table = "t_store";
		
		$result = mysqli_query($this->conn,"SELECT * FROM $table where `fi_delete`='0'");
		$num_fields = mysqli_num_fields($result);
		$word_before = substr($table,0,2);
		$table_type = $word_before=="t_"?"table":"view";
		$return = 'DROP '.$table_type.' IF EXISTS '.$table.';';
		
		$row2 = mysqli_fetch_row(mysqli_query($this->conn,'SHOW CREATE table '.$table));

		$split_str = explode(",",$row2[1] );
		$k_brand_name = $this->getKey($split_str,"fp_geo");//13
		$k_type = $this->getKey($split_str,"fi_brand");//5
		$k_logo = $this->getKey($split_str,"ft_create");//6
		$k_menu = $this->getKey($split_str,"ft_update");//7
		$a_kunset = array(3,10,11,12,15,18,19,20);

		
		$return.= "\n\n".strtr($row2[1],array(
			"`fv_source_phone` varchar(15) DEFAULT NULL" => "",//3
			"`fi_brand` int(5) DEFAULT NULL" => "`fi_type` int(3) DEFAULT '0'",//5
			"`ft_create` datetime DEFAULT NULL" => "`fv_logo` varchar(50) DEFAULT NULL",//6
			"`ft_update` datetime DEFAULT NULL" => "`fv_menu` varchar(50) DEFAULT NULL",//7
			"`fi_delete` int(1) DEFAULT '0'," => "",//10
			"`fv_del_user` varchar(20) DEFAULT ''" => "",//11
			"`fd_del` date DEFAULT NULL," => "",//12
			"`fp_geo` point DEFAULT NULL"	=>	"`fv_brand_name` varchar(80) DEFAULT NULL",//13
			"`fi_like` int(10) DEFAULT '0'" => "",//15
			"`fv_passwd` varchar(100) DEFAULT ''" => "",//18
			"`fi_voice_ad` int(1) DEFAULT '0'" => "",//19
			"`fi_msg_ad` int(1) DEFAULT '0'" => "",//20
			"AUTO_INCREMENT"				=>	"",
			"ENGINE=MyISAM DEFAULT CHARSET=utf8"=>"")).";\n\n";

		//$return = preg_replace("/`fv_del_user`[\s]?varchar(20)[\s]?DEFAULT[\s]?''[\s]?[,]?/",'',$return); 
		$return = preg_replace("/COMMENT[\s]'[^']*'/",'',$return); 
		$return = preg_replace("/ENGINE=MyISAM (AUTO_INCREMENT)?=[\d]* DEFAULT CHARSET=utf8/",'',$return);        
		$return = preg_replace("/\s\s/",' ',$return); 
		while(substr_count($return, "  "))
			$return=strtr($return,array("  "=>" "));
		while(substr_count($return, ", ,")||substr_count($return, ",,"))
			$return = strtr($return,array(", ,"=>",",",,"=>","));
		//
	
		//6,7,10,11,12
		
		//$key = array_search("`fv_brand_name` point DEFAULT NULL", $split_str);

		for ($i = 0; $i < $num_fields; $i++) 
		{
			if($table_type!="view")
			while($row = mysqli_fetch_row($result))
			{
				$row3 = mysqli_fetch_row( mysqli_query($this->conn,"select fi_type,fv_brand_name,fv_logo,fv_menu from t_brand where fi_id='".$row[5]."' limit 1;"));
				$return.= 'INSERT INTO '.$table.' VALUES(';
				for($j=0; $j<$num_fields; $j++) 
				{
					$row[$j] = addslashes($row[$j]);
					$row[$j] = strtr($row[$j],array("\n"=>"\\n"));
					if($j==$k_brand_name)//fp_geo
					{
						if (isset($row[5]))
						{
							$return.= '"'.$row3[1].'"' ; 
						}
						else 
						{ 
							$return.= '""'; 
						}
					}
					else if($j == $k_type)
					{
						if (isset($row[5]))
						{
							$return.= '"'.$row3[0].'"' ; 
						}
						else 
						{ 
							$return.= '""'; 
						}
					}
					else if($j == $k_logo)
					{
						if (isset($row[5]))
						{
							$return.= '"'.$row3[2].'"' ; 
						}
						else 
						{ 
							$return.= '""'; 
						}
					}
					else if($j == $k_menu)
					{
						if (isset($row[5]))
						{
							$return.= '"'.$row3[3].'"' ; 
						}
						else 
						{ 
							$return.= '""'; 
						}
					}
					else if(in_array($j, $a_kunset))
					{
						continue;
					}
					else
					{
						if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
					}
					$return.= ','; 
					//$return.= ',';
					
				}
				$return=substr($return,0,-1);
				$return.= ");\n";
			}
		}
		$return.="\n\n\n";
		
		//save file
		//$handle = fopen('backups/db-backup-'.time().'-'.(md5(implode(',',$tables))).'.sql','w+');
		$handle = fopen('backups/db-store-backup.sql','w+');
		fwrite($handle,$return);
		fclose($handle);

		//echo strtr($return,array("\n"=>"<br>"))."<br>";

		$db_name = "sqlite.db";
		$db_path = "backups/".$db_name;
		@unlink($db_path);
		$db = new SQLite3($db_name);
		//$db->busyTimeout(1000);
		$db->exec("begin");
		$db->exec($return);
		$db->exec("commit");
		@rename($db_name,$db_path);
		echo "Export DB SQLite success!!<Br>";
		
		$db->close();
		unset($db);

	}

	function getKey($a_split_str,$key_word){
		$key = 0;
		$dis = 0;
		foreach($a_split_str as $k => $val ){
			//echo $val;
			if(strpos(trim($val), '`')!==0&&
				strpos( strtolower(trim($val)), 'drop')!==0&&
				strpos( strtolower(trim($val)), 'create')!==0){
				$dis++;
			}
			if( strpos($val,$key_word) !==false){
				$key = $k-$dis;
				break;
			}
		}
		return $key;
	}

	function backup_tables($tables = '*')
	{
		/*define("LN", __LINE__);//行號
		define("FL", __FILE__);//當前文件
		define("DEBUG", 0);//調試開關

		$db_name = "sqite.db";
		$db_path = "backups/".$db_name;
		if (!file_exists($db_path)) {
			if (!($fp = fopen($db_path, "w+"))) {
				exit(error_code(-1, LN));
			}
			fclose($fp);
		}
		//打開資料庫檔
		if (!($db = sqlite_open($db_path))) {
			exit(error_code(-2, LN));
		}*/
		if(!$this->conn)
			$this->connect();
		//get all of the tables
		if($tables == '*')
		{
			$tables = array();
			$result = mysqli_query($this->conn,'SHOW TABLES');
			while($row = mysqli_fetch_row($result))
			{
				$tables[] = $row[0];
			}
		}
		else
		{
			$tables = is_array($tables) ? $tables : explode(',',$tables);
		}
		
		//cycle through
		foreach($tables as $table)
		{
			$result = mysqli_query($this->conn,'SELECT * FROM '.$table);
			$num_fields = mysqli_num_fields($result);
			$word_before = substr($table,0,2);
			$table_type = $word_before=="t_"?"table":"view";
			$return.= 'DROP '.$table_type.' IF EXISTS '.$table.';';
			$row2 = mysqli_fetch_row(mysqli_query($this->conn,'SHOW CREATE table '.$table));
			$return.= "\n\n".$row2[1].";\n\n";
			
			for ($i = 0; $i < $num_fields; $i++) 
			{
				if($table_type!="view")
				while($row = mysqli_fetch_row($result))
				{
					$return.= 'INSERT INTO '.$table.' VALUES(';
					for($j=0; $j<$num_fields; $j++) 
					{
						$row[$j] = addslashes($row[$j]);
						$row[$j] = strtr($row[$j],array("\n"=>"\\n"));
						if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
						if ($j<($num_fields-1)) { $return.= ','; }
					}
					$return.= ");\n";
				}
			}
			$return.="\n\n\n";
		}
		
		//save file
		//$handle = fopen('backups/db-backup-'.time().'-'.(md5(implode(',',$tables))).'.sql','w+');
		$handle = fopen('backups/db-backup.sql','w+');
		fwrite($handle,$return);
		fclose($handle);
		
		/*if (!sqlite_query($db, $return)) {
			exit(error_code(-3, LN));
		}*/
	}
	function error_code($code, $line_num, $debug=DEBUG)
	{
		if ($code<-6 || $code>-1) {
		 return false;
		}
		switch($code) {
			case -1: $errmsg = "Create database file error.";
				break;
			case -2: $errmsg = "Open sqlite database file failed.";
				break;
			case -3: $errmsg = "Create table failed, table already exist.";
				break;
			case -4: $errmsg = "Insert data failed.";
				break;
			case -5: $errmsg = "Query database data failed.";
				break;
			case -6: $errmsg = "Fetch data failed.";
				break;
			case -7: $errmsg = "";
				break;
			default: $errmsg = "Unknown error.";
		}
		$m = "<b>[ Error ]</b><br>File: ". basename(FL) ." <br>Line: ". LN ."<br>Mesg: ". $errmsg ."";
		if (!$debug) {
			($m = $errmsg);
		}
		return $m;
	}
}
?>