
<?php
	include_once "Classes/PHPExcel/IOFactory.php";
	class Library {
		public static function getLatLng($address)
		{
			$allurl = "http://maps.googleapis.com/maps/api/geocode/json?address=".$address;
		//print_r( json_decode(file_get_contents($allurl),true) );
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $allurl);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-FORWARDED-FOR:8.8.8.8', 'CLIENT-IP:8.8.8.8'));  //构造IP

		curl_setopt($ch, CURLOPT_REFERER, "http://www.gosoa.com.cn/ ");
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$all = curl_exec($ch);
		curl_close($ch);
		if(strpos($all,"That’s an error")!==false){
			return false;
		}
		$a_pos = json_decode($all,true);
		if( !is_array($a_pos) || count($a_pos)==0 )
		{
			return self::getLatLng2($address);
		}
		//print_r($a_pos);
		$lat = $a_pos["results"][0]["geometry"]["location"]["lat"];
		$lng = $a_pos["results"][0]["geometry"]["location"]["lng"];
		$r["lat"] = $lat;
		$r["lng"] = $lng;
		sleep(1);
		ob_flush();
		flush();
		return $r;
		}
		
		public static function getLatLng2($address)
		{
			$allurl = "http://www.google.com.tw/maps/place/{$address}";
			$useragent = "Opera/9.80 (J2ME/MIDP; Opera Mini/4.2.14912/870; U; id) Presto/2.4.15";
			$post = array(
				"q"=>$address,
				"gs_htif50"=>$address
			);
			$ch = curl_init();
			$options = array(
				CURLOPT_URL=>$allurl,
				CURLOPT_HEADER=>0,
				CURLOPT_VERBOSE=>0,
				//CURLOPT_RETURNTRANSFER=>true,
				CURLOPT_USERAGENT=>$useragent,
				//CURLOPT_POST=>true,
				//CURLOPT_POSTFIELDS=>http_build_query($post)
			);
			curl_setopt_array($ch, $options);
			$all = curl_exec($ch);
			curl_close($ch);
			echo "^^".$address."^^".$all;
			return array("lat"=>0,"lng"=>0);
		}
	
		public static function get_colnum($colname,$data)
		{
			$cols = $data->getHighestColumn();
			//$cols = $data->colcount($sheet);//列
			$rows = $data->getHighestRow();
			//$rows = $data->colcount($sheet);//列
			$key_words = explode("||", $colname);
			if(count($key_words)>1){
				$key_len = 1;
			}else{
				$key_words = explode("&&", $colname);
				$key_len = count($key_words);
			}
			$cols++;
			for($row=1;$row<=$rows;$row++)
			{
				for($i='A';$i!=$cols;$i++)
				{
					$tmp_v="";
					foreach($key_words as $key_word)
					{
						
						if( strpos($data->getCell($i.$row)->getValue(),$key_word)!==false )
						{
							$tmp_v.="1";
						}else{
							break;
						}
							
						if(strlen($tmp_v)>=$key_len)
						{
							//$this->start_row = $row+1;
							return $i;
						}
					}
				}
			}
			return 0;
		}
	
	}
	
	
?>