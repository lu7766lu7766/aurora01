<?php 
    session_start();
    include 'backend/template.php';
    
    if(!empty($_SESSION['admin']['login_user'])){
        
        //取資料
        $login_fi_id = $_SESSION['admin']['login_fi_id'];
        //$login_store = $_SESSION['admin']['login_store'];
        $login_name = $_SESSION['admin']['login_name'];
        $login_permissions = $_SESSION['admin']['login_permissions'];
        $login_user = $_SESSION['admin']['login_user'];
        $login_username = $_SESSION['admin']['login_username'];
        $login_title = $_SESSION['admin']['login_title'];
        //$login_php_vars = $_SESSION['admin']['login_php_vars'];
        $menu = $_SESSION['admin']['menu'];
        
        $subject = "advertisement";
        //取資料
        $permissions2visit	=	(strpos($login_permissions,$subject."_list")!==false||$login_permissions=="all")?true:false;
        $permissions2add	=	(strpos($login_permissions,$subject."_add")	!==false||$login_permissions=="all")?true:false;
        $permissions2edit	=	(strpos($login_permissions,$subject."_edit")!==false||$login_permissions=="all")?true:false;
        $permissions2del	=	(strpos($login_permissions,$subject."_del")	!==false||$login_permissions=="all")?true:false;
        
        include 'library/dba.php';
        require_once "library/class_pager2.php";
        $dba = new dba();
        $pager = new Pager();
        $status = $_GET["s"];
        if($status == "add"){
        	$result = $dba->getAll("select max(fi_id) as fi_id from v_advertisement limit 1;");
        	$where = " and fi_id = ".$result[0]["fi_id"];
        }

        $keyword = $_COOKIE[$subject."_keyword"];
        if($keyword !=""){ 
        	$keyword_val = '%'.strtr($keyword,array(" "=>"%","台"=>"臺")).'%';
        	$where_keyword = " and ( fv_brand_name like '$keyword_val' or fv_describe like '$keyword_val' or fv_area_list like '$keyword_val')"; 
        }

        $type = $_COOKIE[$subject."_type"];
        if($type!=""){ $where_type=" and fi_type='$type' "; }

        $sql = "select fi_id
        				,fi_type
        				,fv_ad
        				,fv_describe
        				,fi_brand
        				,fv_url
        				,fi_date_active
        				,fd_start_date
        				,fd_end_date
        				,fv_area_list
        				,fi_weights
        				,fi_active
        				,fv_brand_name
        				,fv_sbrand_name
        				,fv_phone
        		from v_advertisement 
        		where (fi_brand_delete <>1||fi_brand_delete is null)
        			and fi_delete <>1
        			$where_type
        			$where_keyword
        			$where 
        		order by fi_active desc, fi_brand_weights desc, fi_weights desc";

        $all_data = $pager->query($sql);
        $all_city = $dba->getAll("select distinct fv_city from t_area");
        $img_path = "images/advertisement/";
		$file_size_limit = 1024*1024*1;//1M   unit:byte
		
    }else{
        header("Location:index.php");
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo $html_title;?></title>
    	<link rel="stylesheet" type="text/css" href="backend/css/jquery.datetimepicker.css">
        <?php echo $html_resource;?>
        <script type="text/javascript" src="backend/js/jquery.datetimepicker.js"></script>
	    <script type="text/javascript" src="backend/js/date_setting.js"></script>
        <script>
            /******************** user define js ********************/
            $(document).ready(function(){
            	
                //ajax
                //filename write in template.js
                
                //新增
                $("input[data-open-dialog='新增視窗']").click(function(){
                	var $dialog = $("#dialog_content");
                	search_setting($dialog);
                })
                	
                function search_setting($dialog,$data){
                	$data = $data||$("#ccccc");
                	$dialog.find("input[search]").each(function(){
	                	$this = $(this);
	                	$(this).wrap("<div class='search' style='position:relative;'></div>");
	                	$container = $(this).parent();
	                	$container.append("<div class='selector'></div>");
	                	$selector = $container.children(".selector");
	                	$selector.css({
	                		position:"absolute",
	                		top:$this.height()+6,
	                		left:0,
	                		width:$this.width(),
	                		"z-index":10
	                	})
	                	$(this).keyup(function(e){
	                		if(e.keyCode == 8)return; // backspace
	                		var keyword = $(this).val();
	                		//console.log(keyword.length);
	                		if(keyword.length>1){
	                			//console.log("keyword:"+keyword+"::"+filename)
	                			$.post(filename,{
		                			query_type:	"get_search",
		                			keyword:keyword
									},function(data){
										//console.log(data);
										if(data.count==0){
											if($data.length==0){//add
                								$dialog.find("input[name='"+$this.attr("search")+"']").val('');
                							}else{
                								$data.attr($this.attr("search"),"");
                							}
											$selector.html('');
	                						$selector.css("border","none");
			                    		}else if(data.count==1){
				                    		for(var key in data.result){
			                    				$selector.append("<div class='values' id='"+data.result[key].id+"'>"+data.result[key].text+"</div>")
			                    			}
			                    			$selector.children(".values").click(function(){
			                    				if($data.length==0){//add
                									$dialog.find("input[name='"+$this.attr("search")+"']").val($(this).attr("id"));
	                							}else{
	                								$data.attr($this.attr("search"),$(this).attr("id"));
	                							}
			                    				
			                    				$this.val($(this).text());
			                    				$selector.html('');
	                							$selector.css("border","none");
			                    			})
			                    			$selector.children(".values").trigger("click");
			                    		}else if(data.count<10){
				                    		for(var key in data.result){
			                    				$selector.append("<div class='values' id='"+data.result[key].id+"'>"+data.result[key].text+"</div>")
			                    			}
			                    			$selector.css({
			                    				"border-left":"2px solid #caf",
						                		"border-bottom":"2px solid #caf",
						                		"border-right":"2px solid #caf"
						                	})
			                    			$selector.children(".values").css({
			                    				"background-color":"#eef1ff",
			                    				"color":"#1c0097"
			                    			}).hover(function(){
			                    				$(this).css({
			                    					"background-color":"#284bff",
			                    					"color":"#eef1ff"
			                    				})
			                    			},function(){
			                    				$(this).css({
			                    					"background-color":"#eef1ff",
			                    					"color":"#1c0097"
			                    				})
			                    			}).click(function(){
			                    				if($data.length==0){//add
                									$dialog.find("input[name='"+$this.attr("search")+"']").val($(this).attr("id"));
	                							}else{
	                								$data.attr($this.attr("search"),$(this).attr("id"));
	                							}
			                    				//$("input[name='"+$this.attr("search")+"']").val($(this).attr("id"));
			                    				$this.val($(this).text());
			                    				$selector.html('');
	                							$selector.css("border","none");
			                    			})
			                    		}else{
			                    			if($data.length==0){//add
            									$dialog.find("input[name='"+$this.attr("search")+"']").val($(this).attr("id"));
                							}else{
                								$data.attr($this.attr("search"),$(this).attr("id"));
                							}
			                    			$selector.html('');
	                						$selector.css("border","none");
			                    		}
			                    },"json");
	                		}
	                	})
	                })
                }
                
                $("input.add_btn").click(function(){
                	
                	var $dialog = $("#dialog_container");
                	var brand = $dialog.find("input[name='add_brand']").val();
                	var brand_name = $dialog.find("input[name='add_brand_name']").val();
                	
                	$.post(filename,{
                			query_type:	"advertisement_add",
                			brand:brand,
                			brand_name:brand_name
						},function(data){
							data=$.trim(data);
                    		if(data=="success"){
                    			//alert("新增成功！");
	                    		location.href = "manage_advertisement.php?s=add";
                    		}
                    		console.log(data)
                    });
	            });
                
                //編輯
                var $panel = $("#editPanel");
                function init(){
                	//刪除
	                $("input.del_btn").bind('click',function(){
	                    if(!confirm("確定刪除？"))return;
	                    var $this = $(this);
	                    var fi_id = $this.attr('fi_id');
	                    
	                    $this.unbind('click');
	                    $.post(filename,{
	                    		query_type:"advertisement_del",
	                    		fi_id:fi_id
							},function(data){
								data=$.trim(data);
								if(data=="success"){
			                        alert("已刪除！");
			                        location.href = location.href+"&s=c";
		                        }
		                        console.log(data)
	                    });
	                });
                
                	//$("#list_panel [data-open-dialog]").unbind("click");
	                $("#list_panel [data-open-dialog]").click(function(){
	                	dataInit();
	                	var $dialog = $("#dialog_content");
	                	
	                	edit_id = $(this).attr('fi_id');
	                	console.log()
	                	var $data = $("#data_"+edit_id);
	                	
	                	search_setting($dialog,$data)
	                	
	                    //radio更新
	                	var arr_name = [];
	                    $dialog.find("input[type='radio']").each(function(){
		                    var name=$(this).prop("name").replace(/\W+/g, "");
		                    if( !in_array(name,arr_name) )
		                    	arr_name.push(name);
	                    })
	                    for(i=0; i<arr_name.length; i++){
	                    	var name=arr_name[i];
	                    	var radio_val = $data.attr(name);
							$dialog.find("input[name='"+name+"'][value='"+radio_val+"']").prop("checked",true);
	                    }
	                    //text
						$dialog.find("input[type='text']").each(function(){
							$(this).val($data.attr($(this).attr("name")));
						})
							
						//date
						$dialog.find(".date").change(function(){
							$data.attr($(this).attr("name"),$(this).val());
						})
						//img
						$dialog.find("input.pic[type='file']").each(function(){
							var preview_id = $(this).prop('name')+"_preview";
							$("#"+preview_id).html($data.data(preview_id));
							img_setting($("#"+preview_id));
						});
						
						//select
						$dialog.find("select").each(function(){
							$(this).find("option[selected]").removeAttr("selected");
							$(this).find("option[value='"+$data.attr($(this).prop('name'))+"']").prop("selected",true);
						})
						
						//city
						var timer
						$dialog.find(".city").parent().mouseenter(function(){
							var city = $(this).find(".city").val();
							var inData = $dialog.find("#dist_container").find("input:first").val()==city;
							var isChecked = $(this).find(".city").is(":checked");
						
							if(!inData){
								timer = setTimeout(function(){
									$.post(filename,{
			                    		query_type:"get_dist",
			                    		city:city
									},function(data){
										$table = $(data);
										$table.find(".all").unbind("click");
										$table.find(".all").click(function(){
											$dialog.find("#dist_container").prev().find("input[value='"+city+"']").trigger("click");
										})
										$table.find(":not(.all)").unbind("change");
										$table.find(":not(.all)").change(function(){
											var dist = $(this).val();
											if(dist=="")return;
											var area_list = $data.attr("edit_area_list")||"";
											var a_area_list = area_list.split(",");
											var city_checked = false;
											for(var key in a_area_list){
												if( a_area_list[key] == city ){
													city_checked = true;
													break;
												}
											}
											if( $(this).is(":checked") ){
												$data.attr("edit_area_list",$data.attr("edit_area_list")+city+dist+",");
											}else{
												if(city_checked){
													$table.find(".all").prop("checked",false);
													$dialog.find("#dist_container").prev().find("input[value='"+city+"']").prop("checked",false)
													area_list = area_list.replace(city+",","");
													var new_add_list = "";
													$table.find(":checkbox:checked").each(function(){
														new_add_list += city+$(this).val()+",";
													})
													$data.attr("edit_area_list",area_list+new_add_list);
												}else{
													$data.attr("edit_area_list",$data.attr("edit_area_list").replace(city+dist+",",""));
												}
											}
											update_area();
										})
										
										
										$dialog.find("#dist_container").html($table).show();
										table_flash(city);
			                    	});
								},500)
							}
						}).mouseleave(function(){
							clearTimeout(timer)
						})
						$dialog.find(".city").change(function(){
							var city = $(this).val();
							var isChecked = $(this).is(":checked")
							//$dialog.find("#dist_container").find("input").prop("checked",isChecked);
							var area_list = $data.attr("edit_area_list")||"";
							var a_area_list = area_list.split(",");
							var new_area_list = "";
							for(var key in a_area_list){
								var area = a_area_list[key];
								if(area.indexOf(city)==-1 && area!=""){
									new_area_list += area+",";
								}
							}
							if(isChecked)
							{
								new_area_list += city+",";
							}
							$data.attr("edit_area_list",new_area_list);
							update_area();
							table_flash(city);
						})
						
						table_flash();
						function table_flash(city){
							city = city||"";
							var area_list = $data.attr("edit_area_list");
							var a_area_list = area_list.split(",");
							if( city=="" ){
								for(var key in a_area_list){
									var val = a_area_list[key];
									if(val!=""){
										$(".city_container input[value='"+val+"']").prop("checked",true);
									}
								}
								return;
							}
							var city_checked = false;
							for(var key in a_area_list){
								if( a_area_list[key] == city ){
									city_checked = true;
									break;
								}
							}
							if(city_checked){
								$dialog.find("#dist_container input:checkbox").prop("checked",true)
							}else{
								$dialog.find("#dist_container input:checkbox").each(function(){
									$(this).prop("checked",
										area_list.indexOf(city+$(this).val())>-1?true:false )
								});
							}
						}
						
						function update_area(){
							var area_list = $data.attr("edit_area_list");
							var a_area_list = area_list.split(",");
							var html = "";
							for(var key in a_area_list){
								if(a_area_list[key]=="")continue;
								html+="<div class='list' style='margin:5px 10px 0 0;border:1px solid #333;padding:3px;min-width:5px;display:inline-block;cursor:pointer;'>"+
									a_area_list[key]+"</div>";
							}
							$(".area_list").html(html);
							$(".area_list").find(".list").click(function(){
								var len = $dialog.find(".city[value='"+$(this).html()+"']").length;
								if(len>0)
									$dialog.find(".city[value='"+$(this).html()+"']").trigger("click");
								area_list = area_list.replace($(this).html()+",","");
								$data.attr("edit_area_list",area_list);
								console.log($dialog.find("#dist_container").find("input:first").val())
								table_flash($dialog.find("#dist_container").find("input:first").val())
								update_area()
							})
						}
						//alert2($dialog.find("input[name='edit_type']:checked").length);
						
						$dialog.find("input[name='edit_type']").change(function(){
							var  val = $(this).val();
							//alert2(val);
							if(val==1){
								$("#trphone").show();
							}else{
								$("#trphone").hide();
							}
						})
						$dialog.find("input[name='edit_type']:checked").trigger("change");
		            })
					
                }
                init()
                
                //及時更新暫存資料(hidden暫存，沒存進資料庫)
                $panel.find("input[type!='button'][type!='checkbox'][type!='radio']").keyup(function(){
	                $("#data_"+edit_id).attr($(this).prop("name"),$(this).val() );
                })
                	
                $panel.find("input[type='radio']").click(function(){
                    $("#data_"+edit_id).attr($(this).prop("name"),$(this).val() );
                });
                //select
                $panel.find("select").change(function(){
                    $("#data_"+edit_id).attr($(this).prop("name"),$(this).val() );
                });
                
                $panel.find("input.edit_btn").click(function(){
                	var $dialog = $("#dialog_content");
                    //權限
                    if(!chk_all_input("#dialog_container")){
	                    return;
                    }
                    var $data = $("#data_"+edit_id);
                    var type 		= $data.attr("edit_type");
                    var brand 		= $data.attr("edit_brand");
                    var brand_name 	= $data.attr("edit_brand_name");
                    var describe 	= $data.attr("edit_describe");
                    var url 		= $data.attr("edit_url");
                    var date_active = $data.attr("edit_date_active");
                    var area_list 	= $data.attr("edit_area_list");
                    var start_date 	= $data.attr("edit_start_date");
                    var end_date 	= $data.attr("edit_end_date");
                    var active 		= $data.attr("edit_active");
                    var weights 	= $data.attr("edit_weights");
                    var phone 		= $data.attr("edit_phone");
					var $a_file = $data.data("$a_file")||[];
					var $a_remove_file = $data.data("$a_remove_file")||[];
					var len = 0;
					
					var form_data = new FormData();
						form_data.append("query_type",	"advertisement_edit");
						form_data.append("fi_id",		edit_id);
						form_data.append("type",		type);
						form_data.append("describe",	describe);
						form_data.append("brand",		brand);
						form_data.append("brand_name",	brand_name);
						form_data.append("url",	url);
						form_data.append("area_list",	area_list);
						form_data.append("date_active",	date_active);
						form_data.append("start_date",	start_date);
						form_data.append("end_date",	end_date);
						form_data.append("weights",		weights);
						form_data.append("active",		active);
						form_data.append("phone",		phone);
					
					len = $a_file.length;
					for( j=0 ; j<len ; j++ )
						form_data.append('file[]', $a_file[j]);
					
					len = $a_remove_file.length;
					for( j=0 ; j<len ; j++ )
						form_data.append( 'a_remove_file['+j+']',$a_remove_file[j] );
                    
                    $.ajax({
							url: filename,
							dataType: 'text',
							cache: false,
							contentType: false,
							processData: false,
							data: form_data,                         
							type: 'post',
							success: function(data){
								if($.trim(data).indexOf("success")>-1){
			                        alert("已更新！");
			                        location.href="manage_advertisement.php";
		                        }
		                        console.log(data)
							}
						});
                });
                
                //無span之圖片(原有)包裝後加上span[default]，點下會進入$a_remove_file[]，剩下的span(新加入)點擊後加入$a_file[]
                function img_setting($preview){
                	var total = $preview.find("img").length;
	                var $add_img_btn = $preview.parent().children("input[type='file']");
	                //console.log(total+"^^"+$add_img_btn.attr("max_len")+$preview.html());
	                if( total>=$add_img_btn.attr("max_len") ){
		                $add_img_btn.unbind("click");
		                $add_img_btn.hide()
	                }else{
		                $add_img_btn.bind("click");
		                $add_img_btn.show()
	                }
	                
	                var name	= $preview.attr("btn_name");
	                
                    var preview_id = name+"_preview";
                    
                    var $data 		= $("#data_"+edit_id);
                    
	                $preview.find("img:not([new])").each(function(index){
	                    $img = $(this);
	                    if( $img.parent().children("span").length==0 ){//原先圖片
	                    	$img.css('max-height','100px')
		                    //console.log("img_parent.html:\n"+$img.parent().html())
		                    $img.wrap("<div style='display:inline-block;text-align:center;margin-bottom:10px;'></div>");
                            $img.parent().append("<br/><span class='bg shadowRoundCorner' style='padding:3px;cursor:pointer;color:white'>delete</span>");
                        }
                    })
                    $preview.find("img").change(function(){
                    	$data.data(preview_id,$(this).parent().parent().html());
                    })
                    // img
					$preview.find("span:not([new])").click(function(){
						var $a_remove_file = $data.data("$a_remove_file")||[];
						$preview = $(this).parent().parent();
                        var tmp_arr = [];
                        $img = $(this).parent().find("img");
                        src = $img.prop("src")||"";
                        //tmp_arr["src"]	 = src;
                        //tmp_arr["title"] = src.substring(src.lastIndexOf('/')+1);
                        //$a_remove_file.push(tmp_arr);
                        $a_remove_file.push(src.substring(src.lastIndexOf('/')+1))
                        $(this).parent().remove();
                        //console.log(src);
                        img_setting($preview)
                        $data.data("$a_remove_file",$a_remove_file);
                        $data.data(preview_id,$preview.html());
                    })
                    $preview.find("span[new]").click(function(){
                    	var $a_file = $data.data("$a_file");
                    	console.log("data : "+$data.prop("id"))
                    	console.log("span click : "+$a_file)
                        $preview = $(this).parent().parent();
	                    $(this).parent().remove();
	                    var now_len = $a_file.length;
	                    for( k=0 ; k<now_len ; k++ ){
	                    	try{
	                    		var title=unescape($(this).parent().find("img").attr("title"));
                            	if( $a_file[k].name == title ){
                                    $a_file.splice(k,1);
                                    break;
                                }
                        	}catch(error){
                        	}
	                    }
	                    img_setting($preview)
	                    $data.data("$a_file",$a_file);
	                    $data.data(preview_id,$preview.html());
                    })
                    
                    $data.data(preview_id,$preview.html());
                }
                
                //加上預覽ＤＩＶ
                $panel.find("input[type='file'][class='pic'][name]").each(function(){
                	var $this = $(this);
                	var name = $this.prop("name");
	                var preview_id = name+"_preview";
	                if($("div#"+preview_id).length==0)
	                	$this.before("<div id='"+preview_id+"' btn_name='"+name+"'></div>");
                })
                
                //var $a_remove_file=[];//存放所有刪除檔案路徑
	            //var $a_file=[];//存放所有檔案
				
                $panel.find("input[type='file'][class='pic'][name]").change(function(evt)
                {
                    var $btn	= $(this);
                    var name	= $btn.prop("name");
                    var height	= $btn.attr("height")||100;
               
                    var now_no	= $panel.attr('now_no');
                    var data_id = "data_"+edit_id;
                    var preview_id	= name+"_preview";
                       
                    var $data   		= $("#"+data_id);
                    var $preview		= $("#"+preview_id);
                    var $a_file = $data.data("$a_file")||[];
                    
                    var max_len = $btn.attr("max_len");
                    var new_len = $btn[0].files.length;
                    var old_len = $a_file.length;
                    $preview.html($data.data(preview_id)||"");
                    
                    for(var i=0, f; f=evt.target.files[i]; i++) {
                        if(!f.type.match('image.*')) {
                            continue;
                        }
                        var file_size_limit = "<?php echo $file_size_limit?>";
                        file_size_limit = file_size_limit==""? 0 : file_size_limit;
                        if( f.size > file_size_limit ){
	                        alert2(f.name+"檔案超過1MB，請重新上傳。");
	                        if(evt.target.files[i+1]==undefined){
		                        evt.target.value="";
	                        }
	                        continue;
                        }
                        
                        var reader = new FileReader();
                        reader.onload = (function(theFile) {
                            return function(e) {
	                            //new屬性作用在於，圖檔先進preview，img_setting會偵測到沒有span，給加上default span
	                            $preview.append('<img new class="thumb" src="'+e.target.result+'" title="'+escape(theFile.name)+'"/>');
                                $preview.find("img:last")[0].onload = function(){
                                    var $this = $(this);
                                    var $preview = $this.parent();
                                    var title = $this.attr("title");
                                    var onload_len = $preview.find("img").length;
                                    //var filename = this.width+"x"+this.height+"_@."+title.substring(title.lastIndexOf(".")+1);
                                    $this.css({
                                        "border":"1px solid gray",
                                        "margin":"5px"
                                    });
                                    //$this.attr("title",filename);
                                    $this.height(height);
                                    $this.wrap("<div style='display:inline-block;text-align:center;margin-bottom:10px;'></div>");
                                    $this.parent().append("<br/><span new class='bg shadowRoundCorner' style='padding:3px;cursor:pointer;color:white'>delete</span>");
                                    
                                    $a_file.push(theFile);
                                    var tmp_new_len =$a_file.length-old_len
                                    //console.log(+"onload_len:"+onload_len+"^^old_len:"+old_len+"^^new_len:"+new_len+"^^max_len:"+max_len);
                                    if( tmp_new_len == new_len || onload_len == max_len ){
	                                    img_setting($preview);
                                    	$data.data("$a_file",$a_file);
                                    	console.log("data : "+$data.prop("id"))
                                    	console.log("a_file : "+$a_file)
                                    	$data.data(preview_id,$preview.html());
                                    	//console.log(preview_id+"^^"+$preview.html())
	                                    $btn.val("");
                                    }
                                }
                            };
                        })(f);
                        reader.readAsDataURL(f);
                    }
                });
                ////////////////
                <?php 
                	if($status=="add"){
                		echo '$("#list_panel [data-open-dialog]").trigger("click");';
                	}
                ?>
            });
        </script>
        <style>
            /******************** user define css ********************/
			.search_img{
				background:url(images/public/search.png);
				-moz-background-size:contain;
				-webkit-background-size:contain;
				-o-background-size:contain;
				background-size:contain;
				background-repeat: no-repeat;
				vertical-align: text-top;
				width:20px;
				height:20px;
				border: 0;
				z-index:2;
				position:relative;
				left:-25px;
			}
        </style>
    </head>
    <body>
    	<form>
    		<input type="hidden" id="subject" value="<?php echo $subject;?>"/>
    		<input type="hidden" name="fi_type" id="fi_type" value="<?php echo $type;?>"/>
    		<input type="hidden" name="fv_keyword" id="fv_keyword" value="<?php echo $keyword; ?>"/>
    	</form>
        <div id="wrapper">
            <!-- ******************** header ******************** -->
            <div id="header">
                <h3><?php echo $html_title; ?></h3>
            </div>
            <!-- /.header -->
            <!-- ******************** body ******************** -->
            <div id="body">
                <div id="body_left">
                    <?php echo $menu; ?>
                </div>
                <!-- /.body_left -->
                <div id="body_right">
                	<?php
                        if($permissions2add){
                            echo "<input type='button' value='新增廣告' data-open-dialog='新增視窗'>";
                            echo "<div id='addPanel' data-dialog='新增視窗'>";
                            echo "<table class='table-v'>";
                            echo 	"<tr><td>廣告店家</td>
                            			<td>
	                            		<input type='hidden' id='add_brand' name='add_brand'/>
	                            		<input id='add_brand_name' name='add_brand_name' search='add_brand'>
	                            		</td>
	                        		</tr>";
                            echo 	"<tr><td></td><td><input class='add_btn' type='button' value='送出'></td></tr>";
                            echo "</table>";
                            echo "</div>";
                        }else{
                            echo "你無權新增廣告";
                        }
                    ?>
                    <br><br>
                    <div id="search">
	                    <div style='float:left;'>
		                    <select id="type_select">
		                    	<option value="">全部</option>
		                    	<option value="0" >半版</option>
		                    	<option value="1" >選單</option>
		                    </select>
	                    </div>
	                    <div style='float:left;'>
	                    	<div style'clear:both;'>
	                    		<input type='text' name='keyword' id='keyword' placeholder="請輸關鍵字" alt="可輸入 區域 或 描述 或 店家名稱"/>
	                    		<input type='button' name='search_img' id='search_img' class='search_img'/>
								<!--<img src='images/search.png' height='20' valign='bottom'/>-->
	                    	</div>
	                    </div>
	                    <div style='float:left;'>
	                    	<input type='button' id="submit" value="查詢"/>
	                    </div>
<script>
	$(document).ready(function(){
		$("#type_select").val($("#fi_type").val());
		$("#keyword").val($("#fv_keyword").val());
		$("#type_select").change(function(){
			$("#fi_type").val($(this).val());
			document.cookie = $("#subject").val()+"_type="+$(this).val();
		})
		$("#keyword").keyup(function(e){
			$("#fv_keyword").val($(this).val());
			document.cookie = $("#subject").val()+"_keyword="+$(this).val();
			if(e.keyCode=="13"){$("form").submit();}
		})
		$("#search_img").click(function(){
			$("form").submit();
		})
		$("#submit").click(function(){
			$("form").submit();
		})
	})
</script>
                    </div>
                    <br>
                    <?php
						echo "<br>";
						echo "
							<table class='table-h' id='list_panel' style='margin-top:10px'>
					            <tr>
					            	<td>ID</td>
					            	<td>類型</td>
					            	<td>廣告圖片</td>
					            	<td>廣告描述</td>
					            	<td>品牌名稱</td>
					        		<td>時效性</td>
					            	<td>前台顯示</td>
					            	<td>修改</td>
					            	<td>刪除</td>
					            </tr>";
						$pager->display();
						if(is_array($all_data))
						foreach($all_data as $data){
								$id = $data["fi_id"];
								$type = $data["fi_type"];
								$type_cht = $type==0 ?"半版":"選單";
								$ad = $data["fv_ad"];
								$describe 	= $data["fv_describe"];
								$brand 		= $data["fi_brand"];
								$brand_name = $brand==0?$data["fv_brand_name"]:$data["fv_sbrand_name"];
	        					$url 		= $data["fv_url"];
	        					$date_active= $data["fi_date_active"];
	        					$date_active_cht = $date_active==1?"啟用":"停用";
	        					$start_date = $data["fd_start_date"];
	        					$end_date 	= $data["fd_end_date"];
	        					$area_list 	= $data["fv_area_list"];
	        					$weights 	= $data["fi_weights"];
	        					$active 	= $data["fi_active"];
	        					$active_cht = $active==1?"顯示":"不顯示";
	        					$phone 		= $data["fv_phone"];
							echo "<tr>";
							echo "<td>$id</td>
								  <td>$type_cht</td>
								  <td>";
							if( file_exists($img_path.$ad)&&!is_dir($img_path.$ad) )
					            echo "<img src='{$img_path}{$ad}' title='$ad' style='max-height:100px;max-width:600px'/>";
					        echo "	</td>
					        		<td>$describe</td>
					            	<td>$brand_name</td>
					        		<td>$date_active_cht</td>
					            	<td>$active_cht</td>
					        		<td>";
					        if($permissions2edit)
					            echo "<input type='button' fi_id='$id' value='編輯' data-open-dialog='編輯視窗'>";
					        echo "	</td>
					        		<td>";
					        if($permissions2del)
					            echo "<input type='button' fi_id='$id' value='刪除' class='del_btn'>";
							echo "	<input type='hidden' id='data_{$id}' 
											edit_brand='$brand' 
											edit_type = '$type'
											edit_brand_name='$brand_name' 
											edit_describe = '$describe'
											edit_active='$active' 
											edit_weights='$weights' 
											edit_url = '$url'
											edit_date_active = '$date_active' 
											edit_start_date='$start_date'
											edit_end_date='$end_date' 
											edit_area_list='$area_list' 
											edit_phone='$phone'>";
				            if( file_exists($img_path.$ad)&&!is_dir($img_path.$ad) )
				            echo "<script>
				            		$(document).ready(function(){
					            		$('#data_{$id}').data('ad_preview',
					            			\"<img src='{$img_path}{$ad}' title='$ad' style='max-height:100px;' />\"
					            		);
				            		});
				            	</script>";
							echo "	</td>
								</tr>";
						}
						echo "</table>
							<br>";
						$pager->display();
	                ?>
                    <div id="editPanel" data-dialog="編輯視窗">
                        <table class='table-v'>
                        	<tr>
                                <td width="30%">廣告類別</td>
                                <td>
                                	<input type="radio" name="edit_type" id="edit_type_menu" value="1">
                                	<label for="edit_type_menu">選單廣告</label>
                                	<input type="radio" name="edit_type" id="edit_type_half" value="0">
                                	<label for="edit_type_half">半版廣告</label>
                                </td>
                            </tr>
                        	<tr>
                                <td>廣告圖片</td>
                                <td><input type="file" class="pic" name="ad" max_len='1'></td>
                            </tr>
                            <tr>
                                <td>廣告描述</td>
                                <td><input type="text" name="edit_describe" id="edit_describe" /></td>
                            </tr>
                        	<tr>
                                <td>廣告店家</td>
                                <td><input type="text" name="edit_brand_name" id="edit_brand_name" search='edit_brand'/></td>
                            </tr>
                        	<tr>
                                <td>廣告連結</td>
                                <td><input type="text" name="edit_url" id="edit_url" /></td>
                            </tr>
                            <tr id="trphone">
                                <td>廣告電話</td>
                                <td><input type="text" name="edit_phone" id="edit_phone" /></td>
                            </tr>
                        	<tr>
                                <td>廣告顯示區域<br>(請將滑鼠移至縣市上)</td>
                                <td>
                        			<div class='area_list' style="padding:10px"></div>
                        			<div style='float:left'>
                        			<?php
                        				echo "<table class='table-n city_container'>";
                        				foreach($all_city as $key=>$val){
                        					$city = $val["fv_city"];
                        					if($key%3==0){echo "<tr>";}
                        					echo "<td width='33%'><label><input type='checkbox' value='$city' class='city' name='edit_area_list' >$city</label></td>";
                        					if($key%3==2){echo "</tr>";}
                    					}
                    					echo "</table>";
                        			?>
                        			</div>
                        			<div style='float:left;margin:20px 0 0 20px;width:auto' id='dist_container'>
                        			</div>
                        		</td>
                            </tr>
                        	<tr>
                                <td>時效性<br>(是否要依據開始及結束時間撥放廣告)</td>
                                <td>
                                	<input type="radio" name="edit_date_active" id="edit_date_active_on" value="1">
                                	<label for="edit_date_active_on">啟用</label>
                                	<input type="radio" name="edit_date_active" id="edit_date_active_off" value="0">
                                	<label for="edit_date_active_off">停用</label>
                                </td>
                            </tr>
                        	<tr>
                                <td>開始時間</td>
                                <td>
                                	<input type="text" name="edit_start_date" id="edit_start_date" class='date'>
                                </td>
                            </tr>
                       		<tr>
                                <td>結束時間</td>
                                <td>
                                	<input type="text" name="edit_end_date" id="edit_end_date" class='date'>
                                </td>
                            </tr>
                        	<tr>
                                <td>權重<br>(數字大者在前)</td>
                                <td>
                                	<input type="text" name="edit_weights" id="edit_weights">
                                </td>
                            </tr>
                        	<tr>
                                <td>前台顯示</td>
                                <td>
                                	<input type="radio" name="edit_active" id="edit_active_on" value="1">
                                	<label for="edit_active_on">顯示</label>
                                	<input type="radio" name="edit_active" id="edit_active_off" value="0">
                                	<label for="edit_active_off">不顯示</label>
                                </td>
                            </tr>
                            <tr><td></td><td><input type="button" value="儲存" class="edit_btn" style="cursor: pointer;"></td></tr>
                        </table>
                    </div>
                </div>
                <!-- /.body_right -->
            </div>
            <!-- /.body -->
            <!-- ******************** footer ******************** -->
            <div id="footer">
                <span><?php echo $html_copyright; ?></span>
            </div>
            <!-- /.footer -->
        </div>
        <!-- /.wrapper -->
    </body>
</html>
