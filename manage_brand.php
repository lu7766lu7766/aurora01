<?php 
    session_start();
    include 'backend/template.php';
    
    if(!empty($_SESSION['admin']['login_user'])){
        
        //取資料
        $login_fi_id = $_SESSION['admin']['login_fi_id'];
        //$login_store = $_SESSION['admin']['login_store'];
        $login_name = $_SESSION['admin']['login_name'];
        $login_permissions = $_SESSION['admin']['login_permissions'];
        $login_user = $_SESSION['admin']['login_user'];
        $login_username = $_SESSION['admin']['login_username'];
        $login_title = $_SESSION['admin']['login_title'];
        //$login_php_vars = $_SESSION['admin']['login_php_vars'];
        $menu = $_SESSION['admin']['menu'];
        
        $subject = "brand";
        //取資料
        $permissions2visit	=	(strpos($login_permissions,$subject."_list")!==false||$login_permissions=="all")?true:false;
        $permissions2add	=	(strpos($login_permissions,$subject."_add")	!==false||$login_permissions=="all")?true:false;
        $permissions2edit	=	(strpos($login_permissions,$subject."_edit")!==false||$login_permissions=="all")?true:false;
        $permissions2del	=	(strpos($login_permissions,$subject."_del")	!==false||$login_permissions=="all")?true:false;
        
        include 'library/dba.php';
        $dba = new dba();
        $all_type = $dba->getAll("select fi_id,fv_name,fi_active from t_store_type order by fi_weights desc");
        $first_type = $all_type[0]["fi_id"];
        
        $brand = $_COOKIE[$subject."_brand"];
        $selector_html="";

        foreach($all_type as $per_type){
            $select_attr = $brand==$per_type["fi_id"]?"selected":"";
        	if($per_type["fi_active"]==1)
        		$selector_html.="<option value='".$per_type["fi_id"]."' $select_attr >".$per_type["fv_name"]."</option>";
        	else
        		$selector_html.="<option value='".$per_type["fi_id"]."' $select_attr ><s>".$per_type["fv_name"]."</s></option>";
        }
        
        $file_size_limit = 1024*1024*1;//1M   unit:byte
		
    }else{
        header("Location:index.php");
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo $html_title;?></title>
        <?php echo $html_resource;?>
        <script>
            /******************** user define js ********************/
            $(document).ready(function(){
            	
                //ajax
                //filename write in template.js
                
                //新增
                $("input.add_btn").click(function(){
                	
                	var $this=$("#addPanel"); 
                    var add_name 		= $this.find("input[name='add_name']").val();
                    var add_type 		= $this.find("select[name='add_type']").val();
                    if( !chk_all_input("addPanel") ){
	                    return;
                    }
                    $.post(filename,{
                			query_type:	"brand_add",
							name:		add_name,
							type:		add_type
						},function(data){
							data=$.trim(data);
                    		if(data=="success"){
                    			alert("新增成功！");
	                    		location.href = location.href;
                    		}
                    		console.log(data)
                    });
	            });
                
                
        
                //編輯
                var $panel = $("#editPanel");
                function init(){
                	//刪除
	                $("input.del_btn").bind('click',function(){
	                    if(!confirm("確定刪除？"))return;
	                    var $this = $(this);
	                    var fi_id = $this.attr('fi_id');
	                    
	                    $this.unbind('click');
	                    $.post(filename,{
	                    		query_type:"brand_del",
	                    		fi_id:fi_id
							},function(data){
								data=$.trim(data);
								if(data=="success"){
			                        alert("已刪除！");
			                        location.href = location.href;
		                        }
		                        console.log(data)
	                    });
	                });
                
                	//$("#list_panel [data-open-dialog]").unbind("click");
	                $("#list_panel [data-open-dialog]").click(function(){
	                	
	                	var $dialog = $("#dialog_content");
	                	
	                	edit_id = $(this).attr('fi_id');
	                	var $data = $("#data_"+edit_id);
	                		
	                    //radio更新
	                	var arr_name = [];
	                    $dialog.find("input[type='radio']").each(function(){
		                    var name=$(this).prop("name").replace(/\W+/g, "");
		                    if( !in_array(name,arr_name) )
		                    	arr_name.push(name);
	                    })
	                    for(i=0; i<arr_name.length; i++){
	                    	var name=arr_name[i];
	                    	var radio_val = $data.attr(name);
							$dialog.find("input[name='"+name+"'][value='"+radio_val+"']").prop("checked",true);
	                    }
	                    //text
						$dialog.find("input[type='text']").each(function(){
							$(this).val($data.attr($(this).attr("name")));
						})
						
						//img
						$dialog.find("input.pic[type='file']").each(function(){
							var preview_id = $(this).prop('name')+"_preview";
							$("#"+preview_id).html($data.data(preview_id));
							img_setting($("#"+preview_id));
						});
						
						//select
						$dialog.find("select").each(function(){
							$(this).find("option[selected]").removeAttr("selected");
							$(this).find("option[value='"+$data.attr($(this).prop('name'))+"']").prop("selected",true);
						})
	                });
                }
                init()
                
                //及時更新暫存資料(hidden暫存，沒存進資料庫)
                $panel.find("input[type!='button'][type!='checkbox'][type!='radio']").keydown(function(){
	                $("#data_"+edit_id).attr($(this).prop("name"),$(this).val() );
                })
                	
                $panel.find("input[type='radio']").click(function(){
                    $("#data_"+edit_id).attr($(this).prop("name"),$(this).val() );
                });
                //select
                $panel.find("select").change(function(){
                    $("#data_"+edit_id).attr($(this).prop("name"),$(this).val() );
                });
                
                $panel.find("input.edit_btn").click(function(){
                	var $dialog = $("#dialog_content");
                    //權限
                    if(!chk_all_input("#dialog_container")){
	                    return;
                    }
                    var $data = $("#data_"+edit_id);
                    var active = $data.attr("edit_active");
					var name = $data.attr("edit_brand_name");
					var weights = $data.attr("edit_weights");
					var type = $data.attr("edit_type");
					var $a_file = $data.data("$a_file")||[];
					var $a_remove_file = $data.data("$a_remove_file")||[];
					var len = 0;
					
					var form_data = new FormData();
						form_data.append("query_type",	"brand_edit");
						form_data.append("fi_id",		edit_id);
						form_data.append("name",		name);
						form_data.append("type",		type);
						form_data.append("active",		active);
						form_data.append("weights",		weights);
						
					
					len = $a_file.length;
					for( j=0 ; j<len ; j++ ){
						form_data.append('file[]', $a_file[j]);
						form_data.append('file_path[]', $a_file[j].img_path);
					}
					len = $a_remove_file.length;
					for( j=0 ; j<len ; j++ ){
						form_data.append( "a_remove_file["+j+"][title]",$a_remove_file[j]["title"] );
						form_data.append( "a_remove_file["+j+"][path]",$a_remove_file[j]["path"] );
					}
                    
                    $.ajax({
							url: filename,
							dataType: 'text',
							cache: false,
							contentType: false,
							processData: false,
							data: form_data,                         
							type: 'post',
							success: function(data){
								if($.trim(data).indexOf("success")>-1){
			                        alert("已更新！");
			                        location.reload();
		                        }
		                        console.log(data)
							}
						});
                });
                
                //無span之圖片(原有)包裝後加上span[default]，點下會進入$a_remove_file[]，剩下的span(新加入)點擊後加入$a_file[]
                function img_setting($preview){
                	var total = $preview.find("img").length;
	                var $add_img_btn = $preview.parent().children("input[type='file']");
	                //console.log(total+"^^"+$add_img_btn.attr("max_len")+$preview.html());
	                if( total>=$add_img_btn.attr("max_len") ){
		                $add_img_btn.unbind("click");
		                $add_img_btn.hide()
	                }else{
		                $add_img_btn.bind("click");
		                $add_img_btn.show()
	                }
	                
	                var name	= $preview.attr("btn_name");
	                
                    var preview_id = name+"_preview";
                    
                    var $data 		= $("#data_"+edit_id);
                    
	                $preview.find("img:not([new])").each(function(index){
	                    $img = $(this);
	                    if( $img.parent().children("span").length==0 ){//原先圖片
	                    	$img.css('max-height','100px')
		                    //console.log("img_parent.html:\n"+$img.parent().html())
		                    $img.wrap("<div style='display:inline-block;text-align:center;margin-bottom:10px;'></div>");
                            $img.parent().append("<br/><span class='bg shadowRoundCorner' style='padding:3px;cursor:pointer;color:white'>delete</span>");
                        }
                    })
                    $preview.find("img").change(function(){
                    	$data.data(preview_id,$(this).parent().parent().html());
                    })
                    // img
					$preview.find("span:not([new])").click(function(){
						var $a_remove_file = $data.data("$a_remove_file")||[];
						$preview = $(this).parent().parent();
                        var tmp_arr = [];
                        $img = $(this).parent().find("img");
                        src = $img.prop("src")||"";
                        tmp_arr["src"]	 = src;
                        tmp_arr["path"]	 = src.substring(0,src.lastIndexOf('/')+1);
                        tmp_arr["title"] = src.substring(src.lastIndexOf('/')+1);
                        $a_remove_file.push(tmp_arr);
                        //console.log($a_remove_file)
                        //$a_remove_file.push(src.substring(src.lastIndexOf('/')+1))
                        $(this).parent().remove();
                        img_setting($preview)
                        $data.data("$a_remove_file",$a_remove_file);
                        $data.data(preview_id,$preview.html());
                    })
                    $preview.find("span[new]").click(function(){
                    	var $a_file = $data.data("$a_file");
                        $preview = $(this).parent().parent();
	                    $(this).parent().remove();
	                    var now_len = $a_file.length;
	                    for( k=0 ; k<now_len ; k++ ){
	                    	try{
	                    		var title=unescape($(this).parent().find("img").attr("title"));
                            	if( $a_file[k].name == title ){
                                    $a_file.splice(k,1);
                                    break;
                                }
                        	}catch(error){
                        	}
	                    }
	                    img_setting($preview)
	                    $data.data("$a_file",$a_file);
	                    $data.data(preview_id,$preview.html());
                    })
                    
                    $data.data(preview_id,$preview.html());
                }
                
                //加上預覽ＤＩＶ
                $panel.find("input[type='file'][class='pic'][name]").each(function(){
                	var $this = $(this);
                	var name = $this.prop("name");
	                var preview_id = name+"_preview";
	                if($("div#"+preview_id).length==0)
	                	$this.before("<div id='"+preview_id+"' btn_name='"+name+"'></div>");
                })
                
                //var $a_remove_file=[];//存放所有刪除檔案路徑
	            //var $a_file=[];//存放所有檔案
	                
                $panel.find("input[type='file'][class='pic'][name]").change(function(evt)
                {
                    var $btn	= $(this);
                    var name	= $btn.prop("name");
                    var height	= $btn.attr("height")||100;
               
                    var now_no	= $panel.attr('now_no');
                    var data_id = "data_"+edit_id;
                    var preview_id	= name+"_preview";
                       
                    var $data   		= $("#"+data_id);
                    var $preview		= $("#"+preview_id);
                    var $a_file = $data.data("$a_file")||[];
                    
                    var max_len = $btn.attr("max_len");
                    var new_len = $btn[0].files.length;
                    var old_len = $a_file.length;
                    $preview.html($data.data(preview_id)||"");
                    
                    for(var i=0, f; f=evt.target.files[i]; i++) {
                        if(!f.type.match('image.*')) {
                            continue;
                        }
                        var file_size_limit = "<?php echo $file_size_limit?>";
                        file_size_limit = file_size_limit==""? 0 : file_size_limit;
                        if( f.size > file_size_limit ){
	                        alert2(f.name+"檔案超過1MB，請重新上傳。");
	                        if(evt.target.files[i+1]==undefined){
		                        evt.target.value="";
	                        }
	                        continue;
                        }
                        
                        var reader = new FileReader();
                        reader.onload = (function(theFile) {
                            return function(e) {
	                            //new屬性作用在於，圖檔先進preview，img_setting會偵測到沒有span，給加上default span
	                            $preview.append('<img new class="thumb" src="'+e.target.result+'" title="'+escape(theFile.name)+'"/>');
                                $preview.find("img:last")[0].onload = function(){
                                    var $this = $(this);
                                    var $preview = $this.parent();
                                    var title = $this.attr("title");
                                    var onload_len = $preview.find("img").length;
                                    //var filename = this.width+"x"+this.height+"_@."+title.substring(title.lastIndexOf(".")+1);
                                    $this.css({
                                        "border":"1px solid gray",
                                        "margin":"5px"
                                    });
                                    //$this.attr("title",filename);
                                    $this.height(height);
                                    $this.wrap("<div style='display:inline-block;text-align:center;margin-bottom:10px;'></div>");
                                    $this.parent().append("<br/><span new class='bg shadowRoundCorner' style='padding:3px;cursor:pointer;color:white'>delete</span>");
                                    if(name=="brand_menu"){
                                    	theFile.img_path = "menu";	
                                    }else{
                                    	theFile.img_path = "logo";	
                                    }
                                    $a_file.push(theFile);
                                    var tmp_new_len =$a_file.length-old_len
                                    //console.log(+"onload_len:"+onload_len+"^^old_len:"+old_len+"^^new_len:"+new_len+"^^max_len:"+max_len);
                                    if( tmp_new_len == new_len || onload_len == max_len ){
	                                    img_setting($preview);
                                    	$data.data("$a_file",$a_file);
                                    	$data.data(preview_id,$preview.html());
                                    	//console.log(preview_id+"^^"+$preview.html())
	                                    $btn.val("");
                                    }
                                }
                            };
                        })(f);
                        reader.readAsDataURL(f);
                    }
                    
                });
                
                ////////////////
                $("#selector").change(function(){
                	
                	$("#add_type[selected]").removeAttr("selected");
                	$("#add_type").find("option[value='"+$(this).val()+"']").attr("selected",true);
                		
                	$("#list_panel").remove();
                	$("div.pager").remove();
                	$obj = $(this);
                    document.cookie = $("#subject").val()+"_brand="+$obj.val();
                	$.post(filename,{
                			query_type:	"get_info",
                			fi_id:				$obj.val(),
                            keyword:            $("#keyword").val(), 
							permissions2del:	<?php echo !$permissions2del?"false":"true";?>,
	                    	permissions2edit:	<?php echo !$permissions2edit?"false":"true";?>
						},function(data){
							data=$.trim(data);
							$table = $(data);
							$("#search").after($table);
							
							init_style( $("#list_panel,#list_panel *") );
							init();

                            $table.find(".combine").click(function(){
                                $btn = $(this);
                                var source_id = $btn.attr("fi_id");
                                var target_id = prompt("請輸入欲合併到的ID\t（目前項目將被刪除）");
                                if(source_id==target_id)
                                {
                                    alert2("不能與自己合併")
                                }
                                $.post(filename,{
                                        query_type: "combine",
                                        source_id:  $btn.attr("fi_id"),
                                        target_id:  target_id
                                    },function(data2){
                                        data2 = $.trim(data2);
                                        console.log(data2)
                                        if(data2.indexOf("success")>-1)
                                        {
                                            alert("已成功合併");
                                            location.reload();
                                        }

                                });
                            })
							
							//$table.find("[data-open-dialog]").unbind("click");
							//$table.find("[data-open-dialog]").bind("click");
                    });
                }).trigger("change");

                $("#keyword").keyup(function(e){
                    if(e.keyCode=="13"){
                        $("#selector").trigger("change");
                    }
                })
                $("#search_img").click(function(){
                    $("#selector").trigger("change");
                })
                $("#submit").click(function(){
                    $("#selector").trigger("change");
                })
                
            });
        </script>
        <style>
            /******************** user define css ********************/
            .search_img{
                background:url(images/public/search.png);
                -moz-background-size:contain;
                -webkit-background-size:contain;
                -o-background-size:contain;
                background-size:contain;
                background-repeat: no-repeat;
                vertical-align: text-top;
                width:20px;
                height:20px;
                border: 0;
                z-index:2;
                position:relative;
                left:-25px;
            }
        </style>
    </head>
    <body>
        <input type="hidden" id="subject" value="<?php echo $subject;?>"/>
        <div id="wrapper">
            <!-- ******************** header ******************** -->
            <div id="header">
                <h3><?php echo $html_title; ?></h3>
            </div>
            <!-- /.header -->
            <!-- ******************** body ******************** -->
            <div id="body">
                <div id="body_left">
                    <?php echo $menu; ?>
                </div>
                <!-- /.body_left -->
                <div id="body_right">
                	<?php
                        if($permissions2add){
                            echo "<input type='button' value='新增品牌' data-open-dialog='新增視窗'>";
                            echo "<div id='addPanel' data-dialog='新增視窗'>";
                            
                            echo "<table class='table-v'>";
                            echo 	"<tr><td>分類</td><td><select id='add_type' name='add_type'>
                        				$selector_html
                        			</select></td></tr>";
                            echo 	"<tr><td>品牌名稱</td><td><input type='text' name='add_name' id='add_name' class='must' /></td></tr>";
                            echo 	"<tr><td></td><td><input class='add_btn' type='button' value='送出'></td></tr>";
                            echo "</table>";
                            
                            echo "</div>";
                        }else{
                            echo "你無權新增品牌";
                        }
                                    
                    ?>
                    <?php
						echo "<br>";
	                ?>
                    <div id="search">
                        <div style='float:left;'>
                            <select id='selector'>
                            	<?php echo $selector_html;?>
                            </select>
                        </div>
                        <div style='float:left;'>
                            <div style'clear:both;'>
                                <input type='text' name='keyword' id='keyword' placeholder="請輸關鍵字" alt="可輸入 區域 或 描述 或 店家名稱"/>
                                <input type='button' name='search_img' id='search_img' class='search_img'/>
                                <!--<img src='images/search.png' height='20' valign='bottom'/>-->
                            </div>
                        </div>
                        <div style='float:left;'>
                            <input type='button' id="submit" value="查詢"/>
                        </div>
                    </div>
                    <br>
                    	
                    <div id="editPanel" data-dialog="編輯視窗">
                        <table class='table-v'>
                        	<tr>
                                <td>分類</td>
                                <td>
                        			<select id="edit_type" name="edit_type">
                        				<?php echo $selector_html; ?>
                        			</select>
                        		</td>
                            </tr>
                            <tr>
                                <td width="30%">LOGO</td>
                                <td><input type="file" class="pic" name="brand_logo" max_len='1'></td>
                            </tr>
                        	<tr>
                                <td>MENU</td>
                                <td><input type="file" class="pic" name="brand_menu" max_len='1'></td>
                            </tr>
                            <tr>
                                <td>品牌名稱</td>
                                <td><input type="text" name="edit_brand_name" id="edit_brand_name" /></td>
                            </tr>
                        	<tr>
                                <td>權重<br>(數字大者在前)</td>
                                <td><input type="text" name="edit_weights" id="edit_weights" /></td>
                            </tr>
                            <tr>
                                <td>啟用設定</td>
                                <td>
                                	<input type="radio" name="edit_active" id="edit_active_on" value="1">
                                	<label for="edit_active_on">啟用</label>
                                	<input type="radio" name="edit_active" id="edit_active_off" value="0">
                                	<label for="edit_active_off">停用</label>
                                </td>
                            </tr>
                            <tr><td></td><td><input type="button" value="儲存" class="edit_btn" style="cursor: pointer;"></td></tr>
                        </table>
                        
                    </div>
                    
                </div>
                <!-- /.body_right -->
            </div>
            <!-- /.body -->
            <!-- ******************** footer ******************** -->
            <div id="footer">
                <span><?php echo $html_copyright; ?></span>
            </div>
            <!-- /.footer -->
        </div>
        <!-- /.wrapper -->
    </body>
</html>
