<?php 
    session_start();
    include 'backend/template.php';
    
    if(!empty($_SESSION['admin']['login_user'])){
        
        //取資料
        $login_fi_id = $_SESSION['admin']['login_fi_id'];
        //$login_store = $_SESSION['admin']['login_store'];
        $login_name = $_SESSION['admin']['login_name'];
        $login_permissions = $_SESSION['admin']['login_permissions'];
        $login_user = $_SESSION['admin']['login_user'];
        $login_username = $_SESSION['admin']['login_username'];
        $login_title = $_SESSION['admin']['login_title'];
        //$login_php_vars = $_SESSION['admin']['login_php_vars'];
        $menu = $_SESSION['admin']['menu'];
        
        //取資料
        $permissions2visit	=	(strpos($login_permissions,"brand_list")!==false||$login_permissions=="all")?true:false;
        $permissions2add	=	(strpos($login_permissions,"brand_add")	!==false||$login_permissions=="all")?true:false;
        $permissions2edit	=	(strpos($login_permissions,"brand_edit")!==false||$login_permissions=="all")?true:false;
        $permissions2del	=	(strpos($login_permissions,"brand_del")	!==false||$login_permissions=="all")?true:false;
        
        include 'library/dba.php';
        $dba = new dba();
        $all_type = $dba->getAll("select fi_id,fv_name,fi_active from t_store_type order by fi_weights desc");
        $first_type = $all_type[0]["fi_id"];
        
        $brand = $_COOKIE["brand"];
        $selector_html="";
        foreach($all_type as $per_type){
            $select_attr = $brand==$per_type["fi_id"]?"selected":"";
        	if($per_type["fi_active"]==1)
        		$selector_html.="<option value='".$per_type["fi_id"]."' $select_attr >".$per_type["fv_name"]."</option>";
        	else
        		$selector_html.="<option value='".$per_type["fi_id"]."' $select_attr ><s>".$per_type["fv_name"]."</s></option>";
        }
        
        $file_size_limit = 1024*1024*1;//1M   unit:byte
		
    }else{
        header("Location:index.php");
    }
?>
<html>
    <head>
        <title><?php echo $html_title;?></title>
        <?php echo $html_resource;?>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.0/angular.min.js"></script>
    <script>
        var app = angular.module("App",[]);
        app.controller("manage_brand",function($scope){
            $scope.editing=false;
            $scope.modify = function(key){
                $scope.editing=true;
                $scope.edit_id=key;
                $scope.edit_name = self.school[key].name;
                $scope.edit_age = self.school[key].age;
                $scope.edit_weight = self.school[key].weight;
                $("#editPanel").show();
            };
            $scope.update = function(key){
                $scope.editing=false;
                self.school[key].name = $scope.edit_name;
                self.school[key].age = $scope.edit_age;
                self.school[key].weight = $scope.edit_weight;
                if(!in_array(self.modifyId,key)){
                    self.modifyId.push(key);
                }
            };
            var self = this;
            self.school = [
                {"name":"jack",
                 "age":18,
                 "weight":56}
        
                ,{"name":"john",
                 "age":20,
                 "weight":47}
        
                ,{"name":"mary",
                 "age":15,
                 "weight":43}
            ];
            self.modifyId = Array();
        });

        function in_array(arr,value){
            for(var x in arr){
                if(arr[x]==value){
                    return true;
                }
            }
            return false;
        }
    </script>
    <style>
        .edit_panel{
            position:absolute;
            top:0px;
            left:0px;
            background-color:red;
            width:100%;
            height:100%;
        }
        .search{
            width:20px;
        }
    </style>
    <meta charset="utf-8">
</head>
<body ng-app="App">
    <div id="wrapper">
        <!-- ******************** header ******************** -->
        <div id="header">
            <h3><?php echo $html_title; ?></h3>
        </div>
        <!-- /.header -->
        <!-- ******************** body ******************** -->
        <div id="body">
            <div id="body_left">
                <?php echo $menu; ?>
            </div>
            <!-- /.body_left -->
            <div id="body_right">
                <!----------add panel---------->
                <div ng-controller="manage_brand as ctrl">
                    <div class="edit_panel" ng-show="adding">
                        姓名：<input type="text" ng-model="edit_name"/>
                        <br>
                        年齡：<input type="text" ng-model="edit_age"/>
                        <br>
                        體重：<input type="text" ng-model="edit_weight"/>
                        <br>
                        <button ng-click="update(edit_id)">update</button>
                    </div>
                    <!----------list panel---------->
                    <table class='table-h' style='margin-top:10px'>
                        <tr>
                            <td>ID</td>
                            <td>
                                <input type="text" class="search" ng-model="search.name">姓名
                            </td>
                            <td><input type="text" class="search" ng-model="search.age">年齡</td>
                            <td><input type="text" class="search" ng-model="search.weight">體重</td>
                            <td>修改</td>
                        </tr>
                        <tr ng-repeat="(key,value) in ctrl.school| filter:search">
                            <td>{{key}}</td>
                            <td>{{value.name}}</td>
                            <td>{{value.age}}</td>
                            <td>{{value.weight}}</td>
                            <td><button ng-click="modify(key)">modify</button></td>
                         </tr>
                    </table>

                    <div id="editPanel" ng-show="editing">
                        <table class='table-v'>
                            <tr>
                                <td>分類</td>
                                <td>
                                    <select id="edit_type" name="edit_type">
                                        <?php echo $selector_html; ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td width="30%">LOGO</td>
                                <td><input type="file" class="pic" name="brand_logo" max_len='1'></td>
                            </tr>
                            <tr>
                                <td>MENU</td>
                                <td><input type="file" class="pic" name="brand_menu" max_len='1'></td>
                            </tr>
                            <tr>
                                <td>品牌名稱</td>
                                <td><input type="text" name="edit_brand_name" id="edit_brand_name" /></td>
                            </tr>
                            <tr>
                                <td>權重<br>(數字大者在前)</td>
                                <td><input type="text" name="edit_weights" id="edit_weights" /></td>
                            </tr>
                            <tr>
                                <td>啟用設定</td>
                                <td>
                                    <input type="radio" name="edit_active" id="edit_active_on" value="1">
                                    <label for="edit_active_on">啟用</label>
                                    <input type="radio" name="edit_active" id="edit_active_off" value="0">
                                    <label for="edit_active_off">停用</label>
                                </td>
                            </tr>
                            <tr><td></td><td><input type="button" value="儲存" class="edit_btn" style="cursor: pointer;"></td></tr>
                        </table>
                        
                    </div>
                </div>
            </div>
            <!-- /.body -->
            <!-- ******************** footer ******************** -->
            <div id="footer">
                <span><?php echo $html_copyright; ?></span>
            </div>
            <!-- /.footer -->
        </div>
        <!-- /.wrapper -->
    </div>
</body>
</html>