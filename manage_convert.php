<?php 
    session_start();
    include 'backend/template.php';
    //ini_set("display_errors", "On");
    //error_reporting(E_ALL);
    if(!empty($_SESSION['admin']['login_user'])){
        $login_fi_id = $_SESSION['admin']['login_fi_id'];
        //$login_store = $_SESSION['admin']['login_store'];
        $login_name = $_SESSION['admin']['login_name'];
        $login_permissions = $_SESSION['admin']['login_permissions'];
        $login_user = $_SESSION['admin']['login_user'];
        $login_username = $_SESSION['admin']['login_username'];
        $login_title = $_SESSION['admin']['login_title'];
        //$login_php_vars = $_SESSION['admin']['login_php_vars'];
        $menu = $_SESSION['admin']['menu'];
        
        include 'library/dba.php';
        $dba = new dba();
        $all_type = $dba->getAll("select fi_id,fv_name,fi_active from t_store_type order by fi_weights desc");
        
        $selector_html="";
        foreach($all_type as $per_type){
        	if($per_type["fi_active"]==1)
        		$selector_html.="<option value='".$per_type["fi_id"]."'>".$per_type["fv_name"]."</option>";
        	else
        		$selector_html.="<option value='".$per_type["fi_id"]."'><s>".$per_type["fv_name"]."</s></option>";
        }
        $all_data = $dba->getAll("select fi_id,fv_address from t_store where ff_latitude=0 or ff_longitude=0 order by RAND();");
        
        
        $file_path="download/convert.xlsx";
        
        if(is_array($_FILES["upload_xls"])){
	        include_once "model/convert.php";
			$convert = new Convert();
			$convert->xls2db();
		}else{
			@unlink($file_path);
		}
        
    }else{
        header("Location:index.php");
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo $html_title;?></title>
        <?php echo $html_resource;?>
        <script>
            /******************** user define js ********************/
            $(document).ready(function(){
    			$("form").submit(function(){
    				if($("#upload_type").val()==0){
    					alert("請選擇分類");
    					return false;
    				}
    			})
    			var num = 0;
    			var timer = setInterval(function(){
    				//console.log($("input[type='hidden']").length+"^^"+num);
    				if($("input[type='hidden']").length>num){
	    				$data = $("input[type='hidden']").eq(num)
	    				var fi_id = $data.attr("fi_id");
	    				var address = $data.attr("address");
	    				getLatLng(address,fi_id);
	    				num++;
    				}else{
    					clearInterval(timer);
    				}
    			},1300);
    			function getLatLng(address,fi_id) {
                	
					var geocoder = new google.maps.Geocoder();
					var result = Array();
					geocoder.geocode( { 'address': address, 'region': 'uk' }, function(results, status) {
					     if (status == google.maps.GeocoderStatus.OK) {
					         result["lat"] = results[0]["geometry"]["location"]["k"]||0;
					         result["lng"] = results[0]["geometry"]["location"]["D"]||0;
					         //
					     } else {
					     	 result["lat"] = 0;
					         result["lng"] = 0;
					         result["status"] = "Unable to find address: " + status;
					     }
                         var form_data = new FormData();
					     	form_data.append("query_type",	"getLatLng");
							form_data.append("fi_id",		fi_id);
							form_data.append("address",		address);
							form_data.append("lat",			result["lat"]);
							form_data.append("lng",			result["lng"]);
						
	                    $.ajax({
								url: filename,
								dataType: 'text',
								cache: false,
								contentType: false,
								processData: false,
								data: form_data,                         
								type: 'post',
								success: function(data){
									//alert2(data)
									$("#body_right").append(data+"<br>");
								}
						});
				    });
				}
    		})
        </script>
        <style>
            /******************** user define css ********************/
			
        </style>
    </head>
    <body>
        <div id="wrapper">
            <!-- ******************** header ******************** -->
            <div id="header">
                <h3><?php echo $html_title; ?></h3>
            </div>
            <!-- /.header -->
            <!-- ******************** body ******************** -->
            <div id="body">
                <div id="body_left">
                    <?php echo $menu; ?>
                </div>
                <!-- /.body_left -->
                <div id="body_right">
                    <?php
						require_once "view/upload_convert_xls.php";
						echo $convert->html;
						if(file_exists($file_path))
							echo "<h3><a href='download/convert.xlsx'>重複的Excel</a>點右鍵另存</h3><br>";
						
						foreach($all_data as $data){
							$fi_id = $data["fi_id"];
							$address = $data["fv_address"];
							echo "<input type='hidden' id='data_{$fi_id}' fi_id='$fi_id' address='$address'>";
						}
					?>
                </div>
                <!-- /.body_right -->
            </div>
            <!-- /.body -->
            <!-- ******************** footer ******************** -->
            <div id="footer">
                <span><?php echo $html_copyright; ?></span>
            </div>
            <!-- /.footer -->
        </div>
        <!-- /.wrapper -->
    </body>
</html>
