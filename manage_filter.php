<?php 
    session_start();
    include 'backend/template.php';
    
    if(!empty($_SESSION['admin']['login_user'])){
        
        $login_fi_id = $_SESSION['admin']['login_fi_id'];
        //$login_store = $_SESSION['admin']['login_store'];
        $login_name = $_SESSION['admin']['login_name'];
        $login_permissions = $_SESSION['admin']['login_permissions'];
        $login_user = $_SESSION['admin']['login_user'];
        $login_username = $_SESSION['admin']['login_username'];
        $login_title = $_SESSION['admin']['login_title'];
        //$login_php_vars = $_SESSION['admin']['login_php_vars'];
        $menu = $_SESSION['admin']['menu'];
        
        include 'library/dba.php';
        $dba = new dba();
        
        $file_path="download/filter.xlsx";
        
        if(is_array($_FILES["upload_xls"])){
	        require_once "model/filter.php";
			$filter = new Filter();
			$filter->xls2db();
		}else{
			//@unlink($file_path);
		}
        
    }else{
        header("Location:index.php");
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo $html_title;?></title>
        <?php echo $html_resource;?>
        <script>
            /******************** user define js ********************/
            
        </script>
        <style>
            /******************** user define css ********************/
			
        </style>
    </head>
    <body>
        <div id="wrapper">
            <!-- ******************** header ******************** -->
            <div id="header">
                <h3><?php echo $html_title; ?></h3>
            </div>
            <!-- /.header -->
            <!-- ******************** body ******************** -->
            <div id="body">
                <div id="body_left">
                    <?php echo $menu; ?>
                </div>
                <!-- /.body_left -->
                <div id="body_right">
                    <?php
						require_once "view/upload_xls.php";
						echo $filter->html;
						if(file_exists($file_path))
							echo "<h3><a href='download/filter.xlsx'>過濾完的Excel</a>點右鍵另存</h3><br>";
					?>
                </div>
                <!-- /.body_right -->
            </div>
            <!-- /.body -->
            <!-- ******************** footer ******************** -->
            <div id="footer">
                <span><?php echo $html_copyright; ?></span>
            </div>
            <!-- /.footer -->
        </div>
        <!-- /.wrapper -->
    </body>
</html>
