<?php 
    session_start();
    include 'backend/template.php';
    
    if(!empty($_SESSION['admin']['login_user'])){
        
        //取資料
        $login_fi_id = $_SESSION['admin']['login_fi_id'];
        //$login_store = $_SESSION['admin']['login_store'];
        $login_name = $_SESSION['admin']['login_name'];
        $login_permissions = $_SESSION['admin']['login_permissions'];
        $login_user = $_SESSION['admin']['login_user'];
        $login_username = $_SESSION['admin']['login_username'];
        $login_title = $_SESSION['admin']['login_title'];
        //$login_php_vars = $_SESSION['admin']['login_php_vars'];
        $menu = $_SESSION['admin']['menu'];
        
        //取資料
        $permissions2visit	=	(strpos($login_permissions,"brand_list")!==false||$login_permissions=="all")?true:false;
        $permissions2add	=	(strpos($login_permissions,"brand_add")	!==false||$login_permissions=="all")?true:false;
        $permissions2edit	=	(strpos($login_permissions,"brand_edit")!==false||$login_permissions=="all")?true:false;
        $permissions2del	=	(strpos($login_permissions,"brand_del")	!==false||$login_permissions=="all")?true:false;
        
        include_once 'library/dba.php';
        $dba = new dba();
        $all_type = $dba->getAll("select fi_id,fv_name,fi_active from t_store_type order by fi_weights desc");
        
        $selector_html="";
        foreach($all_type as $per_type){
        	if($per_type["fi_active"]==1)
        		$selector_html.="<option value='".$per_type["fi_id"]."'>".$per_type["fv_name"]."</option>";
        	else
        		$selector_html.="<option value='".$per_type["fi_id"]."'><s>".$per_type["fv_name"]."</s></option>";
        }
    }else{
        header("Location:index.php");
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo $html_title;?></title>
        <?php echo $html_resource;?>
        <script>
            /******************** user define js ********************/
            $(document).ready(function(){
            	
                $("#search").keydown(function(e){
                	if(e.which==13){
                		show_list($("#search").val())
                	}
                })
                $("#search_img").click(function(){show_list($("#search").val());});
                
                function show_list(search){
                	var search=search||"";
                		
                	$("#list_panel").remove();
                	$("div.pager").remove();
                	$obj = $("#selector");
                	$.post(filename,{
                			query_type:	"get_info",
                			search:				search
						},function(data){
							data=$.trim(data);
							$table = $(data);
							$obj.after("<br>");
							$obj.after($table);
							
							init_style( $("#list_panel,#list_panel *") );
                    });
                }
            });
            function ValidateNumber(e, pnumber)
			{
			    if (!/^\d+$/.test(pnumber))
			    {
			        e.value = /^\d+/.exec(e.value);
			    }
			    return false;
			}
        </script>
        <style>
            /******************** user define css ********************/
			.search_img{
				background:url(images/public/search.png);
				-moz-background-size:contain;
				-webkit-background-size:contain;
				-o-background-size:contain;
				background-size:contain;
				background-repeat: no-repeat;
				vertical-align: text-top;
				width:20px;
				height:20px;
				border: 0;
				z-index:2;
				position:relative;
				left:-25px;
			}
        </style>
    </head>
    <body>
        <div id="wrapper">
            <!-- ******************** header ******************** -->
            <div id="header">
                <h3><?php echo $html_title; ?></h3>
            </div>
            <!-- /.header -->
            <!-- ******************** body ******************** -->
            <div id="body">
                <div id="body_left">
                    <?php echo $menu; ?>
                </div>
                <!-- /.body_left -->
                <div id="body_right">
                	
                    <?php
						echo "<br>";
	                ?>
                	<div style='float:left;'>
                    	<div style'clear:both;'>
                    		<input type='text' name='search' id='search' placeholder="請輸關鍵字" alt="可輸入店家編號 或 店家電話 或 55104分機 或 撥打者電話"/>
                    		<input type='button' name='search_img' id='search_img' class='search_img'/>
                    		<!--<img src='images/search.png' height='20' valign='bottom'/>-->
                    	</div>
                    </div>
                    <div id='selector' style='clear:both;height:10px'></div>
	                
                </div>
                <!-- /.body_right -->
            </div>
            <!-- /.body -->
            <!-- ******************** footer ******************** -->
            <div id="footer">
                <span><?php echo $html_copyright; ?></span>
            </div>
            <!-- /.footer -->
        </div>
        <!-- /.wrapper -->
    </body>
</html>
