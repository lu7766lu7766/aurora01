<?php 
    session_start();
    include 'backend/template.php';
    
    if(!empty($_SESSION['admin']['login_user'])){
        
        //取資料
        $login_fi_id = $_SESSION['admin']['login_fi_id'];
        //$login_store = $_SESSION['admin']['login_store'];
        $login_name = $_SESSION['admin']['login_name'];
        $login_permissions = $_SESSION['admin']['login_permissions'];
        $login_user = $_SESSION['admin']['login_user'];
        $login_username = $_SESSION['admin']['login_username'];
        $login_title = $_SESSION['admin']['login_title'];
        //$login_php_vars = $_SESSION['admin']['login_php_vars'];
        $menu = $_SESSION['admin']['menu'];
        
        //取資料
        $permissions2visit	=	(strpos($login_permissions,"member_list")!==false||$login_permissions=="all")?true:false;
        $permissions2add	=	(strpos($login_permissions,"member_add")	!==false||$login_permissions=="all")?true:false;
        $permissions2edit	=	(strpos($login_permissions,"member_edit")!==false||$login_permissions=="all")?true:false;
        $permissions2del	=	(strpos($login_permissions,"member_del")	!==false||$login_permissions=="all")?true:false;
        
        include_once 'library/dba.php';
        $dba = new dba();
        $all_type = $dba->getAll("select fi_id,fv_name,fi_active from t_store_type order by fi_weights desc");
        
        $selector_html="";
        foreach($all_type as $per_type){
        	if($per_type["fi_active"]==1)
        		$selector_html.="<option value='".$per_type["fi_id"]."'>".$per_type["fv_name"]."</option>";
        	else
        		$selector_html.="<option value='".$per_type["fi_id"]."'><s>".$per_type["fv_name"]."</s></option>";
        }
    }else{
        header("Location:index.php");
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo $html_title;?></title>
        <?php echo $html_resource;?>
        <script>
            /******************** user define js ********************/
            $(document).ready(function(){
            	
                //ajax
                //filename write in template.js
                
                //編輯
                var $panel = $("#editPanel");
                function init(){
                	//刪除
                    //$("input.del_btn").unbind('click');
	                $("input.del_btn").bind('click',function(){
	                    if(!confirm("確定刪除？"))return;
	                    var $this = $(this);
	                    var fi_id = $this.attr('fi_id');
	                    
	                    $this.unbind('click');
	                    $.post(filename,{
	                    		query_type:"member_del",
	                    		fi_id:fi_id
							},function(data){
								data=$.trim(data);
								if(data=="success"){
			                        alert("已刪除！");
			                        location.href = location.href;
		                        }
		                        console.log(data)
	                    });
	                });
                
                	//$("#list_panel [data-open-dialog]").unbind("click");
	                $("#list_panel [data-open-dialog]").click(function(){
	                	
	                	var $dialog = $("#dialog_content");
	                	
	                	edit_id = $(this).attr('fi_id');
                        //console.log($(this));
                        //console.log(edit_id);
	                	var $data = $("#data_"+edit_id);
	                	
                        $dialog.find("input:first").focus();

	                    //text
						$dialog.find("input[type='text']").each(function(){
							$(this).val($data.attr($(this).attr("name")));
						})

                        if($(this).is(".recode")){
                            var fv_phone = $(this).attr("fv_phone");
                            $.post(filename,{
                                    query_type:"bonus_log",
                                    fv_phone:fv_phone
                                },function(data){
                                    //data=$.trim(data);
                                    // $dialog.find("#bonus_log_content").html(data);
                                    var len = data.length;
                                    var table_html = "";
                                    table_html += "<table border='1'><tr><td>編號</td><td>點數(增加為正，扣點為負)</td><td>時間</td><td>備註</td></tr>";

                                    for (var i=0; i<len; i++) {
                                        var member = data[i];
                                        var mul = member["fi_type"]>0? 1: -1;

                                        table_html += "<tr>";
                                        table_html += "<td>"+(i+1)+"</td>";
                                        table_html += "<td>"+(mul*member["fi_bonus"])+"</td>";
                                        table_html += "<td>"+member["ft_time"]+"</td>";
                                        table_html += "<td>"+member["fv_memo"]+"</td>";
                                        table_html += "</tr>";
                                    };
                                    table_html += "</table>";
                                    // $table = $(table_html);
                                    $dialog.find("#bonus_log_content").html(table_html);
                            },"json");
                        }
	                });
                }
                init()
                
                //及時更新暫存資料(hidden暫存，沒存進資料庫)
                $panel.find("input[type!='button'][type!='checkbox'][type!='radio']").keyup(function(){
	                $("#data_"+edit_id).attr($(this).prop("name"),$(this).val() );
                })
                
                $panel.find("input.edit_btn").click(function(e,lat,lng){
                	var $dialog = $("#dialog_content");
                    //權限
                    if(!chk_all_input("#dialog_container")){
	                    return;
                    }
                    var $data = $("#data_"+edit_id);
                    var phone = $data.attr("phone");
					var bonus = $data.attr("edit_bonus");
                    var memo  = $data.attr("edit_memo");
					
					var form_data = new FormData();
						form_data.append("query_type",	"member_edit");
						form_data.append("fi_id",		edit_id);
                        form_data.append("phone",       phone);
                        form_data.append("bonus",       bonus);
                        form_data.append("memo",        memo);
					
                    $.ajax({
							url: filename,
							dataType: 'text',
							cache: false,
							contentType: false,
							processData: false,
							data: form_data,                         
							type: 'post',
							success: function(data){
                                console.log(data)
								if($.trim(data)=="success"){
			                        alert("已更新！");
			                        location.reload();
		                        }
		                        
							}
						});
                });

                $("#search").keydown(function(e){
                    if(e.which==13){
                        show_list($("#search").val())
                    }
                })
                $("#search_img").click(function(){show_list($("#search").val());});
                
                function show_list(search){
                    var search=search||"";
                        
                    $("#list_panel").remove();
                    $("div.pager").remove();
                    $obj = $("#searchor");
                    $.post(filename,{
                            query_type: "get_info",
                            search:     search,
                            permissions2edit: <?php echo $permissions2edit;?>,
                            permissions2del:  <?php echo $permissions2del;?>
                        },function(data){
                            data=$.trim(data);
                            $table = $(data);
                            $obj.after("<br>");
                            $obj.after($table);
                            
                            init_style( $("#list_panel,#list_panel *") );
                            init();
                    });
                }

                show_list();
                
            });
            function ValidateNumber(e, pnumber)
			{
			    if (!/^[\d-]+$/.test(pnumber))
			    {
			        e.value = /^\d+/.exec(e.value);
			    }
			    return false;
			}
        </script>
        <style>
            /******************** user define css ********************/
			.search_img{
				background:url(images/public/search.png);
				-moz-background-size:contain;
				-webkit-background-size:contain;
				-o-background-size:contain;
				background-size:contain;
				background-repeat: no-repeat;
				vertical-align: text-top;
				width:20px;
				height:20px;
				border: 0;
				z-index:2;
				position:relative;
				left:-25px;
			}
        </style>
    </head>
    <body>
        <div id="wrapper">
            <!-- ******************** header ******************** -->
            <div id="header">
                <h3><?php echo $html_title; ?></h3>
            </div>
            <!-- /.header -->
            <!-- ******************** body ******************** -->
            <div id="body">
                <div id="body_left">
                    <?php echo $menu; ?>
                </div>
                <!-- /.body_left -->
                <div id="body_right">
                	
                    <div id='searchor' style='float:right;'>
                    	<div style'clear:both;'>
                    		<input type='text' name='search' id='search' placeholder="請輸手機或Email" />
                    		<input type='button' name='search_img' id='search_img' class='search_img'/>
                    		<!--<img src='images/search.png' height='20' valign='bottom'/>-->
                    	</div>
                    </div>
                        
                    <div id="editPanel" data-dialog="編輯視窗">
                        <table class='table-v'>
                        	<tr>
                                <td width="30%">點數調整<br>(輸入負值為扣點)</td>
                                <td>
                        			<input type='text' id="edit_bonus" name="edit_bonus" onkeyup='return ValidateNumber(this,value)' must>
                        		</td>
                            </tr>
                        	<tr>
                                <td>備註</td>
                                <td>
                                    <input type="text" name="edit_memo" id="edit_memo" alt='' />
                                </td>
                            </tr>
                        	<tr><td></td><td><input type="button" value="儲存" class="edit_btn" style="cursor: pointer;"></td></tr>
                        </table>
                    </div>

                    <div id="editPanel" data-dialog="紀錄">
                        <table class='table-v'>
                            <tr>
                                <td width="30%">紀錄</td>
                                <td id="bonus_log_content">
                                </td>
                            </tr>
                        </table>
                    </div>
                    
                </div>
                <!-- /.body_right -->
            </div>
            <!-- /.body -->
            <!-- ******************** footer ******************** -->
            <div id="footer">
                <span><?php echo $html_copyright; ?></span>
            </div>
            <!-- /.footer -->
        </div>
        <!-- /.wrapper -->
    </body>
</html>
