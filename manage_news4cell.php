<?php 
    session_start();
    include 'backend/template.php';
    
    if(!empty($_SESSION['admin']['login_user'])){
        
        //取資料
        $login_fi_id = $_SESSION['admin']['login_fi_id'];
        //$login_store = $_SESSION['admin']['login_store'];
        $login_name = $_SESSION['admin']['login_name'];
        $login_permissions = $_SESSION['admin']['login_permissions'];
        $login_user = $_SESSION['admin']['login_user'];
        $login_username = $_SESSION['admin']['login_username'];
        $login_title = $_SESSION['admin']['login_title'];
        //$login_php_vars = $_SESSION['admin']['login_php_vars'];
        $menu = $_SESSION['admin']['menu'];
        
        $subject = "news4cell";
        //取資料
        $permissions2visit	=	(strpos($login_permissions,$subject."_list")!==false||$login_permissions=="all")?true:false;
        $permissions2add	=	(strpos($login_permissions,$subject."_add")	!==false||$login_permissions=="all")?true:false;
        $permissions2edit	=	(strpos($login_permissions,$subject."_edit")!==false||$login_permissions=="all")?true:false;
        $permissions2del	=	(strpos($login_permissions,$subject."_del")	!==false||$login_permissions=="all")?true:false;
        
        include 'library/dba.php';
        $dba = new dba();
        require_once "library/class_pager2.php";
        $pager = new Pager();
        $all_data = $pager->query("select fi_id,fv_images,fi_date_active,fd_start,fd_end,fi_active,fi_weights
                                  from t_news4cell
                                  where fi_delete<>'1'
                                  order by fi_weights desc");

        $file_size_limit = 1024*1024*1;//1M   unit:byte
        $image_path = "images/news4cell/";
		
    }else{
        header("Location:index.php");
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo $html_title;?></title>
        <link rel="stylesheet" type="text/css" href="backend/css/jquery.datetimepicker.css">
        <?php echo $html_resource;?>
        <script type="text/javascript" src="backend/js/jquery.datetimepicker.js"></script>
        <script type="text/javascript" src="backend/js/date_setting.js"></script>
        <script>
            /******************** user define js ********************/
            $(document).ready(function(){

                var subject = "<?php echo $subject?>";
                var image_path = "<?php echo $image_path?>";
                var a_field = [
                    {
                        field:"images",
                        field_txt:"圖片上傳",
                        type:"pic",
                        attr:{
                            max_len:1,
                            must:"",
                            image_path:image_path
                        }
                    },
                    {
                        field:"active",
                        field_txt:"前台顯示",
                        type:"radio",
                        value:{"true":"顯示","false":"不顯示"},
                        default_val:"true"
                    },
                    {
                        field:"date_active",
                        field_txt:"日期限制<br>如果選擇請用請輸入起始與結束日期",
                        type:"radio",
                        value:{"true":"啟用","false":"停用"},
                        default_val:"false"
                    },
                    {
                        field:"start",
                        field_txt:"起始日期",
                        type:"text",
                        class:["date"],
                        attr:{
                            "readonly":""
                        }
                    },
                    {
                        field:"end",
                        field_txt:"結束日期",
                        type:"text",
                        class:["date"],
                        attr:{
                            "readonly":""
                        }
                    },
                    {
                        field:"weights",
                        field_txt:"權重",
                        type:"text",
                        attr:{
                            value:0
                        }
                    }
                ];

                function view_init(){
                    if($("#addPanel")){
                        var $table = $("#addPanel").find("table");
                        $table.addClass("table-v");
                        for(var k_field in a_field){
                            $table.append(build_view(a_field[k_field],"add"));
                        }
                        $table.append("<tr><td></td><td><input type='button' class='add_btn' value='送出' style='cursor:pointer;'></td></tr>");
                        init_style( $table );
                    }
                    if($("#editPanel")){
                        var $table = $("#editPanel").find("table");
                        $table.addClass("table-v");
                        for(var k_field in a_field){
                            $table.append(build_view(a_field[k_field],"edit"));
                        }
                        $table.append("<tr><td></td><td><input type='button' class='edit_btn' value='儲存' style='cursor:pointer;'></td></tr>");
                        init_style( $table );
                    }
                }
                view_init();
                function build_view(field,type){
                    type = type || "add";
                    var html = "<tr><td width='30%'>"+field.field_txt+"</td><td>";
                    var idname = type+"_"+field.field;
                    var class_val = "";
                    for(var k_class in field.class){
                        class_val += field.class[k_class]+" ";
                    }
                    var attr_val = "";
                    for(var k_attr in field.attr){
                        attr_val += k_attr+"='"+field.attr[k_attr]+"' ";
                    }
                    switch(field.type){
                        case "text":
                            html += "<input type='text' id='"+idname+"' name='"+idname+"' class='"+class_val+"' "+attr_val+"/>";
                            break;
                        case "pic":
                            html += "<input type='file' id='"+idname+"' name='"+idname+"' class='pic' "+attr_val+"/>";
                            break;
                        case "radio":
                            for(var k_val in field.value){
                                var check_val = field.default_val==k_val? "checked='checked'": "";
                                html += "<label>"+"<input type='radio' id='"+idname+k_val+"' name='"+idname+"' "+check_val+" class='"+class_val+"' "+attr_val+" value='"+k_val+"'/>"+field.value[k_val]+"</label>";
                            }
                            break;
                    }
                    html += "</td></tr>";

                    return $(html);
                }

                //ajax
                //filename write in template.js

                $("#add_windows").click(function(){
                    var $dialog = $("#dialog_content");
                    edit_id = "add";
                    var $data = $("#data_"+edit_id);
                    $dialog.find("input.pic[type='file']").each(function(){
                        var preview_id = $(this).prop('name')+"_preview";
                        $("#"+preview_id).html($data.data(preview_id));
                        img_setting($("#"+preview_id));
                    });
                })

                //新增
                $("input.add_btn").click(function(){

                    if( !chk_all_input("addPanel") ){
                        return;
                    }

                    var form_data = new FormData();
                    form_data.append("query_type",	subject+"_add");
                	var $this=$("#addPanel");
                    var $data = $("#data_add");
                    for(var key in a_field){
                        var field = a_field[key].field;

                        if(a_field[key].type=="pic"){
                            var $a_file = $data.data("$a_file")||[];
                            var $a_remove_file = $data.data("$a_remove_file")||[];
                            var len = 0;
                            len = $a_file.length;
                            for( var j=0 ; j<len ; j++ ){
                                form_data.append('file[]', $a_file[j]);
                                form_data.append('file_path[]', $a_file[j].img_path);
                            }
                        }else{
                            form_data.append(field,$this.find("input[name='add_"+field+"']").val());
                        }
                    }

                    $.ajax({
                        url: filename,
                        dataType: 'text',
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function(data){
                            if($.trim(data).indexOf("success")>-1){
                                alert("已新增！");
                                location.reload();
                            }
                            console.log(data)
                        }
                    });
	            });

                //刪除
                $("input.del_btn").bind('click',function(){
                    if(!confirm("確定刪除？"))return;
                    var $this = $(this);
                    var fi_id = $this.attr('fi_id');

                    $this.unbind('click');
                    $.post(filename,{
                        query_type:subject+"_del",
                        fi_id:fi_id
                    },function(data){
                        data=$.trim(data);
                        if(data=="success"){
                            alert("已刪除！");
                            location.href = location.href;
                        }
                        console.log(data)
                    });
                });

                $("input.edit_btn").click(function(){

                    //權限
                    if(!chk_all_input("#dialog_container")){
                        return;
                    }

                    var $dialog = $("#dialog_content");
                    var form_data = new FormData();
                    form_data.append("query_type",	subject+"_edit");
                    form_data.append("fi_id",	edit_id);
                    var $data = $("#data_"+edit_id);
                    var $a_file = $data.data("$a_file")||[];
                    var $a_remove_file = $data.data("$a_remove_file")||[];

                    for(var key in a_field){
                        var field = a_field[key].field;
                        if(a_field[key].type=="pic"){
                            var len = 0;
                            len = $a_file.length;
                            for( j=0 ; j<len ; j++ ){
                                form_data.append('file[]', $a_file[j]);
                                form_data.append('file_path[]', $a_file[j].img_path);
                            }
                            len = $a_remove_file.length;
                            for( var j=0 ; j<len ; j++ ){
                                form_data.append( "a_remove_file["+j+"][title]",$a_remove_file[j]["title"] );
                                form_data.append( "a_remove_file["+j+"][path]",$a_remove_file[j]["path"] );
                            }
                        }else{
                            form_data.append(field,$data.attr("edit_"+field+""));
                        }
                    }
                    $.ajax({
                        url: filename,
                        dataType: 'text',
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function(data){
                            if($.trim(data).indexOf("success")>-1){
                                alert("已更新！");
                                location.reload();
                            }
                            console.log(data)
                        }
                    });
                });

                //編輯
                var $panel = $("#editPanel");

                function init(){

                    dataInit();
                	//$("#list_panel [data-open-dialog]").unbind("click");
	                $("#list_panel [data-open-dialog]").click(function(){
	                	
	                	var $dialog = $("#dialog_content");
	                	
	                	edit_id = $(this).attr('fi_id');
	                	var $data = $("#data_"+edit_id);
	                		
	                    //radio更新
	                    $dialog.find("input[type='radio']").each(function(){
		                    var name=$(this).attr("name");
                            var radio_val = $data.attr(name);
                            $dialog.find("input[name='"+name+"'][value='"+radio_val+"']").prop("checked",true);
	                    });
	                    //text
						$dialog.find("input[type='text']").each(function(){
							$(this).val($data.attr($(this).attr("name")));
						})

                        //date
                        $dialog.find(".date").change(function(){
                            $data.attr($(this).attr("name"),$(this).val());
                        })
						
						//img
						$dialog.find("input.pic[type='file']").each(function(){
							var preview_id = $(this).prop('name')+"_preview";
							$("#"+preview_id).html($data.data(preview_id));
                            console.log(preview_id)
							img_setting($("#"+preview_id));
						});
						
						//select
						$dialog.find("select").each(function(){
							$(this).find("option[selected]").removeAttr("selected");
							$(this).find("option[value='"+$data.attr($(this).prop('name'))+"']").prop("selected",true);
						})
	                });
                }
                init();
                
                //及時更新暫存資料(hidden暫存，沒存進資料庫)
                $panel.find("input[type!='button'][type!='checkbox'][type!='radio']").keydown(function(){
	                $("#data_"+edit_id).attr($(this).prop("name"),$(this).val() );
                })
                	
                $panel.find("input[type='radio']").click(function(){
                    $("#data_"+edit_id).attr($(this).prop("name"),$(this).val() );
                });
                //select
                $panel.find("select").change(function(){
                    $("#data_"+edit_id).attr($(this).prop("name"),$(this).val() );
                });

                //無span之圖片(原有)包裝後加上span[default]，點下會進入$a_remove_file[]，剩下的span(新加入)點擊後加入$a_file[]
                function img_setting($preview){
                	var total = $preview.find("img").length;
	                var $add_img_btn = $preview.parent().children("input[type='file']");
	                //console.log(total+"^^"+$add_img_btn.attr("max_len")+$preview.html());
	                if( total>=$add_img_btn.attr("max_len") ){
		                $add_img_btn.unbind("click");
		                $add_img_btn.hide()
	                }else{
		                $add_img_btn.bind("click");
		                $add_img_btn.show()
	                }
	                
	                var name	= $preview.attr("btn_name");
	                
                    var preview_id = name+"_preview";
                    
                    var $data 		= $("#data_"+edit_id);
                    
	                $preview.find("img:not([new])").each(function(index){
	                    $img = $(this);
	                    if( $img.parent().children("span").length==0 ){//原先圖片
	                    	$img.css('max-height','100px')
		                    //console.log("img_parent.html:\n"+$img.parent().html())
		                    $img.wrap("<div style='display:inline-block;text-align:center;margin-bottom:10px;'></div>");
                            $img.parent().append("<br/><span class='bg shadowRoundCorner' style='padding:3px;cursor:pointer;color:white'>delete</span>");
                        }
                    })
                    $preview.find("img").change(function(){
                    	$data.data(preview_id,$(this).parent().parent().html());
                    })
                    // img
					$preview.find("span:not([new])").click(function(){
						var $a_remove_file = $data.data("$a_remove_file")||[];
						$preview = $(this).parent().parent();
                        var tmp_arr = [];
                        $img = $(this).parent().find("img");
                        src = $img.prop("src")||"";
                        tmp_arr["src"]	 = src;
                        tmp_arr["path"]	 = src.substring(0,src.lastIndexOf('/')+1);
                        tmp_arr["title"] = src.substring(src.lastIndexOf('/')+1);
                        $a_remove_file.push(tmp_arr);
                        //console.log($a_remove_file)
                        //$a_remove_file.push(src.substring(src.lastIndexOf('/')+1))
                        $(this).parent().remove();
                        img_setting($preview)
                        $data.data("$a_remove_file",$a_remove_file);
                        $data.data(preview_id,$preview.html());
                    })
                    $preview.find("span[new]").click(function(){
                    	var $a_file = $data.data("$a_file");
                        $preview = $(this).parent().parent();
	                    $(this).parent().remove();
	                    var now_len = $a_file.length;
	                    for( k=0 ; k<now_len ; k++ ){
	                    	try{
	                    		var title=unescape($(this).parent().find("img").attr("title"));
                            	if( $a_file[k].name == title ){
                                    $a_file.splice(k,1);
                                    break;
                                }
                        	}catch(error){
                        	}
	                    }
	                    img_setting($preview)
	                    $data.data("$a_file",$a_file);
	                    $data.data(preview_id,$preview.html());
                    })
                    
                    $data.data(preview_id,$preview.html());
                }
                
                //加上預覽ＤＩＶ
                $("input[type='file'][class='pic'][name]").each(function(){
                	var $this = $(this);
                	var name = $this.prop("name");
	                var preview_id = name+"_preview";
	                if($("div#"+preview_id).length==0)
	                	$this.before("<div id='"+preview_id+"' btn_name='"+name+"'></div>");
                })
                
                //var $a_remove_file=[];//存放所有刪除檔案路徑
	            //var $a_file=[];//存放所有檔案
	                
                $("input[type='file'][class='pic'][name]").change(function(evt)
                {
                    var $btn	= $(this);
                    var name	= $btn.attr("name");
                    var height	= $btn.attr("height")||100;
                    var image_path	= $btn.attr("image_path");
               
                    var now_no	= $panel.attr('now_no');
                    var data_id = "data_"+edit_id;
                    var preview_id	= name+"_preview";
                       
                    var $data   		= $("#"+data_id);
                    var $preview		= $("#"+preview_id);
                    var $a_file = $data.data("$a_file")||[];
                    
                    var max_len = $btn.attr("max_len");
                    var new_len = $btn[0].files.length;
                    var old_len = $a_file.length;
                    $preview.html($data.data(preview_id)||"");
                    
                    for(var i=0, f; f=evt.target.files[i]; i++) {
                        if(!f.type.match('image.*')) {
                            continue;
                        }
                        var file_size_limit = "<?php echo $file_size_limit?>";
                        file_size_limit = file_size_limit==""? 0 : file_size_limit;
                        if( f.size > file_size_limit ){
	                        alert2(f.name+"檔案超過1MB，請重新上傳。");
	                        if(evt.target.files[i+1]==undefined){
		                        evt.target.value="";
	                        }
	                        continue;
                        }
                        
                        var reader = new FileReader();
                        reader.onload = (function(theFile) {
                            return function(e) {
	                            //new屬性作用在於，圖檔先進preview，img_setting會偵測到沒有span，給加上default span
	                            $preview.append('<img new class="thumb" src="'+e.target.result+'" title="'+escape(theFile.name)+'"/>');
                                $preview.find("img:last")[0].onload = function(){
                                    var $this = $(this);
                                    var $preview = $this.parent();
                                    var title = $this.attr("title");
                                    var onload_len = $preview.find("img").length;
                                    //var filename = this.width+"x"+this.height+"_@."+title.substring(title.lastIndexOf(".")+1);
                                    $this.css({
                                        "border":"1px solid gray",
                                        "margin":"5px"
                                    });
                                    //$this.attr("title",filename);
                                    $this.height(height);
                                    $this.wrap("<div style='display:inline-block;text-align:center;margin-bottom:10px;'></div>");
                                    $this.parent().append("<br/><span new class='bg shadowRoundCorner' style='padding:3px;cursor:pointer;color:white'>delete</span>");
                                    theFile.img_path = image_path;
                                    $a_file.push(theFile);
                                    var tmp_new_len =$a_file.length-old_len
                                    //console.log(+"onload_len:"+onload_len+"^^old_len:"+old_len+"^^new_len:"+new_len+"^^max_len:"+max_len);
                                    if( tmp_new_len == new_len || onload_len == max_len ){
	                                    img_setting($preview);
                                    	$data.data("$a_file",$a_file);
                                    	$data.data(preview_id,$preview.html());
                                    	//console.log(preview_id+"^^"+$preview.html())
	                                    $btn.val("");
                                    }
                                }
                            };
                        })(f);
                        reader.readAsDataURL(f);
                    }
                    
                });
                
                ////////////////

                
            });
        </script>
        <style>
            /******************** user define css ********************/

        </style>
    </head>
    <body>
        <input type="hidden" id="subject" value="<?php echo $subject;?>"/>
        <div id="wrapper">
            <!-- ******************** header ******************** -->
            <div id="header">
                <h3><?php echo $html_title; ?></h3>
            </div>
            <!-- /.header -->
            <!-- ******************** body ******************** -->
            <div id="body">
                <div id="body_left">
                    <?php echo $menu; ?>
                </div>
                <!-- /.body_left -->
                <div id="body_right">
                	<?php
                        if($permissions2add){
                            echo "<input type='button' id='add_windows' value='新增品牌' data-open-dialog='新增視窗'>";
                            echo "<div id='addPanel' data-dialog='新增視窗'>";

                            echo "<table>";
                            echo "</table>";

                            echo "<input type='hidden' id='data_add'/>";
                            echo "</div>";
                        }else{
                            echo "你無權新增品牌";
                        }
                                    
                    ?>
                    <?php
						echo "<br><br>";
                        $pager->display();
	                ?>
                    <table class="table-h" id="list_panel" style='margin-top:10px'>
                        <?php

                        echo "<tr>
                            		<td>ID</td>
                            		<td>圖片</td>
                            		<td>前台顯示</td>
                            		<td>時效性</td>
                            		<td>修改</td>
                            		<td>刪除</td>
                            	</tr>";

                        $len = count($all_data);
                        for($i = 0; $i < $len; $i++){
                            $id     = $all_data[$i]["fi_id"];
                            $images = $all_data[$i]['fv_images'];
                            $active = $all_data[$i]['fi_active'];
                            $active_txt = $active == "1"? "顯示": "不顯示";
                            $active = $active == "1"? "true": "false";
                            $date_active = $all_data[$i]["fi_date_active"];
                            $date_active_txt = $date_active == "1"? "啟用": "停用";
                            $date_active = $date_active == "1"? "true": "false";
                            $start  = $all_data[$i]["fd_start"];
                            $end    = $all_data[$i]["fd_end"];
                            $weights= $all_data[$i]['fi_weights'];

                            echo "<tr>";
                            echo "<td width='10%'>".$id."</td>";
                            echo "<td>";
                            if( file_exists($image_path.$images)&&!is_dir($image_path.$images) )
                                echo "<img src='{$image_path}{$images}' title='$tmp_logo' style='max-height:100px;max-width:600px'/>";
                            echo "</td>";
                            echo "<td width='10%'>".$active_txt."</td>";
                            echo "<td width='10%'>".$date_active_txt."</td>";

                            echo "<td width='10%'>";
                            if($permissions2edit)
                                echo "<input type='button' fi_id='{$id}' value='編輯' data-open-dialog='編輯視窗'>";
                            echo "</td>";

                            echo "<td width='10%'>";
                            if($permissions2del)
                                echo "<input type='button' fi_id='{$id}' class='del_btn' value='刪除'>";

                            echo "<input type='hidden' id='data_{$id}'
                                            edit_active='{$active}'
                                            edit_date_active='{$date_active}'
                                			edit_start='{$start}'
                                			edit_end='{$end}'
                                			edit_weights='$weights'>";
                            echo "
                            <script>
                                $(document).ready(function(){";

                            if( file_exists($image_path.$images)&&!is_dir($image_path.$images) )
                                echo "$('#data_{$id}').data('edit_images_preview',
		            			\"<img src='{$image_path}{$images}' title='$images' style='max-height:100px;' />\"
		            		);";
                            echo "
                                });
                            </script>";
                            echo "</td>";
                            echo "</tr>";
                        }
                        ?>
                    </table>
                    <?php
                        $pager->display();
                    ?>
                    <div id="editPanel" data-dialog="編輯視窗">
                        <table>
                        </table>
                    </div>
                    
                </div>
                <!-- /.body_right -->
            </div>
            <!-- /.body -->
            <!-- ******************** footer ******************** -->
            <div id="footer">
                <span><?php echo $html_copyright; ?></span>
            </div>
            <!-- /.footer -->
        </div>
        <!-- /.wrapper -->
    </body>
</html>
