<?php 
    session_start();
    include 'backend/template.php';
    
    if(!empty($_SESSION['admin']['login_user'])){
        
        //取資料
        $login_fi_id = $_SESSION['admin']['login_fi_id'];
        //$login_store = $_SESSION['admin']['login_store'];
        $login_name = $_SESSION['admin']['login_name'];
        $login_permissions = $_SESSION['admin']['login_permissions'];
        $login_user = $_SESSION['admin']['login_user'];
        $login_username = $_SESSION['admin']['login_username'];
        $login_title = $_SESSION['admin']['login_title'];
        //$login_php_vars = $_SESSION['admin']['login_php_vars'];
        $menu = $_SESSION['admin']['menu'];
        
        $subject = "promote";
        //取資料
        $permissions2visit	=	(strpos($login_permissions,$subject."_list")	!==false||$login_permissions=="all")?true:false;
        $permissions2add	=	(strpos($login_permissions,$subject."_add")	!==false||$login_permissions=="all")?true:false;
        $permissions2edit	=	(strpos($login_permissions,$subject."_edit")	!==false||$login_permissions=="all")?true:false;
        $permissions2del	=	(strpos($login_permissions,$subject."_del")	!==false||$login_permissions=="all")?true:false;
        
        include 'library/dba.php';
        require_once "library/class_pager2.php";

        $dba = new dba();
        $pager = new Pager();
        $status = $_GET["s"];

        $keyword = $_COOKIE[$subject."_keyword"];
        if($keyword !=""){ 
            $keyword_val = '%'.strtr($keyword,array(" "=>"%","台"=>"臺")).'%';
            $where_keyword = " and ( fv_name like '$keyword_val' or fv_describe like '$keyword_val' )"; 
        }

        $type = $_COOKIE[$subject."_type"];
        if($type!=""){ $where_type=" and fi_type='$type' "; }

        $sql = "select 	fi_id
        				,fv_name
        			  	,fv_describe
        				,fv_logo
        				,fi_active
                        ,fi_date_active
        				,fd_start_date
        				,fd_end_date
                        ,fi_bonus
                        ,fi_type
                        ,fv_activity_picture
        		from t_promote 
        		where fi_delete <>1
                $where_keyword
                $where_type
                order by fi_id desc";
        $all_data = $pager->query($sql);
        $img_path = "images/promote/";
        $act_path = "images/promote_activity/";
		$file_size_limit = 1024*1024*1;//1M   unit:byte

        $sql = "select * from t_promote_type;";
        $promote_type = $dba->getAll($sql);
        $promote_html = "";
        foreach ($promote_type as $promote) {
            $id = $promote["fi_id"];
            $name = $promote["fv_name"];
            $promote_html .= "<option value='{$id}'>$name</option>";
        }
		
    }else{
        header("Location:index.php");
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo $html_title;?></title>
    	<link rel="stylesheet" type="text/css" href="backend/css/jquery.datetimepicker.css">
        <?php echo $html_resource;?>
        <script type="text/javascript" src="backend/js/jquery.datetimepicker.js"></script>
	    <script type="text/javascript" src="backend/js/date_setting.js"></script>
        <script>
            /******************** user define js ********************/
            $(document).ready(function(){
            	
                //ajax
                //filename write in template.js
                
                //新增
                
                $("input.add_btn").click(function(){
                	
                	var $dialog = $("#dialog_container");
                	var name = $dialog.find("input[name='add_name']").val();
                	var describe = $dialog.find("input[name='add_describe']").val();
                	
                	$.post(filename,{
                			query_type:	"promote_add",
                			name:name,
                			describe:describe
						},function(data){
							data=$.trim(data );
                    		if(data=="success"){
                    			//alert("新增成功！");
	                    		location.href = "manage_promote.php?s=add";;
                    		}
                    		console.log(data)
                    });
	            });
                
                //編輯
                var $panel = $("#editPanel");
                function init(){
                	//刪除
	                $("input.del_btn").bind('click',function(){
	                    if(!confirm("確定刪除？"))return;
	                    var $this = $(this);
	                    var fi_id = $this.attr('fi_id');
	                    
	                    $this.unbind('click');
	                    $.post(filename,{
	                    		query_type:"promote_del",
	                    		fi_id:fi_id
							},function(data){
								data=$.trim(data);
								if(data=="success"){
			                        alert("已刪除！");
			                        location.href = location.href;
		                        }
		                        console.log(data)
	                    });
	                });
                
                	//$("#list_panel [data-open-dialog]").unbind("click");
	                $("#list_panel [data-open-dialog]").click(function(){
	                	dataInit();
	                	var $dialog = $("#dialog_content");
	                	
	                	edit_id = $(this).attr('fi_id');
	                	console.log()
	                	var $data = $("#data_"+edit_id);
	                	
	                    //radio更新
	                	var arr_name = [];
	                    $dialog.find("input[type='radio']").each(function(){
		                    var name=$(this).prop("name").replace(/\W+/g, "");
		                    if( !in_array(name,arr_name) )
		                    	arr_name.push(name);
	                    })
	                    for(i=0; i<arr_name.length; i++){
	                    	var name=arr_name[i];
	                    	var radio_val = $data.attr(name);
							$dialog.find("input[name='"+name+"'][value='"+radio_val+"']").prop("checked",true);
	                    }
	                    //text
						$dialog.find("input[type='text']").each(function(){
							$(this).val($data.attr($(this).attr("name")));
						})
							
						//date
						$dialog.find(".date").change(function(){
							$data.attr($(this).attr("name"),$(this).val());
						})
						//img
						$dialog.find("input.pic[type='file']").each(function(){
							var preview_id = $(this).prop('name')+"_preview";
							$("#"+preview_id).html($data.data(preview_id));
							img_setting($("#"+preview_id));
						});
						
						//select
						$dialog.find("select").each(function(){
							$(this).find("option[selected]").removeAttr("selected");
							$(this).find("option[value='"+$data.attr($(this).prop('name'))+"']").prop("selected",true);
						})
						
						
		            })
                }
                init()
                
                //及時更新暫存資料(hidden暫存，沒存進資料庫)
                $panel.find("input[type!='button'][type!='checkbox'][type!='radio']").keyup(function(){
	                $("#data_"+edit_id).attr($(this).prop("name"),$(this).val() );
                })
                	
                $panel.find("input[type='radio']").click(function(){
                    $("#data_"+edit_id).attr($(this).prop("name"),$(this).val() );
                });
                //select
                $panel.find("select").change(function(){
                    $("#data_"+edit_id).attr($(this).prop("name"),$(this).val() );
                });
                
                $panel.find("input.edit_btn").click(function(){
                	var $dialog = $("#dialog_content");
                    //權限
                    if(!chk_all_input("#dialog_container")){
	                    return;
                    }
                    var $data = $("#data_"+edit_id);
                    var name 	= $data.attr("edit_name");
                    var describe 	= $data.attr("edit_describe");
                    var date_active = $data.attr("edit_date_active");
                    var start_date 	= $data.attr("edit_start_date");
                    var end_date 	= $data.attr("edit_end_date");
                    var active 		= $data.attr("edit_active");
                    var bonus       = $data.attr("edit_bonus");
                    var type        = $data.attr("edit_type");
					
					var $a_file = $data.data("$a_file")||[];
					var $a_remove_file = $data.data("$a_remove_file")||[];
					var len = 0;
					
					var form_data = new FormData();
						form_data.append("query_type",	"promote_edit");
						form_data.append("fi_id",		edit_id);
						form_data.append("describe",	describe);
						form_data.append("name",	    name);
						form_data.append("date_active",	date_active);
						form_data.append("start_date",	start_date);
						form_data.append("end_date",	end_date);
						form_data.append("active",		active);
                        form_data.append("bonus",       bonus);
                        form_data.append("type",        type);
					
					len = $a_file.length;
					for( j=0 ; j<len ; j++ )
                    {
                        form_data.append('file[]', $a_file[j]);
                        form_data.append('file_path[]', $a_file[j].img_path);
                    }

					len = $a_remove_file.length;
					for( j=0 ; j<len ; j++ ){
                        form_data.append( "a_remove_file["+j+"][title]",$a_remove_file[j]["title"] );
                        form_data.append( "a_remove_file["+j+"][path]",$a_remove_file[j]["path"] );
                    }
                    
                    $.ajax({
							url: filename,
							dataType: 'text',
							cache: false,
							contentType: false,
							processData: false,
							data: form_data,                         
							type: 'post',
							success: function(data){
								if($.trim(data).indexOf("success")>-1){
			                        alert("已更新！");
			                        location.href="manage_promote.php";
		                        }
		                        console.log(data)
							}
						});
                });
                
                //無span之圖片(原有)包裝後加上span[default]，點下會進入$a_remove_file[]，剩下的span(新加入)點擊後加入$a_file[]
                function img_setting($preview)
                {
                	var total = $preview.find("img").length;
	                var $add_img_btn = $preview.parent().children("input[type='file']");
	                //console.log(total+"^^"+$add_img_btn.attr("max_len")+$preview.html());
	                if( total>=$add_img_btn.attr("max_len") ){
		                $add_img_btn.unbind("click");
		                $add_img_btn.hide()
	                }else{
		                $add_img_btn.bind("click");
		                $add_img_btn.show()
	                }
	                
	                var name	= $preview.attr("btn_name");
	                
                    var preview_id = name+"_preview";
                    
                    var $data 		= $("#data_"+edit_id);
                    
	                $preview.find("img:not([new])").each(function(index){
	                    $img = $(this);
	                    if( $img.parent().children("span").length==0 ){//原先圖片
	                    	$img.css('max-height','100px')
		                    //console.log("img_parent.html:\n"+$img.parent().html())
		                    $img.wrap("<div style='display:inline-block;text-align:center;margin-bottom:10px;'></div>");
                            $img.parent().append("<br/><span class='bg shadowRoundCorner' style='padding:3px;cursor:pointer;color:white'>delete</span>");
                        }
                    })
                    $preview.find("img").change(function(){
                    	$data.data(preview_id,$(this).parent().parent().html());
                    })
                    // img
					$preview.find("span:not([new])").click(function(){
						var $a_remove_file = $data.data("$a_remove_file")||[];
						$preview = $(this).parent().parent();
                        var tmp_arr = [];
                        $img = $(this).parent().find("img");
                        src = $img.prop("src")||"";
                        tmp_arr["src"]	 = src;
                        tmp_arr["path"]  = src.substring(0,src.lastIndexOf('/')+1);
                        tmp_arr["title"] = src.substring(src.lastIndexOf('/')+1);
                        $a_remove_file.push(tmp_arr);
                        //$a_remove_file.push(src.substring(src.lastIndexOf('/')+1))
                        $(this).parent().remove();
                        //console.log(src);
                        img_setting($preview)
                        $data.data("$a_remove_file",$a_remove_file);
                        $data.data(preview_id,$preview.html());
                    })
                    $preview.find("span[new]").click(function(){
                    	var $a_file = $data.data("$a_file");
                    	console.log("data : "+$data.prop("id"))
                    	console.log("span click : "+$a_file)
                        $preview = $(this).parent().parent();
	                    $(this).parent().remove();
	                    var now_len = $a_file.length;
	                    for( k=0 ; k<now_len ; k++ ){
	                    	try{
	                    		var title=unescape($(this).parent().find("img").attr("title"));
                            	if( $a_file[k].name == title ){
                                    $a_file.splice(k,1);
                                    break;
                                }
                        	}catch(error){
                        	}
	                    }
	                    img_setting($preview)
	                    $data.data("$a_file",$a_file);
	                    $data.data(preview_id,$preview.html());
                    })
                    
                    $data.data(preview_id,$preview.html());
                }
                
                //加上預覽ＤＩＶ
                $panel.find("input[type='file'][class='pic'][name]").each(function(){
                	var $this = $(this);
                	var name = $this.prop("name");
	                var preview_id = name+"_preview";
	                if($("div#"+preview_id).length==0)
	                	$this.before("<div id='"+preview_id+"' btn_name='"+name+"'></div>");
                })
                
                //var $a_remove_file=[];//存放所有刪除檔案路徑
	            //var $a_file=[];//存放所有檔案
				
                $panel.find("input[type='file'][class='pic'][name]").change(function(evt)
                {
                    var $btn	= $(this);
                    var name	= $btn.prop("name");
                    var height	= $btn.attr("height")||100;
               
                    var now_no	= $panel.attr('now_no');
                    var data_id = "data_"+edit_id;
                    var preview_id	= name+"_preview";
                       
                    var $data   		= $("#"+data_id);
                    var $preview		= $("#"+preview_id);
                    var $a_file = $data.data("$a_file")||[];
                    
                    var max_len = $btn.attr("max_len");
                    var new_len = $btn[0].files.length;
                    var old_len = $a_file.length;
                    $preview.html($data.data(preview_id)||"");
                    
                    for(var i=0, f; f=evt.target.files[i]; i++) {
                        if(!f.type.match('image.*')) {
                            continue;
                        }
                        var file_size_limit = "<?php echo $file_size_limit?>";
                        file_size_limit = file_size_limit==""? 0 : file_size_limit;
                        if( f.size > file_size_limit ){
	                        alert2(f.name+"檔案超過1MB，請重新上傳。");
	                        if(evt.target.files[i+1]==undefined){
		                        evt.target.value="";
	                        }
	                        continue;
                        }
                        
                        var reader = new FileReader();
                        reader.onload = (function(theFile) {
                            return function(e) {
	                            //new屬性作用在於，圖檔先進preview，img_setting會偵測到沒有span，給加上default span
	                            $preview.append('<img new class="thumb" src="'+e.target.result+'" title="'+escape(theFile.name)+'"/>');
                                $preview.find("img:last")[0].onload = function(){
                                    var $this = $(this);
                                    var $preview = $this.parent();
                                    var title = $this.attr("title");
                                    var onload_len = $preview.find("img").length;
                                    //var filename = this.width+"x"+this.height+"_@."+title.substring(title.lastIndexOf(".")+1);
                                    $this.css({
                                        "border":"1px solid gray",
                                        "margin":"5px"
                                    });
                                    //$this.attr("title",filename);
                                    $this.height(height);
                                    $this.wrap("<div style='display:inline-block;text-align:center;margin-bottom:10px;'></div>");
                                    $this.parent().append("<br/><span new class='bg shadowRoundCorner' style='padding:3px;cursor:pointer;color:white'>delete</span>");
                                    
                                    if(name=="logo"){
                                        theFile.img_path = "logo";  
                                    }else{
                                        theFile.img_path = "activity_picture";  
                                    }
                                    $a_file.push(theFile);
                                    var tmp_new_len =$a_file.length-old_len
                                    //console.log(+"onload_len:"+onload_len+"^^old_len:"+old_len+"^^new_len:"+new_len+"^^max_len:"+max_len);
                                    if( tmp_new_len == new_len || onload_len == max_len ){
	                                    img_setting($preview);
                                    	$data.data("$a_file",$a_file);
                                    	console.log("data : "+$data.prop("id"))
                                    	console.log("a_file : "+$a_file)
                                    	$data.data(preview_id,$preview.html());
                                    	//console.log(preview_id+"^^"+$preview.html())
	                                    $btn.val("");
                                    }
                                }
                            };
                        })(f);
                        reader.readAsDataURL(f);
                    }
                });
                ////////////////
                <?php 
                    if($status=="add"){
                        echo '$("#list_panel [data-open-dialog]:first").trigger("click");';
                    }
                ?>
            });
        </script>
        <style>
            /******************** user define css ********************/
            .search_img{
                background:url(images/public/search.png);
                -moz-background-size:contain;
                -webkit-background-size:contain;
                -o-background-size:contain;
                background-size:contain;
                background-repeat: no-repeat;
                vertical-align: text-top;
                width:20px;
                height:20px;
                border: 0;
                z-index:2;
                position:relative;
                left:-25px;
            }
        </style>
    </head>
    <body>
        <form>
            <input type="hidden" id="subject" value="<?php echo $subject;?>"/>
            <input type="hidden" name="fi_type" id="fi_type" value="<?php echo $type;?>"/>
            <input type="hidden" name="fv_keyword" id="fv_keyword" value="<?php echo $keyword; ?>"/>
        </form>
        <div id="wrapper">
            <!-- ******************** header ******************** -->
            <div id="header">
                <h3><?php echo $html_title; ?></h3>
            </div>
            <!-- /.header -->
            <!-- ******************** body ******************** -->
            <div id="body">
                <div id="body_left">
                    <?php echo $menu; ?>
                </div>
                <!-- /.body_left -->
                <div id="body_right">
                	<?php
                        if($permissions2add){
                            echo "<input type='button' value='新增好康' data-open-dialog='新增視窗'>";
                            echo "<div id='addPanel' data-dialog='新增視窗'>";
                            echo "<table class='table-v'>";
                            echo 	"<tr>
                                        <td>好康名稱</td>
                            			<td>
	                            		<input id='add_name' name='add_name'>
	                            		</td>
                                    </tr>
                                    <tr>
                                        <td>好康描述</td>
                                        <td>
                                        <input id='add_describe' name='add_describe'>
                                        </td>
	                        		</tr>";
                            echo 	"<tr><td></td><td><input class='add_btn' type='button' value='送出'></td></tr>";
                            echo "</table>";
                            echo "</div>";
                        }else{
                            echo "你無權新增好康";
                        }
                    ?>
                    <br><br>
                    <div id="search">
                        <div style='float:left;'>
                            <select id="type_select">
                                <option value="">全部</option>
                                <option value="1" >手機</option>
                                <option value="2" >3C</option>
                                <option value="3" >其他</option>
                            </select>
                        </div>
                        <div style='float:left;'>
                            <div style'clear:both;'>
                                <input type='text' name='keyword' id='keyword' placeholder="請輸關鍵字" alt="可輸入 區域 或 描述 或 店家名稱"/>
                                <input type='button' name='search_img' id='search_img' class='search_img'/>
                                <!--<img src='images/search.png' height='20' valign='bottom'/>-->
                            </div>
                        </div>
                        <div style='float:left;'>
                            <input type='button' id="submit" value="查詢"/>
                        </div>
<script>
    $(document).ready(function(){
        $("#type_select").val($("#fi_type").val());
        $("#keyword").val($("#fv_keyword").val());
        $("#type_select").change(function(){
            $("#fi_type").val($(this).val());
            document.cookie = $("#subject").val()+"_type="+$(this).val();
        })
        $("#keyword").keyup(function(e){
            $("#fv_keyword").val($(this).val());
            document.cookie = $("#subject").val()+"_keyword="+$(this).val();
            if(e.keyCode=="13"){$("form").submit();}
        })
        $("#search_img").click(function(){
            $("form").submit();
        })
        $("#submit").click(function(){
            $("form").submit();
        })
    })
</script>
                    </div>
                    <br>
                    <?php
						echo "<br>";
						echo "
							<table class='table-h' id='list_panel' style='margin-top:10px'>
					            <tr>
					            	<td>ID</td>
					            	<td>好康圖片</td>
                                    <td>好康名稱</td>
					            	<td>點數</td>
                                    <td>活動圖片</td>
					        		<td>狀態</td>
					            	<td>修改</td>
					            	<td>刪除</td>
					            </tr>";
						$pager->display();
						if(is_array($all_data))
						foreach($all_data as $data){
								$id = $data["fi_id"];
								$logo = $data["fv_logo"];
								$describe 	= $data["fv_describe"];
								$name = $data["fv_name"];
	        					$date_active= $data["fi_date_active"];
	        					$date_active_cht = $date_active==1?"啟用":"停用";
	        					$start_date = $data["fd_start_date"];
	        					$end_date 	= $data["fd_end_date"];
	        					$active 	= $data["fi_active"];
	        					$active_cht = $active==1?"開放":"不開放";
                                $bonus      = $data["fi_bonus"];
                                $type       = $data["fi_type"];
                                $act_img    = $data["fv_activity_picture"];
							echo "<tr>";
							echo "<td>$id</td>
								  <td>";
							if( file_exists($img_path.$logo)&&!is_dir($img_path.$logo) )
					            echo "<img src='{$img_path}{$logo}' title='$logo' style='max-height:100px;max-width:600px'/>";
					        echo "	</td>
                                    <td>$name</td>
					        		<td>$bonus</td>
                                    <td>";
                            if( file_exists($act_path.$act_img)&&!is_dir($act_path.$act_img) )
                                echo "<img src='{$act_path}{$act_img}' title='$act_img' style='max-height:100px;max-width:600px'/>";
                            
                            echo "  </td>
					            	<td>$active_cht</td>
					        		<td>";
					        if($permissions2edit)
					            echo "<input type='button' fi_id='$id' value='編輯' data-open-dialog='編輯視窗'>";
					        echo "	</td>
					        		<td>";
					        if($permissions2del)
					            echo "<input type='button' fi_id='$id' value='刪除' class='del_btn'>";
							echo "	<input type='hidden' id='data_{$id}' 
								edit_name='$name' edit_describe = '$describe'
								edit_active='$active' 
								edit_date_active = '$date_active' edit_start_date='$start_date'
								edit_end_date='$end_date' edit_bonus='$bonus' edit_type='$type'>";
				            if( file_exists($img_path.$logo)&&!is_dir($img_path.$logo) )
				            echo "<script>
				            		$(document).ready(function(){
					            		$('#data_{$id}').data('logo_preview',
					            			\"<img src='{$img_path}{$logo}' title='$logo' style='max-height:100px;' />\"
					            		);
				            		});
				            	</script>";
                            if( file_exists($act_path.$act_img)&&!is_dir($act_path.$act_img) )
                            echo "<script>
                                    $(document).ready(function(){
                                        $('#data_{$id}').data('activity_picture_preview',
                                            \"<img src='{$act_path}{$act_img}' title='$act_img' style='max-height:100px;' />\"
                                        );
                                    });
                                </script>";
							echo "	</td>
								</tr>";
						}
						echo "</table>
							<br>";
						$pager->display();
	                ?>
                    <div id="editPanel" data-dialog="編輯視窗">
                        <table class='table-v'>
                        	<tr>
                                <td width="30%">好康圖片</td>
                                <td><input type="file" class="pic" name="logo" max_len='1'></td>
                            </tr>
                            <tr>
                                <td>好康分類</td>
                                <td><select id="edit_type" name="edit_type">
                                        <?php echo $promote_html;?>
                                    </select></td>
                            </tr>
                            <tr>
                                <td>好康名稱</td>
                                <td><input type="text" name="edit_name" id="edit_name"/></td>
                            </tr>
                        	<tr>
                                <td>好康描述</td>
                                <td><input type="text" name="edit_describe" id="edit_describe" /></td>
                            </tr>
                            <tr>
                                <td>對換所需點數</td>
                                <td><input type="text" name="edit_bonus" id="edit_bonus" /></td>
                            </tr>
                            <tr>
                                <td>有效性<br>(是否要開放兌換)</td>
                                <td>
                                    <input type="radio" name="edit_active" id="edit_active_on" value="1">
                                    <label for="edit_date_active_on">開放</label>
                                    <input type="radio" name="edit_active" id="edit_active_off" value="0">
                                    <label for="edit_active_off">不開放</label>
                                </td>
                            </tr>
                            <tr>
                                <td>時效性<br>(是否要依據開始及結束時間撥放廣告)</td>
                                <td>
                                	<input type="radio" name="edit_date_active" id="edit_date_active_on" value="1">
                                	<label for="edit_date_active_on">啟用</label>
                                	<input type="radio" name="edit_date_active" id="edit_date_active_off" value="0">
                                	<label for="edit_date_active_off">停用</label>
                                </td>
                            </tr>
                        	<tr>
                                <td>開始時間</td>
                                <td>
                                	<input type="text" name="edit_start_date" id="edit_start_date" class='date'>
                                </td>
                            </tr>
                       		<tr>
                                <td>結束時間</td>
                                <td>
                                	<input type="text" name="edit_end_date" id="edit_end_date" class='date'>
                                </td>
                            </tr>
                            <tr>
                                <td>活動圖片</td>
                                <td><input type="file" class="pic" name="activity_picture" max_len='1'></td>
                            </tr>
                        	<tr><td></td><td><input type="button" value="儲存" class="edit_btn" style="cursor: pointer;"></td></tr>
                        </table>
                    </div>
                </div>
                <!-- /.body_right -->
            </div>
            <!-- /.body -->
            <!-- ******************** footer ******************** -->
            <div id="footer">
                <span><?php echo $html_copyright; ?></span>
            </div>
            <!-- /.footer -->
        </div>
        <!-- /.wrapper -->
    </body>
</html>
