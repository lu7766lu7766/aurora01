<?php 
    session_start();
    include 'backend/template.php';
    
    if(!empty($_SESSION['admin']['login_user'])){
        
        //取資料
        $login_fi_id = $_SESSION['admin']['login_fi_id'];
        //$login_store = $_SESSION['admin']['login_store'];
        $login_name = $_SESSION['admin']['login_name'];
        $login_permissions = $_SESSION['admin']['login_permissions'];
        $login_user = $_SESSION['admin']['login_user'];
        $login_username = $_SESSION['admin']['login_username'];
        $login_title = $_SESSION['admin']['login_title'];
        //$login_php_vars = $_SESSION['admin']['login_php_vars'];
        $menu = $_SESSION['admin']['menu'];
        
        //取資料
        $permissions2visit	=	(strpos($login_permissions,"brand_list")!==false||$login_permissions=="all")?true:false;
        $permissions2add	=	(strpos($login_permissions,"brand_add")	!==false||$login_permissions=="all")?true:false;
        $permissions2edit	=	(strpos($login_permissions,"brand_edit")!==false||$login_permissions=="all")?true:false;
        $permissions2del	=	(strpos($login_permissions,"brand_del")	!==false||$login_permissions=="all")?true:false;
        
        include_once 'library/dba.php';
        $dba = new dba();
        $all_type = $dba->getAll("select fi_id,fv_name,fi_active from t_store_type order by fi_weights desc");
        
        $brand = $_COOKIE["brand"];
        $selector_html="";
        foreach($all_type as $per_type){
            $select_attr = $brand==$per_type["fi_id"]?"selected":"";
        	if($per_type["fi_active"]==1)
        		$selector_html.="<option value='".$per_type["fi_id"]."' $select_attr >".$per_type["fv_name"]."</option>";
        	else
        		$selector_html.="<option value='".$per_type["fi_id"]."' $select_attr ><s>".$per_type["fv_name"]."</s></option>";
        }
    }else{
        header("Location:index.php");
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo $html_title;?></title>
        <?php echo $html_resource;?>
        <script>
            /******************** user define js ********************/
            $(document).ready(function(){
            	
                //ajax
                //filename write in template.js
                
                //新增
                $("input.add_btn").click(function(e,lat,lng){
                	
                	var $this=$("#addPanel"); 
                	var add_brand 		= $this.find("select[name='add_brand']").val();
                    var add_subname 	= $this.find("input[name='add_subname']").val();
                    var add_phone 		= $this.find("input[name='add_phone']").val();
                    var add_source_phone= $this.find("input[name='add_source_phone']").val();
                    var add_address 	= $this.find("input[name='add_address']").val();
                    var add_level       = $this.find("select[name='add_level']").val();
                    if( !chk_all_input("addPanel") ){
	                    return;
                    }
                    if(add_phone.indexOf("55104")<0){
                    	return alert2("電話格式不正確");
                    }
                    var lat = lat||0;
                    var lng = lng||0;
                    /*if( lat=="" || lng=="" ){
                		getLatLng(add_address,"add");
                		return;
                	}*/
                	
                    $.post(filename,{
                			query_type:	"store_add",
                			brand:		add_brand,
							subname:	add_subname,
							phone:		add_phone,
							source_phone:add_source_phone,
							address:	add_address,
                            level:      add_level, 
							lat:		lat,
							lng:		lng
						},function(data){
							data=$.trim(data);
                    		if(data=="success"){
                    			alert("新增成功！");
	                    		location.href = location.href;
                    		}else if(data=="over"){
                    			alert("資料已存在！")
                    		}
                    		console.log(data)
                    });
	            });
                
                
        
                //編輯
                var $panel = $("#editPanel");
                function init(){
                	//刪除
	                $("input.del_btn").bind('click',function(){
	                    if(!confirm("確定刪除？"))return;
	                    var $this = $(this);
	                    var fi_id = $this.attr('fi_id');
	                    
	                    $this.unbind('click');
	                    $.post(filename,{
	                    		query_type:"store_del",
	                    		fi_id:fi_id
							},function(data){
								data=$.trim(data);
								if(data=="success"){
			                        alert("已刪除！");
			                        location.href = location.href;
		                        }
		                        console.log(data)
	                    });
	                });
                
                	//$("#list_panel [data-open-dialog]").unbind("click");
	                $("#list_panel [data-open-dialog]").click(function(){
	                	
	                	var $dialog = $("#dialog_content");
	                	
	                	edit_id = $(this).attr('fi_id');
	                	var $data = $("#data_"+edit_id);
	                		
	                    //radio更新
	                	var arr_name = [];
	                    $dialog.find("input[type='radio']").each(function(){
		                    var name=$(this).prop("name").replace(/\W+/g, "");
		                    if( !in_array(name,arr_name) )
		                    	arr_name.push(name);
	                    })
	                    for(i=0; i<arr_name.length; i++){
	                    	var name=arr_name[i];
	                    	var radio_val = $data.attr(name);
							$dialog.find("input[name='"+name+"'][value='"+radio_val+"']").prop("checked",true);
	                    }
	                    //text
						$dialog.find("input[type='text']").each(function(){
							$(this).val($data.attr($(this).attr("name")));
							//console.log($(this).attr("name"))
							//console.log($data.attr($(this).attr("name")))
						})
						
						//img
						$dialog.find("input.pic[type='file']").each(function(){
							var preview_id = $(this).prop('name')+"_preview";
							$("#"+preview_id).html($data.data(preview_id));
							img_setting($("#"+preview_id));
						});
						
						//select
						$dialog.find("select").each(function(){
							$(this).find("option[selected]").removeAttr("selected");
							$(this).find("option[value='"+$data.attr($(this).attr('name'))+"']").prop("selected",true);
						})
	                });
                }
                init()
                
                //及時更新暫存資料(hidden暫存，沒存進資料庫)
                $panel.find("input[type!='button'][type!='checkbox'][type!='radio']").keyup(function(){
	                $("#data_"+edit_id).attr($(this).prop("name"),$(this).val() );
                })
                	
                $panel.find("input[type='radio']").click(function(){
                    $("#data_"+edit_id).attr($(this).prop("name"),$(this).val() );
                });
                //select
                $panel.find("select").change(function(){
                    $("#data_"+edit_id).attr($(this).prop("name"),$(this).val() );
                });
                
                $panel.find("input.edit_btn").click(function(e,lat,lng){
                	var $dialog = $("#dialog_content");
                    //權限
                    if(!chk_all_input("#dialog_container")){
	                    return;
                    }
                    var $data = $("#data_"+edit_id);
                    var brand = $data.attr("edit_brand");
					var subname = $data.attr("edit_subname");
					var phone 	= $data.attr("edit_phone");
					var source_phone = $data.attr("edit_source_phone");
					var sphone = $data.attr("sphone");
					var address = $data.attr("edit_address");
					var saddress = $data.attr("saddress");
                    var level = $data.attr("edit_level");
                    var special_price = $data.attr("edit_special_price");
                    var special_info = $data.attr("edit_special_info");
                    var voice_ad = $data.attr("edit_voice_ad");
                    var msg_ad = $data.attr("edit_msg_ad");
					var slat = $data.attr("lat")||"";
					var slng = $data.attr("lng")||"";
					var lat = lat||0;
					var lng = lng||0;
					
					/*if( saddress!=address 
					|| (slat==""&&lat=="") 
					|| (slng==""&&lng=="") ){
                		getLatLng(address,"edit");
                		return;
                	}else if(saddress==address&&slat!=""&&slng!=""){
                		lat = slat;
                		lng = slng;
                	}*/
                	//console.log(saddress+"^^"+address+"^^"+slat+"^^"+lat+"^^"+slng+"^^"+lng)
                	//console.log("location:"+lat+"^^"+lng)
					var form_data = new FormData();
						form_data.append("query_type",	"store_edit");
						form_data.append("fi_id",		edit_id);
						form_data.append("brand",		brand);
						form_data.append("subname",		subname);
						form_data.append("phone",		phone);
						form_data.append("source_phone",source_phone);
						form_data.append("sphone",		sphone);
						form_data.append("address",		address);
						form_data.append("saddress",	saddress);
                        form_data.append("level",       level);
                        form_data.append("special_price",special_price);
                        form_data.append("special_info",special_info);
                        form_data.append("voice_ad",voice_ad);
                        form_data.append("msg_ad",msg_ad);
						form_data.append("lat",			lat);
						form_data.append("lng",			lng);
					
                    $.ajax({
							url: filename,
							dataType: 'text',
							cache: false,
							contentType: false,
							processData: false,
							data: form_data,                         
							type: 'post',
							success: function(data){
								if($.trim(data)=="success"){
			                        alert("已更新！");
			                        location.reload();
		                        }else if(data=="over"){
	                    			alert("資料已存在！")
	                    		}
		                        console.log(data)
							}
						});
                });
                
                function getLatLng(address,_type) {
                	
					var geocoder = new google.maps.Geocoder();
					var result = Array();
					geocoder.geocode( { 'address': address, 'region': 'uk' }, function(results, status) {
					     if (status == google.maps.GeocoderStatus.OK) {
					         result["lat"] = results[0]["geometry"]["location"]["k"]||0;
					         result["lng"] = results[0]["geometry"]["location"]["D"]||0;
					         //console.log(result)
					     } else {
					     	 result["lat"] = 0;
					         result["lng"] = 0;
					         result["status"] = "Unable to find address: " + status;
					     }
					     var type=_type ||"add";
					     //console.log("getLatLng:^^"+type+"^^");
					     //console.log(result)
					     switch(type){
					     	case "add":
					     		$("input.add_btn").trigger("click",[result["lat"],result["lng"]])
					     		break;
					     	case "edit":
					     		$("input.edit_btn").trigger("click",[result["lat"],result["lng"]])
					     		break; 
					     }
					     
				    });
				}
				
                //加上預覽ＤＩＶ
                $panel.find("input[type='file'][class='pic'][name]").each(function(){
                	var $this = $(this);
                	var name = $this.prop("name");
	                var preview_id = name+"_preview";
	                if($("div#"+preview_id).length==0)
	                	$this.before("<div id='"+preview_id+"' btn_name='"+name+"'></div>");
                })
                
                
                
                ////////////////
                $("#selector").change(function(){
                	show_list();
                }).trigger("change")
                $("#search").keydown(function(e){
                	if(e.which==13){
                		show_list($("#search").val())
                	}
                })
                $("#search_img").click(function(){show_list($("#search").val());});
                
                function show_list(search){
                	var search=search||"";
                	$("#add_type[selected]").removeAttr("selected");
                	$("#add_type").find("option[value='"+$("#selector").val()+"']").attr("selected",true);
                		
                	$(".list_panel").remove();
                	$("div.pager").remove();
                	$obj = $("#selector");
                    document.cookie = "brand="+$obj.val();
                	$.post(filename,{
                			query_type:	"get_info",
                			type:				$obj.val(),
                			search:				search,
							permissions2del:	<?php echo !$permissions2del?"false":"true";?>,
	                    	permissions2edit:	<?php echo !$permissions2edit?"false":"true";?>
						},function(data){
							data=$.trim(data);
							$table = $(data);
							$obj.after($table);
							
							init_style( $("#list_panel,#list_panel *") );
							init();
							
							//$table.find("[data-open-dialog]").unbind("click");
							//$table.find("[data-open-dialog]").bind("click");
                    });
                    $.post(filename,{
                			query_type:	"get_brand",
                			type:		$obj.val(),
						},function(data){
							data=$.trim(data);
							$("#edit_brand").html(data)
							$("#add_brand").html(data)
                    });
                }
                
            });
            function ValidateNumber(e, pnumber)
			{
			    if (!/^\d+$/.test(pnumber))
			    {
			        e.value = /^\d+/.exec(e.value);
			    }
			    return false;
			}
        </script>
        <style>
            /******************** user define css ********************/
			.search_img{
				background:url(images/public/search.png);
				-moz-background-size:contain;
				-webkit-background-size:contain;
				-o-background-size:contain;
				background-size:contain;
				background-repeat: no-repeat;
				vertical-align: text-top;
				width:20px;
				height:20px;
				border: 0;
				z-index:2;
				position:relative;
				left:-25px;
			}
        </style>
    </head>
    <body>
        <div id="wrapper">
            <!-- ******************** header ******************** -->
            <div id="header">
                <h3><?php echo $html_title; ?></h3>
            </div>
            <!-- /.header -->
            <!-- ******************** body ******************** -->
            <div id="body">
                <div id="body_left">
                    <?php echo $menu; ?>
                </div>
                <!-- /.body_left -->
                <div id="body_right">
                	<?php
                        if($permissions2add){
                            echo "<input type='button' value='新增商店' data-open-dialog='新增視窗'>";
                            echo "<div id='addPanel' data-dialog='新增視窗'>";
                            
                            echo "<table class='table-v'>";
                            echo 	"<tr><td width='35%'>品牌</td>
                            		<td>
                            			<select id='add_brand' name='add_brand'>
                        				</select>
                        			</td></tr>";
                            echo "	<tr><td>子店名<br>(例:崇德店、三民店...)</td><td><input type='text' name='add_subname' id='add_subname'/></td></tr>
                            		<tr><td>55104電話<br>(請確實填寫)</td><td><input type='text' name='add_phone' id='add_phone' class='must'/></td></tr>
                            		<tr><td>原始電話<br>(請確實填寫)<br>(只能輸入數字)</td><td><input type='text' name='add_source_phone' id='add_source_phone' class='must'  onkeyup='return ValidateNumber(this,value)' placeholder='ex:0421234567'/ ></td></tr>
                            		<tr><td>地址<br>(為有良好的查詢品質，請統一輸入格式)<br>(例:繁體中文、阿拉伯數字、沒有多餘空白等符號)</td><td><input type='text' name='add_address' id='add_address' class='must'/></td></tr>";
                            echo    "<tr><td>級數<br>(店家購買等級)</td>
                                    <td>
                                        <select id='add_level' name='add_level'>
                                            <option value='0' selected>0</option>
                                            <!--<option value='1'>1</option>-->
                                            <option value='2'>2</option>
                                            <option value='3'>3</option>
                                            <option value='4'>4</option>
                                            <option value='5'>5</option>
                                        </select>
                                    </td></tr>";
                            echo 	"<tr><td></td><td><input class='add_btn' type='button' value='送出'></td></tr>";
                            echo "</table>";
                            
                            echo "</div>";
                        }else{
                            echo "你無權新增商店";
                        }
                                    
                    ?>
                    <?php
						echo "<br>";
	                ?>
                	<div style='float:right;'>
                    	<div style'clear:both;'>
                    		<input type='text' name='search' id='search' placeholder="請輸關鍵字"/>
                    		<input type='button' name='search_img' id='search_img' class='search_img'/>
                    		<!--<img src='images/search.png' height='20' valign='bottom'/>-->
                    	</div>
                    </div>
                    <select id='selector'>
                    	<?php echo $selector_html;?>
                    </select>
	                    
                    <div id="editPanel" data-dialog="編輯視窗">
                        <table class='table-v'>
                        	<tr>
                                <td width="30%">品牌</td>
                                <td>
                        			<select id="edit_brand" name="edit_brand">
                        			</select>
                        		</td>
                            </tr>
                        	<tr>
                                <td>子店名<br>(例:崇德店、三民店...)</td>
                                <td><input type="text" name="edit_subname" id="edit_subname" alt='例:崇德店、三民店...'/></td>
                            </tr>
                        	<tr>
                                <td>55104電話<br>(會影響電話撥打，請確實填寫)</td>
                                <td><input type="text" name="edit_phone" id="edit_phone" alt='會影響電話撥打，請確實填寫'/></td>
                            </tr>
                        	<tr>
                                <td>原始電話<br>(會影響電話撥打，請確實填寫，請輸入市話)<br>(只能輸入數字)</td>
                                <td><input type="text" name="edit_source_phone" id="edit_source_phone" alt='不影響電話撥打，但會影響查詢及資料比對'  onkeyup='return ValidateNumber(this,value)'/></td>
                            </tr>
                        	<tr>
                                <td>地址<br>(為有良好的查詢品質，請統一輸入格式)<br>(例:繁體中文、阿拉伯數字、沒有多餘空白等符號)</td>
                                <td><input type="text" name="edit_address" id="edit_address" style='width:250px'/></td>
                            </tr>
                            <tr>
                                <td>級數<br>(店家購買等級)</td>
                                <td><select id="edit_level" name="edit_level">
                                        <option value="0">0</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select></td>
                            </tr>
                            <tr>
                                <td>是否特價</td>
                                <td>
                                    <input type="radio" name="edit_special_price" id="edit_special_price_on" value="1">
                                    <label for="edit_special_price_on">是</label>
                                    <input type="radio" name="edit_special_price" id="edit_special_price_off" value="0">
                                    <label for="edit_special_price_off">否</label>
                                </td>
                            </tr>
                            <tr>
                                <td>特價訊息<br>(若是選取非特價則不會顯示此訊息)</td>
                                <td><input type="text" name="edit_special_info" id="edit_special_info" style='width:250px'/></td>
                            </tr>
                            <tr>
                                <td>語音廣告</td>
                                <td>
                                    <input type="radio" name="edit_voice_ad" id="edit_voice_ad_on" value="1">
                                    <label for="edit_voice_ad_on">是</label>
                                    <input type="radio" name="edit_voice_ad" id="edit_voice_ad_off" value="0">
                                    <label for="edit_voice_ad_off">否</label>
                                </td>
                            </tr>
                            <tr>
                                <td>簡訊廣告</td>
                                <td>
                                    <input type="radio" name="edit_msg_ad" id="edit_msg_ad_on" value="1">
                                    <label for="edit_msg_ad_on">是</label>
                                    <input type="radio" name="edit_msg_ad" id="edit_msg_ad_off" value="0">
                                    <label for="edit_msg_ad_off">否</label>
                                </td>
                            </tr>
                            <tr><td></td><td><input type="button" value="儲存" class="edit_btn" style="cursor: pointer;"></td></tr>
                        </table>
                        
                    </div>
                    
                </div>
                <!-- /.body_right -->
            </div>
            <!-- /.body -->
            <!-- ******************** footer ******************** -->
            <div id="footer">
                <span><?php echo $html_copyright; ?></span>
            </div>
            <!-- /.footer -->
        </div>
        <!-- /.wrapper -->
    </body>
</html>
