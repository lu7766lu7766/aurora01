<?php 
    session_start();
    include 'backend/template.php';
    
    if(!empty($_SESSION['admin']['login_user'])){
        
        //取資料
        $login_fi_id = $_SESSION['admin']['login_fi_id'];
        //$login_store = $_SESSION['admin']['login_store'];
        $login_name = $_SESSION['admin']['login_name'];
        $login_permissions = $_SESSION['admin']['login_permissions'];
        $login_user = $_SESSION['admin']['login_user'];
        $login_username = $_SESSION['admin']['login_username'];
        $login_title = $_SESSION['admin']['login_title'];
        //$login_php_vars = $_SESSION['admin']['login_php_vars'];
        $menu = $_SESSION['admin']['menu'];
        
        $subject = "store_pending";
        //取資料
        $permissions2visit	=	(strpos($login_permissions,"{$subject}_list")	!==false||$login_permissions=="all")?true:false;
        $permissions2add	=	(strpos($login_permissions,"{$subject}_add")	!==false||$login_permissions=="all")?true:false;
        $permissions2edit	=	(strpos($login_permissions,"{$subject}_edit")	!==false||$login_permissions=="all")?true:false;
        $permissions2del	=	(strpos($login_permissions,"{$subject}_del")	!==false||$login_permissions=="all")?true:false;
        
    }else{
        header("Location:index.php");
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo $html_title;?></title>
    	<link rel="stylesheet" type="text/css" href="backend/css/jquery.datetimepicker.css">
        <?php echo $html_resource;?>
        <script type="text/javascript" src="backend/js/jquery.datetimepicker.js"></script>
	    <script type="text/javascript" src="backend/js/date_setting.js"></script>
        <script>
            /******************** user define js ********************/
            $(document).ready(function(){
            	
                function init(){
                    $(".pass_btn").click(function(){
                        var $this = $(this);
                        var id = $(this).attr("fi_id");
                        var intro_phone = $("#data_"+id).attr("intro_phone");
                        $.post(filename,{
                                query_type: "pending_pass",
                                fi_id:      id,
                                intro_phone:intro_phone
                            },function(data){
                                data=$.trim(data);
                                if(data.indexOf("success")>-1){
                                    $tr = $this.parent().parent();
                                    if($tr[0].tagName=="TR"){
                                        $tr.remove();
                                        alert2("審核通過");
                                    }else{
                                        alert2($tr[0].tagName);
                                    }
                                }
                        });
                    })
                    $(".del_btn").click(function(){
                        var $this = $(this);
                        var id = $(this).attr("fi_id");
                        var intro_phone = $("#data_"+id).attr("intro_phone");
                        $.post(filename,{
                                query_type: "pending_del",
                                fi_id:      id,
                                intro_phone:intro_phone
                            },function(data){
                                data=$.trim(data);
                                if(data.indexOf("success")>-1){
                                    $tr = $this.parent().parent();
                                    if($tr[0].tagName=="TR"){
                                        $tr.remove();
                                        alert2("刪除成功");
                                    }else{
                                        alert2($tr[0].tagName);
                                    }
                                }
                        });
                    })
                }

                var default_type = getCookie("pending_type")||0;
                $("#selector").val(default_type).change(function(){
                    $("#exprot_btn").hide();

                    $("#list_panel").remove();
                    $("div.pager").remove();
                    $("br").remove();
                    $obj = $("#selector");
                    document.cookie = "pending_type="+$(this).val();
                    if($obj.val()=="1"){
                        $("#exprot_btn").show();
                        $("#exprot_btn").click(function(){
                            $.post(filename,{
                                    query_type: "export"
                                },function(data){
                                    data=$.trim(data);
                                    if(data.indexOf("success")>-1){
                                        location.reload();
                                    }else{
                                        alert2("輸出失敗");
                                    }
                            });
                        })
                    }

                    $.post(filename,{
                                query_type: "get_list",
                                type:               $obj.val(),
                                permissions2add:    <?php echo !$permissions2add?"false":"true";?>
                            },function(data){
                                data=$.trim(data);
                                $table = $(data);
                                $("#body_right").append($table);
                                
                                init_style( $("#list_panel,#list_panel *") );
                                init();

                                if($obj.val()=='1'||$obj.val()=='2'){
                                    $(".pass_btn").hide();
                                    $(".del_btn").hide();
                                    var pending_cht="";
                                    if($obj.val()=='1'){
                                        pending_cht="已審核";
                                    }else if($obj.val()=='2'){
                                        pending_cht="已輸出";
                                    }
                                    $(".del_btn").after(pending_cht);
                                }
                        });
                    
                }).trigger("change");

            });
            function getCookie(name)//取cookies函数        
            {
                var arr = document.cookie.match(new RegExp("(^| )"+name+"=([^;]*)(;|$)"));
                 if(arr != null) return unescape(arr[2]); return null;

            }
        </script>
        <style>
            /******************** user define css ********************/
        </style>
    </head>
    <body>
        <div id="wrapper">
            <!-- ******************** header ******************** -->
            <div id="header">
                <h3><?php echo $html_title; ?></h3>
            </div>
            <!-- /.header -->
            <!-- ******************** body ******************** -->
            <div id="body">
                <div id="body_left">
                    <?php echo $menu; ?>
                </div>
                <!-- /.body_left -->
                <div id="body_right">
                	<select id='selector'>
                        <option value="0">未審核</option>
                        <option value="1">已審核未匯出</option>
                        <option value="2">已匯出</option>
                    </select>
                    <?php
                    if($permissions2edit){
                        echo "<button id='exprot_btn' style='displaly:none;'>匯出Excel</button>";
                    }
                    ?>
                    <?php
                    if(file_exists("download/store_pending.xlsx")){
                        echo "<a href='download/store_pending.xlsx'>報表下載</a>";
                    }
                    ?>
                </div>
                <!-- /.body_right -->
            </div>
            <!-- /.body -->
            <!-- ******************** footer ******************** -->
            <div id="footer">
                <span><?php echo $html_copyright; ?></span>
            </div>
            <!-- /.footer -->
        </div>
        <!-- /.wrapper -->
    </body>
</html>
