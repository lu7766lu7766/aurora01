<?php 
    session_start();
    include 'backend/template.php';
    include 'library/dba.php';
    $dba = new dba();
    if(isset($_POST['account']) && isset($_POST['password']))
    {
        //帳密
        $account = $_POST['account'];
        $password = $_POST['password'];
        
        //撈員工資料
        
        $query = "select * from t_administrator where fv_user='$account'";
        $result = $dba->getAll($query);
        //檢查帳密
        $len = count($result);
        for($i = 0; $i < $len; $i++){
        	if($result[$i]['fv_user'] == $account && $result[$i]['fv_password'] == md5(md5($password))){
            	
                //未啟用返回登入頁
                if($result[$i]['fi_active']==0){
                    header("Location:index.php?status=該帳號已遭到停權，請聯繫管理員");
                }
                
                //記錄登入者資料
                $login_fi_id 		= $result[$i]['fi_id'];
                $login_user 		= $result[$i]['fv_user'];
                $login_name 		= $result[$i]['fv_name'];
                $login_permissions 	= $result[$i]['ft_permissions'];
                $b_all 				= $login_permissions=="all" ? true : false;
                //session寫入
                $_SESSION['admin']['login_fi_id'] = $login_fi_id;
                $_SESSION['admin']['login_name'] = $login_name;
                $_SESSION['admin']['login_permissions'] = $login_permissions;
                $_SESSION['admin']['login_user'] = $login_user;
                $_SESSION['admin']['login_username'] = $login_user."(".$login_name.")";
                $_SESSION['admin']['login_title'] = "<div>管理員".$login_name."先生/小姐您好!".($b_all?"您目前為最高管理權限":"您目前為普通管理者權限")."</div>";
                
                $login_username = $_SESSION['admin']['login_username'];
                $login_title = $_SESSION['admin']['login_title'];
                                
                //製作畫面左側按鈕清單
	            $menu  = "";
	            $menu .= "<p><a href='manage_welcome.php'>首頁</a></p>";
	            $menu .= (strpos($login_permissions,"administrator_list")	!==false||$b_all)? "<p><a href='manage_administrator.php'>	管理者管理</a></p>":"";
	            $menu .= (strpos($login_permissions,"member_list")			!==false||$b_all)? "<p><a href='manage_member.php'>			會員管理</a></p>":"";
	            $menu .= (strpos($login_permissions,"store_type_list")		!==false||$b_all)? "<p><a href='manage_store_type.php'>		分類管理</a></p>":"";
	            $menu .= (strpos($login_permissions,"brand_list")			!==false||$b_all)? "<p><a href='manage_brand.php'>			品牌管理</a></p>":"";
	            $menu .= (strpos($login_permissions,"store_list")			!==false||$b_all)? "<p><a href='manage_store.php'>			店家管理</a></p>":"";
	            $menu .= (strpos($login_permissions,"advertisement_list")	!==false||$b_all)? "<p><a href='manage_advertisement.php'>	廣告管理</a></p>":"";
	            $menu .= (strpos($login_permissions,"promote_type_list")    !==false||$b_all)? "<p><a href='manage_promote_type.php'>   好康分類管理</a></p>":"";
                $menu .= (strpos($login_permissions,"promote_list")			!==false||$b_all)? "<p><a href='manage_promote.php'>		好康專區</a></p>":"";
                $menu .= (strpos($login_permissions,"store_pending_list")   !==false||$b_all)? "<p><a href='manage_store_pending.php'>  店家審核</a></p>":"";
	            $menu .= (strpos($login_permissions,"history_list")			!==false||$b_all)? "<p><a href='manage_history.php'>		撥打紀錄</a></p>":"";
                $menu .= (strpos($login_permissions,"news4cell_list")			!==false||$b_all)? "<p><a href='manage_news4cell.php'>		手機最新消息</a></p>":"";
	            $menu .= (strpos($login_permissions,"filter_list")			!==false||$b_all)? "<p><a href='manage_filter.php'>			Excel重複過濾</a></p>":"";
	            $menu .= (strpos($login_permissions,"convert_list")			!==false||$b_all)? "<p><a href='manage_convert.php'>		Excel批次上傳</a></p>":"";
	            
	            $menu .= "<p><a href='javascript:void(0);'>登出</a></p>";
	            
	            $_SESSION['admin']['menu'] = $menu;
	            //權限中英對照表，空值為了使管理員權限管理對齊
		        $_SESSION['admin']['permissions_assoc'] = array(
		        	'administrator_list'=> "管理者查詢"
		           ,'administrator_add'	=> "管理者新增"
		           ,'administrator_edit'=> "管理者編輯"
		           ,'administrator_del' => "管理者刪除"
		            //
		            ,'member_list' 	=> "會員查詢"
		            ,'member_add'	=> ""
		            ,'member_edit'	=> "會員編輯"
		            ,'member_del' 	=> "會員刪除"
		            //
		            ,'store_type_list' 	=> "分類查詢"
		            ,'store_type_add'	=> "分類新增"
		            ,'store_type_edit'	=> "分類編輯"
		            ,'store_type_del' 	=> "分類刪除"
		            //
		            ,'brand_list' 	=> "品牌查詢"
		            ,'brand_add'	=> "品牌新增"
		            ,'brand_edit'	=> "品牌編輯"
		            ,'brand_del' 	=> "品牌刪除"
		            //
		            ,'store_list' 	=> "店家查詢"
		            ,'store_add'	=> "店家新增"
		            ,'store_edit'	=> "店家編輯"
		            ,'store_del' 	=> "店家刪除"
		            //
		            ,'advertisement_list' 	=> "廣告查詢"
		            ,'advertisement_add'	=> "廣告新增"
		            ,'advertisement_edit'	=> "廣告編輯"
		            ,'advertisement_del' 	=> "廣告刪除"
		            //
                    ,'promote_type_list' => "好康分類查詢"
                    ,'promote_type_add'  => "好康分類新增"
                    ,'promote_type_edit' => "好康分類編輯"
                    ,'promote_type_del'  => "好康分類刪除"
                    //
		            ,'promote_list' => "好康查詢"
		            ,'promote_add'	=> "好康新增"
		            ,'promote_edit'	=> "好康編輯"
		            ,'promote_del' 	=> "好康刪除"
                    //
                    ,'store_pending_list' => "審核查詢"
                    ,'store_pending_add'  => "店家審核"
                    ,'store_pending_edit' => "輸出清單"
                    ,'store_pending_del'  => ""
		            //
		            ,'history_list' => "撥打紀錄查詢"
		            ,'history_add'	=> ""
		            ,'history_edit'	=> ""
		            ,'history_del' 	=> ""
                    //
                    ,'news4cell_list'   => "手機最新消息查詢"
                    ,'news4cell_add'    => "手機最新消息新增"
                    ,'news4cell_edit'	=> "手機最新消息編輯"
                    ,'news4cell_del' 	=> "手機最新消息刪除"
		            //
		            ,'filter_list' 	=> "Excel重複過濾"
		            ,'filter_add'	=> ""
		            ,'filter_edit'	=> ""
		            ,'filter_del' 	=> ""
		            //
		            ,'convert_list' => "Excel批次上傳"
		            ,'convert_add'	=> ""
		            ,'convert_edit'	=> ""
		            ,'convert_del' 	=> ""
		        );
		        $dba->backup_tables();
                //$dba->backup_store_table();
                break;
            }
        }
        //帳號或密碼錯誤
        if(empty($_SESSION['admin']['login_user']))
        {
            header("Location:index.php?status=帳號或密碼錯誤");
        }
    }else{
        //沒Session資料
        if(!isset($_SESSION)){
            header("Location:index.php");
        }else{
	        
            $login_fi_id = $_SESSION['admin']['login_fi_id'];
            $login_name = $_SESSION['admin']['login_name'];
            $login_permissions = $_SESSION['admin']['login_permissions'];
            $login_user = $_SESSION['admin']['login_user'];
            $login_username = $_SESSION['admin']['login_username'];
            $login_title = $_SESSION['admin']['login_title'];
            $menu = $_SESSION['admin']['menu'];
            
        }
        
    }

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo $html_title;?></title>
        <?php echo $html_resource;?>
        <script>
            /******************** user define js ********************/
            $(function(){
                $("#clear").click(function(){
                	$.post("sql_clear.php",{
                			query_type:	"clearn"
						},function(data){
							data=$.trim(data);
                    		alert("刪除成功！");
                    });
                })
            });
        </script>
        <style>
            /******************** user define css ********************/

        </style>
    </head>
    <body>
        <div id="wrapper">
            <!-- ******************** header ******************** -->
            <div id="header">
                <h3><?php echo $html_title; ?></h3>
            </div>
            <!-- /.header -->
            <!-- ******************** body ******************** -->
            <div id="body">
                <div id="body_left">
                    <?php echo $menu; ?>
                </div>
                <!-- /.body_left -->
                <div id="body_right">
                    <?php echo $login_title; ?>
            		<br>
            		<!--<input type='button' name='clear' id='clear' value='刪除品牌與商店'/>-->
                </div>
                <!-- /.body_right -->
                
            </div>
            <!-- /.body -->
            <!-- ******************** footer ******************** -->
            <div id="footer">
                <span><?php echo $html_copyright; ?></span>
            </div>
            <!-- /.footer -->
        </div>
  

        <!-- /.wrapper -->
    </body>
</html>
