<?php
	//ini_set("display_errors", "On");
	//error_reporting(E_ALL);
	
class Convert {
	
	private $start_row = 2;
	public $html = "";
	
	public function __construct() {
		//require_once "view/upload_convert_xls.php";
	}
	//xls上傳轉檔進db
	public function xls2db()
	{
		$a_start = microtime(true);
		
		require_once "library/excel_reader.php";
		require_once "library/dba.php";
		$dba = new dba();
		if(!is_array($_FILES["upload_xls"])){
			//die();
		}
		$sql = "truncate table t_store_convert;";
		$dba->query($sql);
		
		$dba = new dba();
		$type = $_POST["upload_type"];
		if($type==0)die("請選擇分類");
		$this->html .= "<pre>";
		
		if ( !is_array($_FILES['upload_xls']) || !move_uploaded_file($_FILES['upload_xls']['tmp_name'], 'jacconvert.xls') ) {
			die($_FILES['upload_xls']['name']."檔案上傳失敗");
		}else{
			//$html .= "upload success!!<br>";
		}
		
		$data = new Spreadsheet_Excel_Reader("jacconvert.xls");
		//分頁數判斷
		$sheet_len = 0;
		while( $data->rowcount($sheet_len)!="" && $data->colcount($sheet_len)!="" )
		{++$sheet_len;}
		if($sheet_len==0)
		{die("不好意思讀不到檔案，請上傳2003格式，或連繫管理人員!!");}
		//$html .= "sheet_len:".$sheet_len."^^".$data->rowcount($sheet_len)."^^".$data->colcount($sheet_len)."<br>";
		$process_num = 0;
		for ($sheet=0;$sheet<$sheet_len;$sheet++ )
		{
			//欄位尋找即預設
			$rows = $data->rowcount($sheet);//行
			$cols = $data->colcount($sheet);//列
			//$html .= "sheet:".$sheet."^^rows:".$rows."^^cols:".$cols."<br>";
			$id_col = $this->get_colnum("編號",$data,$sheet);
			$id_col = $id_col==0?1:$id_col;
			
			$store_name_col = $this->get_colnum("店名",$data,$sheet);
			$store_name_col = $store_name_col==0?2:$store_name_col;
			
			$source_phone_col = $this->get_colnum("電話",$data,$sheet);
			$source_phone_col = $source_phone_col==0?3:$source_phone_col;
			
			$address_col = $this->get_colnum("地址",$data,$sheet);
			$address_col = $address_col==0?4:$address_col;
			
			$subname_col = $this->get_colnum("子店名",$data,$sheet);
			$subname_col = $subname_col==0?5:$subname_col;
			
			$phone_col = $this->get_colnum("55104",$data,$sheet);
			$phone_col = $address_col==0?6:$phone_col;
		
			//die("編號：".$id_col."<br>店名：".$store_name_col."<br>電話：".$source_phone_col."<br>地址：".$address_col);
			
			//資料筆數
			for( $i=$this->start_row ;$i<=$rows ;$i++ )
			{
				//db process
				$store_name = $data->val($i,$store_name_col,$sheet);
				$source_phone = strtr($data->val($i,$source_phone_col,$sheet)
										,array("-"=>"","("=>"",")"=>""));
				$address = $data->val($i,$address_col,$sheet);
				$subname = $data->val($i,$subname_col,$sheet);
				$phone = $data->val($i,$phone_col,$sheet);
				
				if( $store_name==""	|| $source_phone=="" || $address=="" || $phone=="" )
				{
					$this->html .= "編號:".($i-1)." 店名，電話，地址，55104欄位皆不可為空白。請確認資料完整。<br>";
					continue;
				}
				
				//$html .= "-----------------------store_filter_start---------------------------<br>";
				$result = $dba->getAll("select fi_id from t_brand where fv_brand_name='$store_name' limit 1");
				if(count($result)>0)
				{
					$brand = $result[0]["fi_id"];
				}else{
					$weights_data = $dba->getAll("select max(fi_weights)+1 as weights from t_brand where fi_type='$type'");
					$weights = !is_numeric($weights_data[0]["weights"])?"1":$weights_data[0]["weights"];
					$sql = "insert into t_brand (fi_type,fv_brand_name,fi_active,fi_weights)
						select * from (select '$type' as a,'$store_name' as b,'1' as c,'$weights' as d) as tmp
							where not exists (
								select 1 from t_brand where fv_brand_name = '$store_name' and fi_type='$type'
							) limit 1";
					$dba->query($sql);
					$brand = $dba->insert_id();
					//$this->html .= $sql;
				}
				//判斷 電話 相符 存在於 t_store
				$store_data = $dba->getAll("select 1 from t_store where fv_source_phone = '$source_phone'");
				if(count($store_data)>0)
				{
					$id = $i-1;
					$this->html .= "編號:{$id} 店名:{$store_name} 電話:{$source_phone} 地址:{$address} 子店名:{$subname} 55104:{$phone} 該資料已存在於資料庫<br>";
					$dba->query("insert into t_store_convert (fi_id,fv_store_name,fv_phone,fv_source_phone,fv_address,fv_subname)
													values('$id','$store_name','$phone','$source_phone','$address','$subname')");
				}else{
					$sql = "INSERT INTO t_store (fi_brand,fv_source_phone,fv_phone,fv_subname,fv_address,ft_create)
								values('$brand','$source_phone','$phone','$subname','$address',now());";
					//echo $sql."<br>";
					$dba->query($sql);
					//$this->html .=$sql."<br>";
				}
				//$html .= $sql."<br>";
			}
		}
		@unlink("jacconvert.xls");
		$this->db2xls();
		
		$a_end = microtime(true);
		$space = $a_end - $a_start;
		$this->html .= "<br>{$space}秒<br>";
		
		return $this;
	}
	//從關鍵字尋早“列”數
	private function get_colnum($colname,$data,$sheet=0)
	{
		$cols = $data->colcount($sheet);//列
		$rows = $data->colcount($sheet);//列
		$key_words = explode(";", $colname);
		$key_len = count($key_words);
		$b_find = false;
		
		for($row=1;$row<=$rows&&!$b_find;$row++)
		{
			for($i=1;$i<=$cols;$i++)
			{
				$tmp_v="";
				foreach($key_words as $key_word)
				{
					if( strpos($data->val($row,$i,$sheet),$key_word)!==false )
					{
						$tmp_v.="1";
					}
					else
					{
						break;
					}
						
					if(strlen($tmp_v)==$key_len)
					{
						$this->start_row = $row+1;
						$b_find = true;
						return $i;
					}
				}
			}
		}
		return 0;
	}
	public function db2xls()
	{ 
		require_once "library/dba.php";
		$dba = new dba();
		$sql = "select * from t_store_convert;";
		$result = $dba->getAll($sql);
		$len = count($result);
		if($len==0||$len=="")	return "";
		
		require_once "library/Classes/PHPExcel.php"; 
		require_once "library/Classes/PHPExcel/IOFactory.php";
		$objPHPExcel = new PHPExcel(); 
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("D")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("E")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("F")->setAutoSize(true);
		
		$objPHPExcel->getActiveSheet()->setCellValue("A1","編號"); 
		$objPHPExcel->getActiveSheet()->setCellValue("B1","店名"); 
		$objPHPExcel->getActiveSheet()->setCellValue("C1","電話"); 
		$objPHPExcel->getActiveSheet()->setCellValue("D1","地址");
		$objPHPExcel->getActiveSheet()->setCellValue("E1","子店名");
		$objPHPExcel->getActiveSheet()->setCellValue("F1","55104");
		
		$line = 2;
		for($i=0;$i<$len;$i++)
		{
			$objPHPExcel->getActiveSheet()->setCellValue("A".$line,$result[$i]["fi_id"]); 
			$objPHPExcel->getActiveSheet()->setCellValue("B".$line,$result[$i]["fv_store_name"]); 
			$objPHPExcel->getActiveSheet()->setCellValue("C".$line,$result[$i]["fv_source_phone"]); 
			$objPHPExcel->getActiveSheet()->setCellValue("D".$line,$result[$i]["fv_address"]);
			$objPHPExcel->getActiveSheet()->setCellValue("E".$line,$result[$i]["fv_subname"]);
			$objPHPExcel->getActiveSheet()->setCellValue("F".$line,$result[$i]["fv_phone"]);
			$line++;
		}
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
		$objWriter->save('download/convert.xlsx');
	}
}
?>