<?php
	//ini_set("display_errors", "On");
	//error_reporting(E_ALL);
	//require_once(dirname(__FILE__)."\library\library.php");
	set_time_limit(0);
	include_once "library/library.php";
class Convert {
	
	private $start_row = 2;
	public $html = "";
	
	public function __construct() {
		//echo $_SERVER['REQUEST_URI'];
		//require_once "view/upload_convert_xls.php";
	}
	//xls上傳轉檔進db
	public function xls2db()
	{
		$a_start = microtime(true);
		
		require_once("library/Classes/PHPExcel/IOFactory.php");
		require_once "library/dba.php";
		$dba = new dba();
		$sql = "delete from t_store_convert;";
		$dba->query($sql);
		
		$type = $_POST["upload_type"];
		if($type==0||$type=="")die("請選擇分類");
		$this->html .= "<table cellpadding='5' class='uninit'>";
		if ( !is_array($_FILES['upload_xls']) || !move_uploaded_file($_FILES['upload_xls']['tmp_name'], 'jacconvert.xls') ) {
			die($_FILES['upload_xls']['name']."檔案上傳失敗");
		}else{
			//$html .= "upload success!!<br>";
		}

		//try{
			$objPHPexcel = PHPExcel_IOFactory::load("jacconvert.xls");
		//}catch(exception $e){
		//	die("無法讀取檔案！！");
		//}
		//分頁數判斷
		$sheet_len = 0;
		try{
			$sheet_len = $objPHPexcel->getSheetCount();
			
			if($sheet_len==0)
			{die("不好意思讀不到檔案，請連繫管理人員!!");}
		}catch(exception $e){
			die("不好意思讀不到檔案，請連繫管理人員!!");
		}
		if($sheet_len>1)
		{
			while($objPHPexcel->setActiveSheetIndex(--$sheet_len) 
				&& $objPHPexcel->getActiveSheet()->getHighestRow()==1 
				&& $objPHPexcel->getActiveSheet()->getHighestColumn()=='A')
			{}
			$sheet_len++;
		}
		//$html .= "sheet_len:".$sheet_len."^^".$data->rowcount($sheet_len)."^^".$data->colcount($sheet_len)."<br>";
		//echo "sheet_len:".$sheet_len."^^".$data->rowcount($sheet_len)."^^".$data->colcount($sheet_len)."<br>";
		for ($sheet=0;$sheet<$sheet_len;$sheet++ )
		{
			$objPHPexcel->setActiveSheetIndex($sheet);
			$data = $objPHPexcel->getActiveSheet();
			//欄位尋找即預設
			$rows = $data->getHighestRow();
			//$rows = $data->rowcount($sheet);//行
			$cols = $data->getHighestColumn();
			//$cols = $data->colcount($sheet);//列
			//$html .= "sheet:".$sheet."^^rows:".$rows."^^cols:".$cols."<br>";
			$id_col = Library::get_colnum("編號",$data);
			$id_col = $id_col==0?'A':$id_col;
			
			$store_name_col = Library::get_colnum("店名",$data);
			$store_name_col = $store_name_col==0?'B':$store_name_col;
			
			$source_phone_col = Library::get_colnum("電話",$data);
			$source_phone_col = $phone_col==0?'C':$source_phone_col;
			
			$address_col = Library::get_colnum("地址",$data);
			$address_col = $address_col==0?'D':$address_col;
			
			$subname_col = Library::get_colnum("子店名",$data);
			$subname_col = $address_col==0?'E':$subname_col;
			
			$phone_col = Library::get_colnum("55104||分機",$data);
			$phone_col = $address_col==0?'F':$phone_col;
		
			//die("編號：".$id_col."<br>店名：".$store_name_col."<br>電話：".$source_phone_col."<br>地址：".$address_col);
			$this->html .= "<table cellpadding='5' class='table-n'>";
			//資料筆數
			for( $i=$this->start_row ;$i<=$rows ;$i++ )
			{
				//db process
				$store_name = $data->getCell($store_name_col.$i)->getValue();
				$source_phone = strtr($data->getCell($source_phone_col.$i)->getValue()
										,array("-"=>"","("=>"",")"=>"","\t"=>""," "=>""));
				if( substr($source_phone, 0,1)!="0" )
				{
					$source_phone = "0".$source_phone;
				}
				$address = $data->getCell($address_col.$i)->getValue();
				$address = strtr($address,array(" "=>"","\t"=>"",));
				$subname = $data->getCell($subname_col.$i)->getValue();
				$phone = $data->getCell($phone_col.$i)->getValue();
				
				if( $store_name==""	|| $source_phone=="" || $address=="" || $phone=="" )
				{
					$this->html .= "編號:".($i-1)." 店名，電話，地址，55104欄位皆不可為空白。請確認資料完整。<br>";
					continue;
				}
				
				//$html .= "-----------------------store_filter_start---------------------------<br>";
				$result = $dba->getAll("select fi_id from t_brand where fv_brand_name='$store_name' limit 1");
				if(count($result)>0)
				{
					$brand = $result[0]["fi_id"];
				}else{
					$weights_data = $dba->getAll("select max(fi_weights)+1 as weights from t_brand where fi_type='$type'");
					$weights = !is_numeric($weights_data[0]["weights"])?"1":$weights_data[0]["weights"];
					$sql = "insert into t_brand (fi_type,fv_brand_name,fi_active,fi_weights)
						select * from (select '$type' as a,'$store_name' as b,'1' as c,'$weights' as d) as tmp
							where not exists (
								select 1 from t_brand where fv_brand_name = '$store_name' and fi_type='$type'
							) limit 1";
					$dba->query($sql);
					$brand = $dba->insert_id();
					//$this->html .= $sql;
				}
				//判斷 電話 相符 存在於 t_store
				$store_data = $dba->getAll("select 1 from t_store where fv_source_phone = '$source_phone'");
				if(count($store_data)>0)
				{
					$id = $i-1;
					$this->html .= "<tr><td>編號:{$id}</td><td>店名:{$store_name}</td><td>電話:{$source_phone}</td><td>地址:{$address}</td><td>子店名:{$subname} 55104:{$phone}</td><td>該資料已存在於資料庫</td></tr>";
					$dba->query("insert into t_store_convert 
								(fi_id	,fv_store_name	,fv_phone	,fv_source_phone	,fv_address	,fv_subname)
								values
								('$id'	,'$store_name'	,'$phone'	,'$source_phone'	,'$address'	,'$subname')");
				}else{
					//$r = Library::getLatLng($address);
					//$latitude = $r["lat"];
					//$longitude = $r["lng"];
					/*$sql = "INSERT INTO t_store 
							(fi_brand,	fv_source_phone,	fv_phone,	fv_subname,
							fv_address,	ft_create,			ff_latitude,ff_longitude,	fp_geo)
					values  ('$brand',	'$source_phone',	'$phone',	'$subname',
							'$address',	now(),				'$latitude','$longitude',	GeomFromText( 'POINT(".$latitude.' '.$longitude.")') );";
					/**/
					
					$sql = "INSERT INTO t_store 
							(fi_brand,	fv_source_phone,	fv_phone,	fv_subname,
							fv_address,	ft_create			)
					values  ('$brand',	'$source_phone',	'$phone',	'$subname',
							'$address',	now()				);";
					/**/
					
					//echo $sql."<br>";
					$dba->query($sql);
					//$this->html .=$sql."<br>";
				}
				//$html .= $sql."<br>";
			}
			$this->html .= "</table>";
		}
		@unlink("jacconvert.xls");
		$this->db2xls();
		
		$a_end = microtime(true);
		$space = $a_end - $a_start;
		$this->html .= "<br>{$space}秒<br>";
		
		return $this;
	}
	//從關鍵字尋早“列”數
	
	/*private function getLatLng($address)
	{       
		$allurl = "http://maps.googleapis.com/maps/api/geocode/json?address=".$address;
		//print_r( json_decode(file_get_contents($allurl),true) );
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $allurl);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-FORWARDED-FOR:8.8.8.8', 'CLIENT-IP:8.8.8.8'));  //构造IP

		curl_setopt($ch, CURLOPT_REFERER, "http://www.gosoa.com.cn/ ");
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$all = curl_exec($ch);
		curl_close($ch);
		if(strpos($all,"That’s an error")!==false){
			return false;
		}
		$a_pos = json_decode($all,true);
		if( !is_array($a_pos) || count($a_pos)==0 )
		{
			
		}
		//print_r($a_pos);
		$lat = $a_pos["results"][0]["geometry"]["location"]["lat"];
		$lng = $a_pos["results"][0]["geometry"]["location"]["lng"];
		$r["lat"] = $lat;
		$r["lng"] = $lng;
		sleep(1);
		ob_flush();
		flush();
		return $r;
		
	}
	private function getLatLng2($address)
	{
		
	}*/
	
	public function db2xls()
	{ 
		require_once "library/dba.php";
		$dba = new dba();
		$sql = "select * from t_store_convert;";
		$result = $dba->getAll($sql);
		$len = count($result);
		if($len==0||$len=="")	return "";
		
		require_once "library/Classes/PHPExcel.php"; 
		require_once "library/Classes/PHPExcel/IOFactory.php";
		$objPHPExcel = new PHPExcel(); 
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("D")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("E")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("F")->setAutoSize(true);
		
		$objPHPExcel->getActiveSheet()->setCellValue("A1","編號"); 
		$objPHPExcel->getActiveSheet()->setCellValue("B1","店名"); 
		$objPHPExcel->getActiveSheet()->setCellValue("C1","電話"); 
		$objPHPExcel->getActiveSheet()->setCellValue("D1","地址");
		$objPHPExcel->getActiveSheet()->setCellValue("E1","子店名");
		$objPHPExcel->getActiveSheet()->setCellValue("F1","55104");
		
		$line = 2;
		for($i=0;$i<$len;$i++)
		{
			$objPHPExcel->getActiveSheet()->setCellValue("A".$line,$result[$i]["fi_id"]); 
			$objPHPExcel->getActiveSheet()->setCellValue("B".$line,$result[$i]["fv_store_name"]); 
			//$objPHPExcel->getActiveSheet()->setCellValue("C".$line,$result[$i]["fv_source_phone"]);
			//echo $result[$i]["fv_source_phone"]."^^";
			$objPHPExcel->getActiveSheet()->getCell("C".$line)->setValueExplicit($result[$i]["fv_source_phone"], PHPExcel_Cell_DataType::TYPE_STRING); 
			$objPHPExcel->getActiveSheet()->setCellValue("D".$line,$result[$i]["fv_address"]);
			$objPHPExcel->getActiveSheet()->setCellValue("E".$line,$result[$i]["fv_subname"]);
			//$objPHPExcel->getActiveSheet()->setCellValue("F".$line,$result[$i]["fv_phone"]);
			$objPHPExcel->getActiveSheet()->getCell("F".$line)->setValueExplicit($result[$i]["fv_phone"], PHPExcel_Cell_DataType::TYPE_STRING); 
			
			$line++;
		}
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
		$objWriter->save('download/convert.xlsx');
	}
}
?>