<?php
	//ini_set("display_errors", "On");
	//error_reporting(E_ALL);
	
class Filter {
	
	private $start_row = 2;
	public $html='';
	
	public function __construct() {
		//require_once "view/upload_xls.php";
		
	}
	//xls上傳轉檔進db
	public function xls2db()
	{
		require_once "library/excel_reader.php";
		if(!is_array($_FILES["upload_xls"])){
			//die();
		}
		
		$con = mysqli_connect("localhost", "aurora01", "auroraaurora01", "aurora");
		if(mysqli_connect_errno()) {echo "Failed to connect to MySQL: ".mysqli_connect_error();}
		mysqli_query($con, "SET NAMES 'UTF8'"); 
		mysqli_query($con, "SET CHARACTER SET UTF8");
		mysqli_query($con, "SET CHARACTER_SET_RESULTS=UTF8");
		
		//echo "truncate table<br>";
		$sql = "truncate table t_store_filter;";
		mysqli_query($con, $sql);
		//echo "clear complete!!<br><br>";
		//$sql = "ALTER TABLE t_store_filter AUTO_INCREMENT=1;";
		//mysqli_query($con, $sql);
		//mysqli_close($con);
		/**/
		$this->html.= "<pre>";
		
		$a_start = microtime(true);
		
		if ( !is_array($_FILES['upload_xls']) || !move_uploaded_file($_FILES['upload_xls']['tmp_name'], 'jacconvert.xls') ) {
			die($_FILES['upload_xls']['name']."檔案上傳失敗");
		}else{
			//echo "upload success!!<br>";
		}
		
		$data = new Spreadsheet_Excel_Reader("jacconvert.xls");
		//分頁數判斷
		$sheet_len = 0;
		echo $data->rowcount($sheet_len)."^^".$data->colcount($sheet_len);
		while( $data->rowcount($sheet_len)!="" && $data->colcount($sheet_len)!="" )
		{++$sheet_len;}
		if($sheet_len==0)
		{die("不好意思讀不到檔案，請上傳2003格式，或連繫管理人員!!");}
		//echo "sheet_len:".$sheet_len."^^".$data->rowcount($sheet_len)."^^".$data->colcount($sheet_len)."<br>";
		$process_num = 0;
		for ($sheet=0;$sheet<$sheet_len;$sheet++ )
		{
			//欄位尋找即預設
			$rows = $data->rowcount($sheet);//行
			$cols = $data->colcount($sheet);//列
			//echo "sheet:".$sheet."^^rows:".$rows."^^cols:".$cols."<br>";
			$id_col = $this->get_colnum("編號",$data,$sheet);
			$id_col = $id_col==0?1:$id_col;
			
			$store_name_col = $this->get_colnum("店名",$data,$sheet);
			$store_name_col = $store_name_col==0?2:$store_name_col;
			
			$phone_col = $this->get_colnum("電話",$data,$sheet);
			$phone_col = $phone_col==0?3:$phone_col;
			
			$address_col = $this->get_colnum("地址",$data,$sheet);
			$address_col = $address_col==0?4:$address_col;
			
			$subname_col = $this->get_colnum("子店名",$data,$sheet);
			$subname_col = $address_col==0?5:$subname_col;
		
			//die("編號：".$id_col."<br>店名：".$store_name_col."<br>電話：".$phone_col."<br>地址：".$address_col);
			
			//資料筆數
			for( $i=$this->start_row ;$i<=$rows ;$i++ )
			{
				//db process
				$store_name = $data->val($i,$store_name_col,$sheet);
				$phone = $data->val($i,$phone_col,$sheet);
				$address = $data->val($i,$address_col,$sheet);
				$subname = $data->val($i,$subname_col,$sheet);
				
				if( $store_name==""	|| $phone=="" || $address=="" )
				{
					$this->html.= "編號:".($i-1)." 店名，電話，地址皆不可為空白。請確認資料完整。<br>";
					continue;
				}
				
				//echo "-----------------------store_filter_start---------------------------<br>";
				
				//判斷 電話 與 地址 相符 存在於 stroe_filter
				$sql = "INSERT INTO t_store_filter (fv_store_name,fv_phone,fv_address,fv_subname)
						SELECT * FROM (SELECT '$store_name','$phone','$address','$subname') AS tmp
							WHERE NOT EXISTS (
									SELECT 1 FROM t_store_filter WHERE fv_phone = '$phone' and fv_address = '$address'
							) LIMIT 1;";
				mysqli_query($con, $sql);
				//echo $sql."<br>";
			}
		}
		@unlink("jacconvert.xls");
		$this->db2xls();
		
		$a_end = microtime(true);
		$space = $a_end - $a_start;
		$this->html.= "<br>{$space}秒<br>";
		
		return $this;
	}
	//從關鍵字尋早“列”數
	private function get_colnum($colname,$data,$sheet=0)
	{
		$cols = $data->colcount($sheet);//列
		$rows = $data->colcount($sheet);//列
		$key_words = explode(";", $colname);
		$key_len = count($key_words);
		$b_find = false;
		
		for($row=1;$row<=$rows&&!$b_find;$row++)
		{
			for($i=1;$i<=$cols;$i++)
			{
				$tmp_v="";
				foreach($key_words as $key_word)
				{
					if( strpos($data->val($row,$i,$sheet),$key_word)!==false )
					{
						$tmp_v.="1";
					}
					else
					{
						break;
					}
						
					if(strlen($tmp_v)==$key_len)
					{
						$this->start_row = $row+1;
						$b_find = true;
						return $i;
					}
				}
			}
		}
		return 0;
	}
	public function db2xls()
	{
		require_once "library/Classes/PHPExcel.php"; 
		require_once "library/Classes/PHPExcel/IOFactory.php";
		$objPHPExcel = new PHPExcel(); 
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("D")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("E")->setAutoSize(true);
		
		$objPHPExcel->getActiveSheet()->setCellValue("A1","編號"); 
		$objPHPExcel->getActiveSheet()->setCellValue("B1","店名"); 
		$objPHPExcel->getActiveSheet()->setCellValue("C1","電話"); 
		$objPHPExcel->getActiveSheet()->setCellValue("D1","地址");
		$objPHPExcel->getActiveSheet()->setCellValue("E1","子店名");
		
		$con = mysqli_connect("localhost", "aurora01", "auroraaurora01", "aurora");
		if(mysqli_connect_errno()) {echo "Failed to connect to MySQL: ".mysqli_connect_error();}
		mysqli_query($con, "SET NAMES 'UTF8'"); 
		mysqli_query($con, "SET CHARACTER SET UTF8");
		mysqli_query($con, "SET CHARACTER_SET_RESULTS=UTF8");
		
		$sql = "select * from t_store_filter;";
		$result = mysqli_query($con, $sql);
		$line = 2;
		while( $row = mysqli_fetch_assoc($result) )
		{
			$objPHPExcel->getActiveSheet()->setCellValue("A".$line,$line-1); 
			$objPHPExcel->getActiveSheet()->setCellValue("B".$line,$row["fv_store_name"]); 
			$objPHPExcel->getActiveSheet()->setCellValue("C".$line,$row["fv_phone"]); 
			$objPHPExcel->getActiveSheet()->setCellValue("D".$line,$row["fv_address"]);
			$objPHPExcel->getActiveSheet()->setCellValue("E".$line,$row["fv_subname"]);
			$line++;
		}
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
		$objWriter->save('download/filter.xlsx');
		//echo $_SERVER['HTTP_HOST']."/aurora01/download/filter.xlsx";
		//echo "<BR>";
		//header("Refresh: 0; url=".$_SERVER['HTTP_HOST']."/aurora01/download/filter.xlsx");
		//header("Location:".$_SERVER['HTTP_HOST']."/download/filter.xlsx");
		//header("Location:".$_SERVER['HTTP_HOST'].__FILE__);
		
		/*echo "
			<script>
				alert('c');
				window.open('download/filter.xlsx', '下載視窗', config='height=20,width=20');
				alert('d');
			</script>
		";*/
	}
}
?>