<?php
	//ini_set("display_errors", "On");
	//error_reporting(E_ALL);
	include_once "library/library.php";
class Filter {
	
	private $start_row = 2;
	public $html='';
	
	public function __construct() {
		//require_once "view/upload_xls.php";
		
	}
	//xls上傳轉檔進db
	public function xls2db()
	{
		require_once("library/Classes/PHPExcel/IOFactory.php");
		require_once "library/dba.php";
		$dba = new dba();
		
		//echo "truncate table<br>";
		$sql = "truncate table t_store_filter;";
		$dba->query($sql);
		//echo "clear complete!!<br><br>";
		//$sql = "ALTER TABLE t_store_filter AUTO_INCREMENT=1;";
		//mysqli_query($con, $sql);
		//mysqli_close($con);
		/**/
		$this->html.= "<pre>";
		
		$a_start = microtime(true);
		
		if ( !is_array($_FILES['upload_xls']) || !move_uploaded_file($_FILES['upload_xls']['tmp_name'], 'jacconvert.xls') ) {
			die($_FILES['upload_xls']['name']."檔案上傳失敗");
		}else{
			//echo "upload success!!<br>";
		}
		
		$objPHPexcel = PHPExcel_IOFactory::load("jacconvert.xls");
		
		$sheet_len = 0;
		try{
			$sheet_len = $objPHPexcel->getSheetCount();
			
			if($sheet_len==0)
			{die("不好意思讀不到檔案，請連繫管理人員!!");}
		}catch(exception $e){
			die("不好意思讀不到檔案，請連繫管理人員!!");
		}
		if($sheet_len>1)
		{
			while($objPHPexcel->setActiveSheetIndex(--$sheet_len) 
				&& $objPHPexcel->getActiveSheet()->getHighestRow()==1 
				&& $objPHPexcel->getActiveSheet()->getHighestColumn()=='A')
			{}
			$sheet_len++;
		}
		//$data = new Spreadsheet_Excel_Reader("jacconvert.xls");
		//分頁數判斷
		
		//echo "sheet_len:".$sheet_len."^^".$data->rowcount($sheet_len)."^^".$data->colcount($sheet_len)."<br>";
		//$process_num = 0;
		for ($sheet=0;$sheet<$sheet_len;$sheet++ )
		{
			$objPHPexcel->setActiveSheetIndex($sheet);
			$data = $objPHPexcel->getActiveSheet();
			//欄位尋找即預設
			$rows = $data->getHighestRow();
			//$rows = $data->rowcount($sheet);//行
			$cols = $data->getHighestColumn();
			//$cols = $data->colcount($sheet);//列
			//echo "sheet:".$sheet."^^rows:".$rows."^^cols:".$cols."<br>";
			$id_col = Library::get_colnum("編號",$data);
			$id_col = $id_col==0?'A':$id_col;
			
			$store_name_col = Library::get_colnum("店名",$data);
			$store_name_col = $store_name_col==0?'B':$store_name_col;
			
			$sphone_col = Library::get_colnum("電話",$data);
			$sphone_col = $phone_col==0?'C':$phone_col;
			
			$address_col = Library::get_colnum("地址",$data);
			$address_col = $address_col==0?'D':$address_col;
			
			$subname_col = Library::get_colnum("子店名",$data);
			$subname_col = $address_col==0?'E':$subname_col;
			
			$phone_col = Library::get_colnum("55104||分機",$data);
			$phone_col = $address_col==0?'F':$subname_col;
		
			//die("編號：".$id_col."<br>店名：".$store_name_col."<br>電話：".$phone_col."<br>地址：".$address_col);
			
			//資料筆數
			for( $i=$this->start_row ;$i<=$rows ;$i++ )
			{
				//db process
				$store_name = $data->getCell($store_name_col.$i)->getValue();
				$sphone = strtr($data->getCell($sphone_col.$i)->getValue()
								,array("-"=>"","("=>"",")"=>""));
				$address = $data->getCell($address_col.$i)->getValue();
				$subname = $data->getCell($subname_col.$i)->getValue();
				$phone = $data->getCell($phone_col.$i)->getValue();
				
				if( $store_name==""	|| $phone=="" || $address=="" )
				{
					$this->html.= "編號:".($i-1)." 店名，電話，地址皆不可為空白。請確認資料完整。<br>";
					continue;
				}
				
				//echo "-----------------------store_filter_start---------------------------<br>";
				
				//判斷 電話 存在於 stroe_filter  //與 地址
				$sql = "INSERT INTO t_store_filter (fv_store_name,fv_source,phone,fv_address,fv_subname,fv_phone)
						SELECT * FROM (SELECT '$store_name','$sphone','$address','$subname','$phone') AS tmp
							WHERE NOT EXISTS (
									SELECT 1 FROM t_store_filter WHERE fv_phone = '$phone'
							) LIMIT 1;";
				$dba->query($sql);
				//echo $sql."<br>";
			}
		}
		@unlink("jacconvert.xls");
		$this->db2xls();
		
		$a_end = microtime(true);
		$space = $a_end - $a_start;
		$this->html.= "<br>{$space}秒<br>";
		
		return $this;
	}
	//從關鍵字尋早“列”數
	
	public function db2xls()
	{
		require_once "library/Classes/PHPExcel.php"; 
		require_once "library/Classes/PHPExcel/IOFactory.php";
		require_once "library/dba.php";
		$dba = new dba();
		$objPHPExcel = new PHPExcel(); 
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("D")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("E")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("F")->setAutoSize(true);
		
		$objPHPExcel->getActiveSheet()->setCellValue("A1","編號"); 
		$objPHPExcel->getActiveSheet()->setCellValue("B1","店名"); 
		$objPHPExcel->getActiveSheet()->setCellValue("C1","電話"); 
		$objPHPExcel->getActiveSheet()->setCellValue("D1","地址");
		$objPHPExcel->getActiveSheet()->setCellValue("E1","子店名");
		$objPHPExcel->getActiveSheet()->setCellValue("F1","55104");
		
		
		$sql = "select * from t_store_filter;";
		$result = $dba->getAll($sql);
		$line = 2;
		$len=count($result);
		for($i=0;$i<$len;$i++)
		{
			$objPHPExcel->getActiveSheet()->setCellValue("A".$line,$line-1); 
			$objPHPExcel->getActiveSheet()->setCellValue("B".$line,$result[$i]["fv_store_name"]); 
			//$objPHPExcel->getActiveSheet()->setCellValue("C".$line,$result[$i]["fv_source_phone"]); 
			$objPHPExcel->getActiveSheet()->getCell("C".$line)->setValueExplicit($result[$i]["fv_source_phone"], PHPExcel_Cell_DataType::TYPE_STRING); 
			$objPHPExcel->getActiveSheet()->setCellValue("D".$line,$result[$i]["fv_address"]);
			$objPHPExcel->getActiveSheet()->setCellValue("E".$line,$result[$i]["fv_subname"]);
			//$objPHPExcel->getActiveSheet()->setCellValue("F".$line,$result[$i]["fv_phone"]);
			$objPHPExcel->getActiveSheet()->getCell("F".$line)->setValueExplicit($result[$i]["fv_phone"], PHPExcel_Cell_DataType::TYPE_STRING);
			$line++;
		}
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
		$objWriter->save('download/filter.xlsx');
		//echo $_SERVER['HTTP_HOST']."/aurora01/download/filter.xlsx";
		//echo "<BR>";
		//header("Refresh: 0; url=".$_SERVER['HTTP_HOST']."/aurora01/download/filter.xlsx");
		//header("Location:".$_SERVER['HTTP_HOST']."/download/filter.xlsx");
		//header("Location:".$_SERVER['HTTP_HOST'].__FILE__);
		
		/*echo "
			<script>
				alert('c');
				window.open('download/filter.xlsx', '下載視窗', config='height=20,width=20');
				alert('d');
			</script>
		";*/
	}
}
?>