<?php
	//ini_set("display_errors", "On");
	//error_reporting(E_ALL);
	//require_once(dirname(__FILE__)."\library\library.php");
	set_time_limit(0);
class StorePending {
	
	
	public function __construct() {
		//echo $_SERVER['REQUEST_URI'];
		//require_once "view/upload_convert_xls.php";
	}
	
	
	public function db2xls()
	{ 
		require_once "library/dba.php";
		$dba = new dba();
		$sql = "select fv_brand_name,fv_store_phone,fv_address,fv_subname 
				from t_store_pending
				where fi_pending='1' 
					and fi_export='0';";
		$result = $dba->getAll($sql);
		$len = count($result);
		if($len==0||$len=="")	return "";
		
		require_once "library/Classes/PHPExcel.php"; 
		require_once "library/Classes/PHPExcel/IOFactory.php";
		$objPHPExcel = new PHPExcel(); 
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("D")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("E")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("F")->setAutoSize(true);
		
		$objPHPExcel->getActiveSheet()->setCellValue("A1","編號"); 
		$objPHPExcel->getActiveSheet()->setCellValue("B1","店名"); 
		$objPHPExcel->getActiveSheet()->setCellValue("C1","電話"); 
		$objPHPExcel->getActiveSheet()->setCellValue("D1","地址");
		$objPHPExcel->getActiveSheet()->setCellValue("E1","子店名");
		$objPHPExcel->getActiveSheet()->setCellValue("F1","55104");
		
		$line = 2;
		for($i=0;$i<$len;$i++)
		{
			$objPHPExcel->getActiveSheet()->setCellValue("A".$line,($line-1)); 
			$objPHPExcel->getActiveSheet()->setCellValue("B".$line,$result[$i]["fv_brand_name"]);
			$objPHPExcel->getActiveSheet()->getCell("C".$line)->setValueExplicit($result[$i]["fv_store_phone"], PHPExcel_Cell_DataType::TYPE_STRING); 
			$objPHPExcel->getActiveSheet()->setCellValue("D".$line,$result[$i]["fv_address"]);
			$objPHPExcel->getActiveSheet()->setCellValue("E".$line,$result[$i]["fv_subname"]); 

		}
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
		$objWriter->save('download/store_pending.xlsx');
		$result = $dba->query("update t_store_pending set fi_export='1' where fi_export='0';");
		return $result;
	}
}
?>