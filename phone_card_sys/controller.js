
angular


    .module('app', ['angularFileUpload','ngXlsx'])


    .controller('AppController', ['$scope', '$http', '$filter', 'FileUploader','ngXlsx', function($scope, $http, $filter, FileUploader,ngXlsx) {

        var self = this;

        var ajax_host = "http://125.227.84.249:3000/";

        var uploader = $scope.uploader = new FileUploader({
            //url: 'upload.php'
            url:"http://125.227.84.249:3000/upload"
            //url:"http://"+document.location.hostname+":3000/upload"
        });

        // FILTERS

        uploader.filters.push({
            name: 'customFilter',
            fn: function(item /*{File|FileLikeObject}*/, options) {
                return this.queue.length < 10 && !uploader.hasHTML5 ? true : /\/(xls|xslx)$/.test(item.file.type);
            }
        });

        // CALLBACKS

        var proccess_items = "";

        uploader.onCompleteItem = function(fileItem, response, status, headers) {
            $scope.show_upload=false;
            //console.info('onCompleteItem', response);
            response = JSON.parse(response);
            $http({
                url: ajax_host+"save_xls",
                method: "GET",
                params: {file_name: response.filename}
            }).success(function(data){
                proccess_items += "1";
                if(data=="unmatch")
                {
                    alert("資料格式不符，請依照範本撰寫");
                }
                //console.log(data+"^^");
            });
        };
        uploader.onCompleteAll = function() {
            $scope.show_upload=false;
            $scope.change = [];
            //$http({
            //    url: ajax_host+"get_data",
            //    method: "GET"
            //}).success(function(response){
            //
            //    self.card_detail = JSON.parse(response);
            //    //console.log(self.card_detail);
            //});

            //$scope.get_data($scope.fi_type)
            alert("請點查詢重新獲取資料")
        };

        //////////////////////// delete phone start ///////////////////////////
        self.isDelete = false;
        self.delType = "end";
        self.delPhone = "";
        $scope.$watch('ctrl.delPhone', function(newValue, oldValue) {
            self.delPhone = newValue.replace(/[^\d]+/g,'')
        },true);

        self.sendDelete = function () {
            var delPhone = self.delPhone;

            switch (self.delType) {
                case "start":
                    delPhone = delPhone + "%"
                    break;
                case "end":
                    delPhone = "%" + delPhone
                    break;
                case "match":

                    break;
            }

            console.log(delPhone);

            $http.get(ajax_host+"del",{
                params: {
                    delPhone: delPhone,
                }
            }).success(function(res){
                if (res.ststus === 0 )
                    alert("已成功刪除")
                //console.log(self.card_detail);
            });
        }

        //////////////////////// delete phone end ///////////////////////////

        //////////////////////// statis change start ///////////////////////////
        $scope.send_chg_status = function ()
        {
            var phone = "", sell="", tmp_data=self.card_detail.slice(0);

            for(var i in $scope.change)
            {
                var key = $scope.change[i];
                phone += self.card_detail[key].fv_phone+",";
                sell += ($filter('date')(self.card_detail[key].fd_sell, 'yyyy-MM-dd','+08')+",");
                tmp_data.splice(key,1);
            }
            phone = phone.substr(0,phone.length-1);
            sell = sell.substr(0,sell.length-1);
            //console.log(phone);

            $http({
                url: ajax_host+"chg_status",
                method: "GET",
                params: {
                    fv_phone: phone,
                    fd_sell: sell
                }
            }).success(function(response){
                self.card_detail = tmp_data;
                $scope.change = [];
                getPager();
                //console.log(response);
            });
        };

        $scope.change = [];

        $scope.chg_status = function (key,fi_delete) {
            //console.log(fi_delete)
            switch(fi_delete)
            {
                case "0":
                    var tmp = $scope.change.indexOf(key);
                    if(tmp>-1)
                        $scope.change.splice(tmp,1);

                    break;
                case "1":
                    $scope.change.push(key);
                    break;
            }

        };

        //////////////////////// status change end ///////////////////////////

        $scope.get_data = function(){
            $scope.change = [];
            $http.get(ajax_host+"get_data",{
                params: {
                    fi_type: $scope.fi_type,
                    fd_sell: $scope.fd_sell,
                    fv_start_phone: $scope.fv_start_phone,
                    fv_end_phone: $scope.fv_end_phone
                }
            }).success(function(response){

                self.card_detail = JSON.parse(response);
                self.source_card_detail = JSON.parse(response);
                getPager();

                //console.log(self.card_detail);
            });
        }

        $scope.get_filter = function(){
            $scope.change = [];
            if(typeof self.card_detail == "undefined")
                alert("系統異常，請聯繫管理員");
            self.card_detail = self.source_card_detail;
            self.card_detail = Enumerable.From(self.card_detail)
                .Where(function (x) {
                    var fd_sell = $filter('date')(x.fd_sell, 'yyyy-MM-dd', '+08')
                    return ($scope.fd_sell?fd_sell == $scope.fd_sell:true) &&
                        //($scope.fv_start_phone?x.fv_phone >= $scope.fv_start_phone:true) &&
                        //($scope.fv_end_phone?x.fv_phone <= $scope.fv_end_phone:true)
                        ($scope.fv_key_phone?x.fv_phone.indexOf($scope.fv_key_phone)>-1:true)
                })
                .ToArray();
            if(self.card_detail.length>0)
                getPager();
            else
                alert("沒有符合條件的資料");
        }

        $scope.exportXlsx = function()
        {
            var tmp_data = self.card_detail.map(function(x){
                x.fd_sell = $filter('date')(x.fd_sell, 'yyyy-MM-dd', '+08');
                x.fd_start = $filter('date')(x.fd_start, 'yyyy-MM-dd', '+08');
                x.fd_end = $filter('date')(x.fd_end, 'yyyy-MM-dd', '+08');
                x.fi_delete = x.fi_delete?"停用":"啟用中";
                return x;
            });

            var result = ngXlsx.writeXlsx([
                {
                    sheetName: "phone_card",
                    columnDefs: [
                        {field: "fd_sell", displayName: "出貨日"},
                        {field: "fv_phone", displayName: "門號"},
                        {field: "fv_subscr_id", displayName: "Subscr_Id"},
                        {field: "fd_start", displayName: "啟用日期"},
                        {field: "fi_can_use", displayName: "可用天數"},
                        {field: "fd_end", displayName: "到期日期"},
                        {field: "ff_total_flow", displayName: "以用流量"},
                        {field: "fi_delete", displayName: "服務狀態"}
                    ],
                    data: tmp_data
                }
            ]);
            /* the saveAs call downloads a file on the local machine */
            saveAs(new Blob([s2ab(result)],{type:"application/octet-stream"}), "phone_card.xlsx");
        }


        //////////////////////////////////////////////////////////////////////
        self.card_detail = [];

        $scope.status = [{text:'啟用中',value:'0'},{text:'停用',value:'1'}];

        $scope.page='0';
        $scope.per_page = 50;
        $scope.total_page = 0;
        $scope.pages=[];

        //獲取初始資料
        //$scope.get_data($scope.fi_type)


        $scope.setPage = function(page,add)
        {
            add = add||0;
            if(add!=0) page = Number(page)+add;
            if(page<0)page=0;
            if(page==$scope.total_page)page=$scope.total_page;

            self.page_card_detail = [];
            var start_row = page*$scope.per_page,end_row = start_row+$scope.per_page;
            for(var i =start_row; i<end_row; i++)
                if(self.card_detail[i]!=undefined)
                    self.page_card_detail.push(self.card_detail[i]);

            self.page_card_detail.forEach(function(e){
                if($scope.fi_type=='0'||$scope.fi_type=='1'){
                    e.fi_delete = e.fi_delete.toString();
                }else{
                    e.fi_delete = null;
                }

            });
            $scope.page = page.toString();
            //console.log(page)
        }


        function getPager()
        {
            $scope.total_data = self.card_detail.length;
            $scope.total_page = Math.ceil($scope.total_data/$scope.per_page);
            $scope.pages = [];
            for(var i=1; i<=$scope.total_page; i++)
            {
                $scope.pages.push(i);
            }
            //console.log($scope.pages);
            $scope.page = $scope.page>$scope.total_page-1?$scope.total_page-1:$scope.page;
            $scope.setPage($scope.page);
        }

        $scope.test = function(){
            console.log(uploader);
        }

        //console.info('uploader', uploader);
    }])
    .directive('myEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if(event.which === 13) {
                    scope.$apply(function (){
                        scope.$eval(attrs.myEnter);
                    });

                    event.preventDefault();
                }
            });
        };
    });


function s2ab(s) {
    var buf = new ArrayBuffer(s.length);
    var view = new Uint8Array(buf);
    for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
    return buf;
}