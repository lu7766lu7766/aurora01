/**
 * Created by jac on 2017/3/4.
 */

exports.dbInit = function(_conn){
    conn = _conn;
}

exports.delete = function(req, res, next){

    var delPhone = req.query.delPhone;
    //console.log(delPhone)
    //res.end(delPhone);
    var sql1 =
        "delete from t_open where fv_phone like ? ;";
    var sql2 =
        "delete from t_used_report where fv_phone like ?;";

    conn.query(sql2, [delPhone], function(error, rows, fields){
        //檢查是否有錯誤
        if(error){
            throw error;
        }
        conn.query(sql1, [delPhone], function(error, rows, fields){
            //檢查是否有錯誤
            if(error){
                throw error;
            }
            res.json({
                status: 0
            })

        });
    });

}