/**
 * Created by jac on 2016/4/10.
 */

exports.dbInit = function(_conn){
    conn = _conn;
}

var url = require('url');
exports.get = function(req, res, next){

    var type = req.query.fi_type || "0";
    var sell = req.query.fd_sell;
    var start_phone = req.query.fv_start_phone;
    var end_phone = req.query.fv_end_phone;

    var sql = "", sql_where = "", sql_where2 = "", a_where = [], a_where2 = [];

    switch (type){
        case "0":
            a_where.push("a.fi_delete=0");

            a_where2.push("CURDATE() <= fd_end");

            break;
        case "1":
            a_where.push("a.fi_delete=0");

            a_where2.push("CURDATE() > fd_end");

            break;
        case "2":
            a_where.push("a.fi_delete = 1");

            break;
        case "3":
            a_where.push("a.ff_total_flow > 0");
            break;
    }

    sql_where = a_where.length? (' where '+ a_where.join(' and ') + ' '): '';
    sql_where2 = a_where2.length? (' where '+ a_where2.join(' and ') + ' '): '';

    sql =
        "select fd_sell, fv_phone, fv_subscr_id, fd_start, fi_can_use, fd_end, ff_total_flow, fi_delete, fd_end>=CURDATE() as fi_over "+
        " from(" +
            "select " +
            "a.fd_sell, " +
            "a.fv_phone, " +
            "a.fv_subscr_id, " +
            //"min(b.fd_used) as fd_start, " +
            "a.fd_start," +
            "a.fv_memo as fi_can_use, " +
            //"DATE_ADD(min(b.fd_used),INTERVAL a.fv_memo DAY) as fd_end, " +
            "a.fd_end, " +
            //"ROUND(sum(b.ff_flow),2) as ff_total_flow, " +
            "a.ff_total_flow," +
            "a.fi_delete "+

            "from t_open as a "+
            //"left join t_used_report as b on a.fv_subscr_id = b.fv_subscr_id " +

            sql_where +

            //"group by a.fd_sell, a.fv_phone, fi_can_use, fi_delete "+
        ")as tmp " +

        sql_where2 +

        "order by fi_over";

    try{

    } catch(err) {
        
    }

    // res.end(sql)
    // res.json(JSON.stringify([sql]));
    var query = conn.query(
        sql, function(error, rows, fields){
            //檢查是否有錯誤
            if(error){
                throw error;
            }
            //console.log(query.sql);
            //console.log(rows);
            res.json(JSON.stringify(rows));

        });

    //console.log(req);
//res.end(JSON.stringify(req));
    //conn.end();
}
