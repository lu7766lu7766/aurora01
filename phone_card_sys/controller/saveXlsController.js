/**
 * Created by jac on 2016/4/10.
 */
require('es6-shim');
var _ = require('lodash');

exports.dbInit = function(_conn){
    conn = _conn;
}

exports.post = function(req, res){
    var xlsx = require('node-xlsx');

    var file_path = 'uploads/'+req.query.file_name;
    var obj = xlsx.parse(file_path);
    var data = obj[0].data;

    //res.end(data);
    //return ;

    var field_json = JSON.stringify(data[0]);
    var field_open_json = JSON.stringify(["門號", "Subscr_Id", "出貨日", "備註"]);
    var field_used_report_json = JSON.stringify(["門號", "Subscr_Id", "流量日", "使用秒數", "使用流量"]);

    if(field_json!=field_open_json && field_json!=field_used_report_json)
    {
        res.end("unmatch");
        console.log(data);
        return ;
    }

    var table = data[0].inArray("流量日")?"t_used_report":"t_open";
    var fields = "";
    var query;
    var pkey = [];

    //conn.connect();
    //console.log("file_path:"+file_path);
    //console.log(data);

    data.splice(0,1);

    try {
        // 欄位修改資料異動
        var GUID_POS = 1;
        //var subscr_ids = [];
        if (table == "t_used_report")
        {
            fields = "fv_phone,fv_subscr_id,fd_used,fi_use_sec,ff_flow";
            pkey = [1, 2];//pkey
        }
        else
        {
            fields = "fv_phone,fv_subscr_id,fd_sell,fv_memo";
            pkey = [1];//pkey
            //console.log(phones);
        }

        //console.log("^^");
        var a_field = fields.split(",");
        Object.keys(data).map(function(row){
            if(data[row].length==0) {
                data.splice(row, 1);
                return;
            }
            Object.keys(data[row]).map(function(cloumn){
                if(a_field[cloumn].substr(0,3)=="fd_"){
                    var add_day = parseInt(data[row][cloumn]);
                    if(add_day>10000){
                        var myDate=new Date();
                        myDate.setFullYear(1899,11,30);//month from 0
                        data[row][cloumn] = myDate.AddDay(add_day).Format("Y-m-d");
                    }
                }else if(a_field[cloumn]=="fv_memo"){
                    data[row][cloumn] = data[row][cloumn].replace(/\D+/g,"");
                }else if(a_field[cloumn]=="fv_phone" && table == "t_open") {
                    data[row][cloumn] = data[row][cloumn].length==9?"0"+data[row][cloumn]:data[row][cloumn];
                }else if(a_field[cloumn]=="fv_subscr_id") {
                    //subscr_ids.push("'" + data[row][cloumn] + "'")
                }
            });
        });

        if( table=="t_open" )
        {
            for (var i in data)
            {
                if((data[i][0] != undefined && data[i][0] != ""))
                {
                    // 欄位修改資料異動
                    var phone = data[i][0], sell = data[i][2]
                    query = conn.query('update ' + table + ' set fi_delete=1 where fv_phone=? and fd_sell<? and fi_delete=0', [phone,sell], function (err,
                                                                                                                                                      result) {
                        if (err) throw err;
                    });
                }
            }
        }

        //console.log(data);

        //console.log(data[0].length +"^^"+ pkey.length);

        var tmpq = "", tmpk = " where ", len = data[0].length;
        for(var j=0;j<len;j++)
            tmpq+="?,";
        tmpq = tmpq.substr(0,tmpq.length-1);
        //for(var j=0;j<pklen;j++)
        //    tmpk+= (a_field[j]+"=?   and ");
        //tmpk = tmpk.substr(0,tmpk.length-6);
        var a_tmpk = [];
        _.forEach(a_field, function (field, j) {
            if (pkey.indexOf(j)>-1) {
                a_tmpk.push(" " + field + "=? ");
            }
        })
        tmpk = " where " + a_tmpk.join(' and ');



        Object.keys(data).map(function(row){
            var params = [];
            var sql = "insert into " + table + " (" + fields + ") " +
                "select * from (select "+tmpq+") as tmp " +
                "where not exists(" +
                "select 1 from " + table + tmpk +
                ") limit 1;";
            Object.keys(data[row]).map(function(cloumn){
                params.push(data[row][cloumn]+"");
            });
            pkey.map(function(val){
                params.push(data[row][val]+"");
            });

            //console.log(sql);
            //console.log(params);

            query = conn.query(sql, params, function (err, result) {
                if (err) throw err;
                var sql2= "update t_open as a " +
                    "set " +
                    "a.ff_total_flow = (select sum(ff_flow) from t_used_report where a.fv_subscr_id= fv_subscr_id), " +
                    "a.fd_start = (select min(fd_used) from t_used_report where a.fv_subscr_id= fv_subscr_id), " +
                    "a.fd_end = (select DATE_ADD(min(fd_used),INTERVAL a.fv_memo DAY) from t_used_report where a.fv_subscr_id= fv_subscr_id) " +
                    "where a.fv_subscr_id ='" + data[row][GUID_POS] + "';";
                // console.log("\n" + sql2);
                conn.query(sql2, function (err, result) {
                    if (err) throw err;
                })
            });

            //console.log(query.sql);
            //console.log();
        });

        var sql3 = "delete from t_open where fi_delete='1' and ff_total_flow = 0";

        conn.query(sql3, function (err, result) {
            if (err) throw err;
        })

        const fs = require('fs');

        fs.unlink(file_path, function(err){
            if (err) throw err;
            //console.log('successfully deleted '+file_path);
        });

        res.send("");

    }catch(err){
        res.send("server error!");
    }
}