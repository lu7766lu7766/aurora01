/**
 * Created by jac on 2016/4/10.
 */

var multer  =   require('multer');
//var path = require('path');
var storage =   multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, './uploads');
    },
    filename: function (req, file, callback) {
        //console.log(file);
        var fileFormat = file.originalname.split(".");
        var filename=Date.now() + "." + fileFormat[fileFormat.length - 1];
        callback(null, filename);
    }
});
var upload = multer({ storage : storage}).single('file');

exports.upload = function (req, res, next) {
    upload(req,res,function(err) {
        if(err) {
            return res.end("Error uploading file.");
        }
        res.json(JSON.stringify({"answer":"File is uploaded","filename":req.file.filename}));
        //console.log(req.file.filename);
        //res.end("File is uploaded");
    });

}

    /////////////////////////////////////////////////

//var  multer=require('multer');
//var storage = multer.diskStorage({
//    //设置上传后文件路径，uploads文件夹会自动创建。
//    destination: function (req, file, cb) {
//        cb(null, './uploads')
//    },
//    //给上传文件重命名，获取添加后缀名
//    filename: function (req, file, cb) {
//        var fileFormat = file.originalname.split(".");
//        var filename=Date.now() + "." + fileFormat[fileFormat.length - 1];
//        callback(null, filename);
//    }
//});
////添加配置文件到muler对象。
//var upload = multer({
//    storage: storage
//});
//
////如需其他设置，请参考multer的limits,使用方法如下。
////var upload = multer({
////    storage: storage,
////    limits:{}
//// });
//
////导出对象
//module.exports = upload;