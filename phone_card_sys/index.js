var express = require('express')
    , cors = require('cors')
    , app = express();

var corsOptions = {
    origin: 'http://125.227.84.249:8080',
    methods:['GET', 'POST']
};

var mysql = require('mysql');
var conn = mysql.createConnection({
    host:'localhost',
    user:'aurora01',
    password:'auroraaurora01',
    database:'phone_sys',
    port:'33060'
});

var controller_dir = "./controller/";
var save_xls = require(controller_dir+"saveXlsController");
save_xls.dbInit(conn);
app.get('/save_xls', cors(corsOptions), save_xls.post);

var get_data = require(controller_dir+"getDataController");
get_data.dbInit(conn);
app.get('/get_data', cors(corsOptions), get_data.get);

var delPhone = require(controller_dir+"deletePhoneController");
delPhone.dbInit(conn);
app.get('/del', cors(corsOptions), delPhone.delete);

var chg_status = require(controller_dir+"chgStatusController");
chg_status.dbInit(conn);
app.get('/chg_status', cors(corsOptions), chg_status.put);

var uploader = require(controller_dir+"uploaderController");
app.options('/upload', cors(corsOptions));
app.post('/upload', cors(corsOptions),uploader.upload);

//var multer  = require('multer');
//var upload = multer({
//    dest: './uploads/',
//    rename: function (fieldname, filename) {
//        var fileFormat = filename.split(".");
//        return Date.now() + "." + fileFormat[fileFormat.length - 1];
//        //return filename.replace(/\W+/g, '-').toLowerCase() + Date.now()
//    }
//});
//app.options('/upload', cors(corsOptions));
//app.post('/upload', cors(corsOptions), upload.single('file'), function(req, res){
//    console.log(req.files);
//});


app.listen(3000, function(){
    console.log('CORS-enabled web server listening on port 3000');
});


Array.prototype.inArray = function (value)
{
    for (var i in this)
        if (this[i] == value)
            return true;
    return false;
};

Date.prototype.Format = function(fmt)
{
    var o = {
        "Y" : this.getFullYear(),
        "y+" : this.getFullYear(),
        "m" : this.getMonth()+1,                 //
        "d" : this.getDate(),                    //
        "H" : this.getHours(),                   //
        "i" : this.getMinutes(),                 //
        "s" : this.getSeconds(),                 //
        "S"  : this.getMilliseconds()             //
    };

    for(var k in o)
        if(new RegExp("("+ k +")").test(fmt))
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ?
                    o[k].toString().length==1?"0"+o[k]:o[k] :
                    o[k].toString().substr(o[k].toString().length-RegExp.$1.length));
    return fmt;
};

Date.prototype.AddDay = function(day)
{
    return new Date(this.getTime() + 1000*60*60*24*day);
};
