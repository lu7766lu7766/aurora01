<?php
ini_set("display_errors", "On");
error_reporting(E_ALL);
date_default_timezone_set("Asia/Taipei");
if ( !empty( $_FILES ) ) {

    $tempPath = $_FILES[ 'file' ][ 'tmp_name' ];

    @mkdir("uploads");

    $uploadPath = get_filename($_FILES[ 'file' ][ 'name' ],"0");

    $is_upload = move_uploaded_file( $tempPath, $uploadPath["filedir"].$uploadPath["filename"] );

    if($is_upload)
    {
        $answer = array( 'answer' => 'File transfer completed ','filename' => $uploadPath["filename"] );
    }
    else
    {
        $answer = array( 'answer' => 'upload faild! ' );
    }
    $json = json_encode( $answer );
    echo $json;

} else {

    echo 'No files';

}

function get_filename($file,$prefix)
{
    $filedir = dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR;
    $tmp = explode(".",$file);
    $filename =  date("YmdHis",time())."_{$prefix}.".end($tmp);
    if(file_exists($filedir.$filename))
    {
        return get_filename($file,++$prefix);
    }
    return array("filedir"=>$filedir,"filename"=>$filename);
}

?>