<?php
session_start();
if(empty($_SESSION['admin']['login_user']) || !isset($_POST["query_type"]))header("Location:index.php");

require_once "library/dba.php";
$dba=new dba();
require_once "library/class_pager2.php";
$pager = new Pager();

$admin_user	= $_SESSION["admin"]["login_user"];
$admin_id	= $_SESSION["admin"]["login_fi_id"];
$db_name	= "t_advertisement";

$img_path = "images/advertisement/";
$file_size_limit = 1024*1024*1;//1M   unit:byte
switch($_POST["query_type"]){
	case "get_dist":
		$city = $_POST["city"];
		$result = $dba->getAll("select fv_dist from t_area where fv_city = '$city'");
		echo "<table style='border:1px solid #333'>";
		echo "<tr><td><label><input type='checkbox' value='$city' class='all' name='edit_area_list'>$city</label></td><td></td><td></td></tr>";
		
		foreach($result as $key=>$val){
			$dist = $val["fv_dist"];
			if($key%3==0){echo "<tr>";}
			echo "<td><label><input type='checkbox' value='$dist' class='dist' name='edit_area_list'>$dist</label></td>";
			if($key%3==2){echo "</tr>";}
			$i = $key;
		}
		echo $i%3==0?"<td></td><td></td>":"";
		echo $i%3==1?"<td></td>":"";
		echo "</table>";
		break;
	case "get_search":
		$keyword = $_POST["keyword"];
		$result = $dba->getAll("select fi_id,fv_brand_name from t_brand where fv_brand_name like '%{$keyword}%'");
		//$result = $dba->getAll("select fi_id,fv_brand_name from t_brand");
		if($result)
			foreach($result as $key=>$val){
				$result2[$key]["id"] = $val["fi_id"];
				$result2[$key]["text"] = $val["fv_brand_name"];
			}
		else
			die(
				json_encode(
				array( "count"=>0 )
				)
			);
		
		die(
			json_encode(
				array( "count"=>count($result),
					   "result"=>$result2 )
					   //"result"=>"cc" )
			)
		);
		break;
	case "advertisement_add":
		$brand	= $_POST["brand"];
		$brand_name	= $_POST["brand_name"];
		//$result = $dba->getAll("select max(fi_weights)+1 as fi_weights from $db_name where fi_brand='$brand' ");
		//$weights = is_numeric($result[0]["fi_weights"])?$result[0]["fi_weights"]:1;
		$sql = "insert into $db_name (`fi_brand`,`fv_brand_name`) values('$brand','$brand_name')";
		$result = $dba->query($sql);
        if($result)
        	die("success");
        else
        	die($sql);
    break;
    case "advertisement_edit":
    	
    	$fi_id			= $_POST["fi_id"];
    	$type			= $_POST["type"];
    	$describe		= $_POST["describe"];
    	$brand 			= $_POST["brand"];
    	$brand_name		= $_POST["brand_name"];
    	$url			= $_POST["url"];
    	$area_list		= $_POST["area_list"];
    	$date_active	= $_POST["date_active"];
    	$start_date		= $_POST["start_date"];
    	$end_date		= $_POST["end_date"];
    	$weights		= $_POST["weights"];
    	$active			= $_POST["active"];
    	$phone			= $_POST["phone"];
		$a_remove_file	= $_POST["a_remove_file"];
    	//圖片驗證
		$len = count($_FILES['file']['name']);
		for($i=0; $i<$len; $i++) {
			if( $_FILES['file']['error'][$i] > 0 ) {
				switch($_FILES['file']['error'][$i]){
                    case 1:
                        $error .= $_FILES['file']['name'][$i]."上傳超過伺服器規定大小<br>";break;
                    case 2:
                        $error .= $_FILES['file']['name'][$i]."上傳超過前台表單規定大小<br>";break;
                    case 3:
                        $error .= $_FILES['file']['name'][$i]."文件上傳不完整<br>";break;
                }
			}else{
				if( $_FILES['file']['size'][$i] > $file_size_limit ){
					$error .= " ";
					break;
				}
			}
		}
		if($error!=""){die($error);}
		//圖片上傳+改名
		for($i=0; $i<$len; $i++) {
			$t = time();
			list($width,$height,,) = getimagesize($_FILES['file']['tmp_name'][$i]);
			$fn = $_FILES['file']['name'][$i];
			$ext = end(explode('.', $fn));
			$dt = date("ymdHis",$t);
			$file_name =  "{$dt}_{$i}_{$width}x{$height}_{$fi_id}.{$ext}";
			move_uploaded_file($_FILES['file']['tmp_name'][$i], $img_path.$file_name);
			chmod($img_path.$file_name,0755);
			//因圖片改名，存原檔名歸類用
			//$file_source_name[] = $_FILES['file']['name'][$i];
			//$file_modify_name[] = $file_name;
		}
		//圖片刪除
		if(is_array($a_remove_file))
			foreach($a_remove_file as $remove_file)
				@unlink($img_path . $remove_file);
		$result = $dba->getAll("select fv_ad from $db_name where where fi_id = '$fi_id'");
		if($len>0)
		{
			$where_image = "fv_ad = '$file_name',";
			if($result[0]["fv_ad"]!="")
			{
				@unlink($img_path.$result[0]["fv_ad"]);
			}
		}
		if($brand==0){
			$where_brand = "fv_brand_name = '$brand_name',";
		}else{
			$where_brand = "fi_brand = '$brand',
							fv_brand_name = '',";
		}
		$sql = "update $db_name set
							fi_type = '$type',
    						fv_describe = '$describe',
    						$where_brand
    						$where_image
				    		fv_url='$url',
				    		fv_area_list='$area_list',
				    		fi_date_active='$date_active',
				    		fd_start_date='$start_date',
				    		fd_end_date='$end_date',
    						fi_active='$active',
    						fv_phone='$phone',
    						fi_weights='$weights'
    					where fi_id='$fi_id'";
        $result = $dba->query($sql);
        if($result)
        	die("success");
        else
        	die($sql);
    break;
    case "advertisement_del":
    	$fi_id		= $_POST["fi_id"];
		$data	= $dba->getAll("select fv_ad from $db_name where fi_id='$fi_id'");
		@unlink($img_path . $data[0]["fv_ad"]);
		$sql = "update $db_name set fi_delete='1' where fi_id='$fi_id'";
        $result = $dba->query($sql);
        if($result)
        	die("success");
        else
        	die($sql);
    break;
}