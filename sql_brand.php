<?php
session_start();
if(empty($_SESSION['admin']['login_user']) || !isset($_POST["query_type"]))header("Location:index.php");

require_once "library/dba.php";
$dba=new dba();
require_once "library/class_pager2.php";
$pager = new Pager();

$admin_user	= $_SESSION["admin"]["login_user"];
$admin_id	= $_SESSION["admin"]["login_fi_id"];
$db_name	= "t_brand";

$logo_path = "images/brand_logo/";
$menu_path = "images/brand_menu/";
$img_path = "";
$file_size_limit = 1024*1024*1;//1M   unit:byte
switch($_POST["query_type"]){

	case "get_info":
		$type 				= $_POST["fi_id"];
		$keyword			= $_POST["keyword"];
		$permissions2del	= $_POST["permissions2del"];
		$permissions2edit	= $_POST["permissions2edit"];

		if($keyword!=""){
			$key_where = " and fv_brand_name like '%$keyword%' ";
		}

		$sql = "select `fi_id`
					,`fv_brand_name`
					,`fv_logo`
					,`fi_active`
					,`fi_weights`
					,`fv_menu` 
				from `t_brand` 
				where `fi_type`='$type' 
					$key_where 
				order by fi_weights desc";
		
		echo "<br><br>";
		$result = $pager->query($sql);
		$pager->display();
		echo "<br>";
		echo "
		<table class='table-h' id='list_panel' style='margin-top:10px'>
            <tr>
            	<td>ID</td>
            	<td>品牌名稱</td>
            	<td>LOGO</td>
            	<td>MENU</td>
            	<td>狀態</td>
            	<td>權重</td>
            	<td>修改</td>
            	<td>刪除</td>
            	<td>合併</td>
            </tr>";
        if(is_array($result))
        {
	        foreach($result as $per_data)
			{
				$tmp_id			= $per_data["fi_id"];
	            $tmp_type		= $type;
	            $tmp_brand_name	= $per_data["fv_brand_name"];
	            $tmp_logo		= $per_data["fv_logo"];
	            $tmp_menu		= $per_data["fv_menu"];
	            $tmp_menu_cht	= $tmp_menu==""?"":"有";
	            $tmp_active		= $per_data["fi_active"];
	            $tmp_active_cht	= $tmp_active==1?"啟用":"停用";
	            $tmp_weights	= $per_data["fi_weights"];
	            
				echo "  
				<tr class='flash'>
					<td>{$tmp_id}</td>
					<td>{$tmp_brand_name}</td><td>";
				if( file_exists($logo_path.$tmp_logo)&&!is_dir($logo_path.$tmp_logo) )
				echo "
		            <img src='{$logo_path}{$tmp_logo}' title='$tmp_logo' style='max-height:100px;max-width:600px'/>";
		        echo "
		        	</td>
		        	<td>{$tmp_menu_cht}</td>
	                <td>{$tmp_active_cht}</td>
	                <td>{$tmp_weights}</td>
	                <td>";
	            if($permissions2edit)
	                echo "
	                    <input type='button' fi_id='$tmp_id' value='編輯' data-open-dialog='編輯視窗'>";
	            echo "
	            	</td>";
	            echo "
	            	<td>";
	            if($permissions2del)
	                echo "
	                    <input type='button' fi_id='$tmp_id' value='刪除' class='del_btn'>";
	            
	            echo "
	            	</td>";
	            echo "
	            	<td>";
	            if($permissions2edit)
	                echo "
	                    <input type='button' fi_id='$tmp_id' value='合併' class='combine'>";
	            //end img
	            echo "	<input type='hidden' id='data_{$tmp_id}' edit_type='$type' edit_brand_name='$tmp_brand_name' edit_active='$tmp_active' edit_weights='$tmp_weights'>";
	            
	            $js_start = "
	            	<script>
	            		$(document).ready(function(){";
	            $js_end = "
	            		});
	            	</script>";
	            echo $js_start;
	            if( file_exists($logo_path.$tmp_logo)&&!is_dir($logo_path.$tmp_logo) )
	            echo "		$('#data_{$tmp_id}').data('brand_logo_preview',
		            			\"<img src='{$logo_path}{$tmp_logo}' title='$tmp_logo' style='max-height:100px;' />\"
		            		);";
	            if( file_exists($menu_path.$tmp_menu)&&!is_dir($menu_path.$tmp_menu) )
	            echo "		$('#data_{$tmp_id}').data('brand_menu_preview',
		            			\"<img src='{$menu_path}{$tmp_menu}' title='$tmp_menu' style='max-height:100px;' />\"
		            		);";
				echo $js_end;

	            echo "
	                </td>
	            </tr>";
			}
        }
		echo "</table>";
		echo "<br>";
		$pager->display();
		die();
	break;
	case "brand_add":
		$new_type	= $_POST["type"];
		$new_name	= $_POST["name"];
		$result = $dba->getAll("select max(fi_weights)+1 as fi_weights from $db_name where fi_type='$type' ");
		$new_weights = is_numeric($result[0]["fi_weights"])?$result[0]["fi_weights"]:1;
		$sql = "insert into $db_name (`fv_brand_name`,`fi_type`,`fi_weights`) values('$new_name','$new_type','$new_weights')";
		$result = $dba->query($sql);
        if($result)
        	die("success");
        else
        	die();
    break;
    case "brand_edit":
    	
    	$fi_id				= $_POST["fi_id"];
    	$new_type			= $_POST["type"];
    	$new_name			= $_POST["name"];
    	$new_weights		= $_POST["weights"];
    	$new_active			= $_POST["active"];
		$a_remove_file		= $_POST["a_remove_file"];
		$file_path			= $_POST["file_path"];
    	//圖片驗證
		$len = count($_FILES['file']['name']);
		for($i=0; $i<$len; $i++) {
			if( $_FILES['file']['error'][$i] > 0 ) {
				switch($_FILES['file']['error'][$i]){
                    case 1:
                        $error .= $_FILES['file']['name'][$i]."上傳超過伺服器規定大小<br>";break;
                    case 2:
                        $error .= $_FILES['file']['name'][$i]."上傳超過前台表單規定大小<br>";break;
                    case 3:
                        $error .= $_FILES['file']['name'][$i]."文件上傳不完整<br>";break;
                }
			}else{
				if( $_FILES['file']['size'][$i] > $file_size_limit ){
					$error .= " ";
					break;
				}
			}
		}
		if($error!=""){die($error);}
		//圖片上傳+改名
		for($i=0; $i<$len; $i++) {
			$t = time();
			list($width,$height,,) = getimagesize($_FILES['file']['tmp_name'][$i]);
			$fn = $_FILES['file']['name'][$i];
			$ext = end(explode('.', $fn));
			$dt = date("ymdHis",$t);
			$file_name =  "{$dt}_{$i}_{$width}x{$height}_{$fi_id}.{$ext}";
			if($file_path[$i]=="menu"){
				$img_path = $menu_path;
				$fv_menu = "fv_menu = '$file_name',";
			}else{
				$img_path = $logo_path;
				$fv_logo = "fv_logo = '$file_name',";
			}
			move_uploaded_file($_FILES['file']['tmp_name'][$i], $img_path.$file_name);
			chmod($logo_path.$file_name,0755);
			//因圖片改名，存原檔名歸類用
			//$file_source_name[] = $_FILES['file']['name'][$i];
			//$file_modify_name[] = $file_name;
		}
		//圖片刪除
		if(is_array($a_remove_file))
			foreach($a_remove_file as $remove_file){
				if(strpos($remove_file["path"],$logo_path)!==false){
					@unlink($logo_path . $remove_file["title"]);
					$fv_logo = "fv_logo = '',";
				}else if(strpos($remove_file["path"],$menu_path)!==false){
					@unlink($menu_path . $remove_file["title"]);
					$fv_menu = "fv_menu = '',";
				}
			}
		$sql = "update $db_name set
        						fi_type = '$new_type',
        						fv_brand_name = '$new_name',
        						$fv_logo
        						$fv_menu
        						fi_active='$new_active',
        						fi_weights='$new_weights'
        					 where fi_id='$fi_id'";
        $result = $dba->query($sql);
        if($result)
        	die("success");
        else
        	die();
    break;
    case "brand_del":
    	$fi_id		= $_POST["fi_id"];
		$data	= $dba->getAll("select fv_logo,fv_menu from $db_name where fi_id='$fi_id'");
		@unlink($logo_path . $data[0]["fv_logo"]);
		@unlink($menu_path . $data[0]["fv_menu"]);
		$sql = "update $db_name set fi_delete = '1' where fi_id='$fi_id'";
        $result = $dba->query($sql);
        if($result)
        	die("success");
        else
        	die();
    break;
    case "combine":
    	$source_id = $_POST["source_id"];
    	$target_id = $_POST["target_id"];

		$source_data = $dba->getAll("select fv_logo,fv_menu from $db_name where fi_id='$source_id'");
		$target_data = $dba->getAll("select fv_logo,fv_menu from $db_name where fi_id='$target_id'");

		$source_logo = $source_data[0]["fv_logo"];
		$source_menu = $source_data[0]["fv_menu"];

		$target_logo = $target_data[0]["fv_logo"];
		$target_menu = $target_data[0]["fv_menu"];

		if($source_logo!="")
		{
			if($target_logo=="")
			{
				$dba->query("update $db_name set fv_logo='$source_logo' where fi_id='$source_id'");
			}
			else
			{
				@unlink($logo_path . $source_logo);
			}
		}

		if($source_menu!="")
		{
			if($target_menu=="")
			{
				$dba->query("update $db_name set fv_menu='$source_menu' where fi_id='$source_id'");
			}
			else
			{
				@unlink($menu_path . $source_menu);
			}
		}

		//$dba->query("update t_history set fi_store_id='$target_id' where fi_store_id='$source_id'");
		$result1 = $dba->query("update t_store set fi_brand='$target_id' where fi_brand='$source_id'");
		$result2 = $dba->query("update t_advertisement set fi_brand='$target_id' where fi_brand='$source_id'");
		$result3 = $dba->query("delete from $db_name where fi_id='$source_id'");

		if($result1 && $result2 &&$result3 )
        	die("success");
    break;
}