<?php
	session_start();
if(empty($_SESSION['admin']['login_user']) || !isset($_POST["query_type"]))header("Location:index.php");
require_once "library/dba.php";
$dba = new dba();
require_once "library/library.php";
$db_name = "t_store";
switch($_POST["query_type"])
{
    case "getLatLng":
    	$fi_id			= $_POST["fi_id"];
		$address		= $_POST["address"];
    	$lat			= $_POST["lat"];
		$lng			= $_POST["lng"];
		if( !$lat || !$lng )
		{
			$r = Library::getLatLng($address);
			$lat = $r["lat"];
			$lng = $r["lng"];
			if( !$lat || !$lng )
			{
				die("編號:{$fi_id} 經緯度抓取失敗,請連線管理員或確認地址正確性！");
			}
		}
		
		$sql = "update $db_name set
						ff_latitude='$lat'
						, ff_longitude='$lng'
						, fp_geo=GeomFromText( 'POINT(".$lat.' '.$lng.")' ) 
				where fi_id='$fi_id'";
		$result = $dba->query($sql);
		if($result){
			
			die("編號:{$fi_id} 經緯度抓取成功！");
		}
        break;
}