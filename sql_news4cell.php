<?php
session_start();
if(empty($_SESSION['admin']['login_user']) || !isset($_POST["query_type"]))header("Location:index.php");

require_once "library/dba.php";
$dba=new dba();

$admin_user	= $_SESSION["admin"]["login_user"];
$admin_id	= $_SESSION["admin"]["login_fi_id"];

$subject = "news4cell";
$db_name	= "t_{$subject}";
$img_path = "images/".$subject."/";
$file_size_limit = 1024*1024*1;//1M   unit:byte

$a_field = json_decode('["query_type","fi_id","file_path","a_remove_file","active","date_active","start","end","weights"]');

foreach($a_field as $field){
    ${$field} = $_POST[$field];
}
$date_active = $date_active == "true"? 1: 0;
$active = $active == "false"? 0: 1;

$len = count($_FILES['file']['name']);
for($i=0; $i<$len; $i++) {
    if( $_FILES['file']['error'][$i] > 0 ) {
        switch($_FILES['file']['error'][$i]){
            case 1:
                $error .= $_FILES['file']['name'][$i]."上傳超過伺服器規定大小<br>";break;
            case 2:
                $error .= $_FILES['file']['name'][$i]."上傳超過前台表單規定大小<br>";break;
            case 3:
                $error .= $_FILES['file']['name'][$i]."文件上傳不完整<br>";break;
        }
    }else{
        if( $_FILES['file']['size'][$i] > $file_size_limit ){
            $error .= " ";
            break;
        }
    }
}
if($error!=""){die($error);}
//圖片上傳+改名
for($i=0; $i<$len; $i++) {
    $t = time();
    list($width,$height,,) = getimagesize($_FILES['file']['tmp_name'][$i]);
    $fn = $_FILES['file']['name'][$i];
    $ext = end(explode('.', $fn));
    $dt = date("ymdHis",$t);
    $file_name =  "{$dt}_{$i}_{$width}x{$height}_{$fi_id}.{$ext}";
    $img_path = $file_path[$i];

    if(strpos($query_type,"add")!==false){
        $fv_images = "$file_name";
    }else if(strpos($query_type,"edit")!==false){
        $fv_images = "fv_images = '$file_name',";
    }

    move_uploaded_file($_FILES['file']['tmp_name'][$i], $img_path.$file_name);
    chmod($logo_path.$file_name,0755);
}
//圖片刪除
if(is_array($a_remove_file)) {
    foreach ($a_remove_file as $remove_file) {
        if (is_file($img_path.$remove_file["title"])) {
            @unlink($img_path.$remove_file["title"]);
            $fv_images = "fv_images = '',";
        }
    }
}

switch($query_type) {

    case "{$subject}_add":

		$sql = "insert into $db_name
                      (`fi_date_active`,`fd_start`,`fd_end`,`fi_active`,`fi_weights`,`fv_images`)
                values('$date_active','$start','$end','$active','$weight', '$fv_images')";

        $result = $dba->query($sql);

        if($result)
        	die("success");
        else
        	die($sql."^".$query_type);
    break;
    case "{$subject}_edit":

		$sql = "update $db_name
                    set
                        fi_date_active = '$date_active',
                        fd_start = '$start',
                        fd_end = '$end',
                        $fv_images
                        fi_active='$active',
                        fi_weights='$weights'
                    where fi_id='$fi_id'";
        $result = $dba->query($sql);
        if($result)
        	die("success");
        else
        	die();
    break;
    case "{$subject}_del":
		$data	= $dba->getAll("select fv_images from $db_name where fi_id='$fi_id'");
		@unlink($img_path . $data[0]["fv_images"]);
		$sql = "update $db_name set fi_delete = '1' where fi_id='$fi_id'";
        $result = $dba->query($sql);
        if($result)
        	die("success");
        else
        	die($sql);
    break;

}