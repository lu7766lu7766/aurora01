<?php
session_start();
if(empty($_SESSION['admin']['login_user']) || !isset($_POST["query_type"]))header("Location:index.php");

require_once "library/dba.php";
require_once "library/class_pager2.php";
require_once "library/library.php";
$pager = new Pager();
$dba = new dba();
$admin_user	= $_SESSION["admin"]["login_user"];
$admin_id	= $_SESSION["admin"]["login_fi_id"];
$db_name	= "t_store";

switch($_POST["query_type"]){

	case "get_brand":
		$type	= $_POST["type"];
		$sql = "select fi_id,fv_brand_name from t_brand where fi_type='$type'";
		$result = $dba->getAll($sql);
		foreach($result as $data){
			echo "<option value='".$data["fi_id"]."'>".$data["fv_brand_name"]."</option>";
		}
		die();
	break;
	case "get_info":
		$once_switch=0;
		$type 				= $_POST["type"];
		$search 			= $_POST["search"];
		$sql = "select 1 from t_store where fi_id = '$search'";
		$r = $dba->getAll($sql);
		if(count($r)>0){
			$where_val = " and fi_id = '$search' ";
			$once_switch=1;
		}else{
			$where_val = $search==""?" and `fi_type`='$type' ":" and `fi_type`='$type' and (fv_address like '%$search%' or fv_phone  like '%$search%' or fv_source_phone  like '%$search%' or fv_brand_name  like '%$search%' or fv_subname  like '%$search%') ";
		}
		$permissions2del	= $_POST["permissions2del"];
		$permissions2edit	= $_POST["permissions2edit"];
		$sql = "select `fi_id`,`fv_address`,`fv_brand_name`,`fv_subname`,`fv_phone`,`fv_source_phone`,`fi_brand`,`ff_latitude`,`ff_longitude`,`fi_level`,`fi_special_price`,`fv_special_info`,`fi_type`,`fi_voice_ad`,`fi_msg_ad`
				from `v_store` 
				where  fi_delete='0' $where_val ";
		
		$result = $pager->query($sql);
		$pager->display();
		echo "<br>";
		echo "
		<table class='table-h list_panel' id='list_panel' style='margin-top:10px'>
            <tr>
            	<td>編號</td>
            	<td>品牌名稱</td>
            	<td>子店名</td>
            	<td>電話(55104)</td>
            	<td>地址</td>
            	<td>修改</td>
            	<td>刪除</td>
            </tr>";
        if(is_array($result))
        {
	        foreach($result as $per_data)
			{
				$tmp_id			= $per_data["fi_id"];
				$tmp_brand		= $per_data["fi_brand"];
				$tmp_brand_name	= $per_data["fv_brand_name"];
	            $tmp_subname	= $per_data["fv_subname"];
	            $tmp_address	= $per_data["fv_address"];
	            $tmp_phone		= $per_data["fv_phone"];
	            $tmp_source_phone= $per_data["fv_source_phone"];
	            $tmp_lat		= $per_data["ff_latitude"]==0?"":$per_data["ff_latitude"];
	            $tmp_lng		= $per_data["ff_longitude"]==0?"":$per_data["ff_longitude"];
	            $tmp_level		= $per_data["fi_level"];
	            $tmp_special_price	= $per_data["fi_special_price"];
	            $special_info	= $per_data["fv_special_info"];
	            $tmp_type		= $per_data["fi_type"];
	            $voice_ad		= $per_data["fi_voice_ad"];
	            $msg_ad			= $per_data["fi_msg_ad"];
	            
				echo "  
				<tr class='flash'>
					<td>{$tmp_id}</td>
					<td>{$tmp_brand_name}</td>
					<td>{$tmp_subname}</td>
	                <td>{$tmp_phone}</td>
	                <td>{$tmp_address}</td>
	                <td>";
	            if($permissions2edit)
	                echo "
	                    <input type='button' fi_id='$tmp_id' value='編輯' data-open-dialog='編輯視窗'>";
	            echo "
	            	</td><td>";
	            if($permissions2del)
	                echo "
	                    <input type='button' fi_id='$tmp_id' value='刪除' class='del_btn'>";
	            
	            //end img
	            echo "	<input type='hidden' id='data_{$tmp_id}' sphone='$tmp_source_phone' saddress='$tmp_address' 
	            		lat='$tmp_lat' lng='$tmp_lng'
	            		edit_brand='$tmp_brand' 
	            		edit_subname='$tmp_subname' 
	            		edit_address='$tmp_address' 
	            		edit_phone='$tmp_phone' 
	            		edit_source_phone='$tmp_source_phone' 
	            		edit_level='$tmp_level'
	            		edit_special_price='$tmp_special_price' 
	            		edit_special_info='$special_info'
	            		edit_voice_ad='$voice_ad' 
	            		edit_msg_ad='$msg_ad'>";
	            echo "
	                </td>
	            </tr>";
	            if($once_switch){
	            	$sql = "select fi_id,fv_brand_name from t_brand where fi_type='$tmp_type'";
					$result = $dba->getAll($sql);
					$return = "";
					foreach($result as $data){
						$return.= "<option value='".$data["fi_id"]."'>".$data["fv_brand_name"]."</option>";
					}
	            	echo "
	            	<script>
	            	$(document).ready(function(){
	            		$('#edit_brand').html(\"{$return}\");
	            	});
	            	</script>
	            	";
	            }
			}
        }
        echo "</table>";
        echo "<br>";
        $pager->display();
		die();
	break;
	case "store_add":
		$new_brand		= $_POST["brand"];
		$new_subname	= $_POST["subname"];
		$new_phone		= $_POST["phone"];
		$new_source_phone= $_POST["source_phone"];
		$new_address	= $_POST["address"];
		$new_level		= $_POST["level"];
		$lat			= $_POST["lat"];
		$lng			= $_POST["lng"];
		$data = $dba->getAll("select 1 from t_store where fv_phone='$new_source_phone'");
		$len = count($data);
		if($len>0)
			die("over");
		if($lat==0||$lng==0){
			$r = Library::getLatLng($new_address);
			$lat = $r["lat"];
			$lng = $r["lng"];
		}
		$sql = "insert into $db_name 
						(`fi_brand`,`fv_subname`,`fv_phone`,`fv_source_phone`,
						`fv_address`,`ft_create`,`ff_latitude`,`ff_longitude`,`fp_geo`,
						`fi_level`) 
				values('$new_brand','$new_subname','$new_phone','$new_source_phone',
						'$new_address',now(),'$lat','$lng', GeomFromText( 'POINT(".$lat.' '.$lng.")'),
						'$new_level')";
		$result = $dba->query($sql);
        if($result)
        	die("success");
        else
        	die($sql);
    break;
    case "store_edit":
    	
    	$fi_id			= $_POST["fi_id"];
    	$brand			= $_POST["brand"];
    	$subname		= $_POST["subname"];
    	$phone			= $_POST["phone"];
    	$source_phone	= $_POST["source_phone"];
    	$sphone			= $_POST["sphone"];
    	$address		= strtr($_POST["address"],array(" "=>""));
    	$saddress		= strtr($_POST["saddress"],array(" "=>""));
    	$lat			= $_POST["lat"];
		$lng			= $_POST["lng"];
		$level			= $_POST["level"];
		$special_price	= $_POST["special_price"];
		$special_info	= $_POST["special_info"];
		$voice_ad		= $_POST["voice_ad"];
		$msg_ad			= $_POST["msg_ad"];
		$update_val		= "";
		
		if($sphone!=$source_phone)
		{
			$data = $dba->getAll("select 1 from t_store where fv_phone='$source_phone'");
			$len = count($data);
			if($len>0)
				die("over");
		}
		//if($saddress!=$address)
		//{
			if( !$lat || !$lng ){
				$r = Library::getLatLng($address);
				$lat = $r["lat"];
				$lng = $r["lng"];
			}
		//}
		$update_val = " , ff_latitude='$lat'
						, ff_longitude='$lng'
						, fp_geo=GeomFromText( 'POINT(".$lat.' '.$lng.")' ) ";
		
		$sql = "update $db_name set
						fi_brand = '$brand',
						fv_subname = '$subname',
						fv_phone='$phone',
						fv_source_phone='$source_phone',
						fv_address='$address',
						fi_level='$level',
						fi_special_price='$special_price',
						fv_special_info='$special_info',
						fi_voice_ad='$voice_ad',
						fi_msg_ad='$msg_ad',
						ft_update=now()
						$update_val
					 where fi_id='$fi_id'";
        $result = $dba->query($sql);
        if($result)
        	die("success");
        else
        	die($sql);
    break;
    case "store_del":
    	$fi_id		= $_POST["fi_id"];
    	$t = time();
    	$date=date("Y-m-d",$t);
		$sql = "update $db_name set fi_delete='1',fd_del='$date',fv_del_user='$admin_user' where fi_id='$fi_id'";
        $result = $dba->query($sql);
        if($result)
        	die("success");
        else
        	die($sql);
    break;
}