<?php
// ini_set("display_errors", "On");
// error_reporting(E_ALL);
session_start();
if(empty($_SESSION['admin']['login_user']) || !isset($_POST["query_type"]))header("Location:index.php");

require_once "library/dba.php";
$dba=new dba();

$admin_user	= $_SESSION["admin"]["login_user"];
$admin_id	= $_SESSION["admin"]["login_fi_id"];

$subject = "store_pending";
$db_name	= "t_{$subject}";

switch($_POST["query_type"]){
	case "get_list"://取得清單
		$type		= $_POST["type"];
		$permissions2add = $_POST["permissions2add"];
		switch($type){
			case 0://未審
				$where_condiction = " where fi_pending = '0' ";
				break;
			case 1://已審未輸出
				$where_condiction = " where fi_pending = '1' and fi_export = '0' ";
				break;
			case 2://已輸出
			$where_condiction = " where fi_pending = '1' and fi_export = '1' ";
				break;
		}
		$sql = "select 	fi_id
        				,fv_brand_name
        			  	,fv_subname
        				,fv_address
        				,fv_store_phone
                        ,fv_intro_phone
        		from {$db_name}
        		$where_condiction";
		$result = $dba->getAll($sql);
		echo "<br>";
		echo "
		<table class='table-h' id='list_panel' style='margin-top:10px'>
            <tr>
            	<td>ID</td>
            	<td>品牌名稱</td>
            	<td>子店名</td>
            	<td>住址</td>
            	<td>電話</td>
            	<td>審核</td>
            </tr>";
        if(is_array($result))
        {
	        foreach($result as $per_data)
			{
				$id			= $per_data["fi_id"];
	            $brand_name	= $per_data["fv_brand_name"];
	            $subname		= $per_data["fv_subname"];
	            $address		= $per_data["fv_address"];
	            $store_phone	= $per_data["fv_store_phone"];
	            $intro_phone	= $per_data["fv_intro_phone"];
	            
				echo "  
				<tr class='flash'>
					<td>{$id}</td>
					<td>{$brand_name}</td>
					<td>{$subname}</td>
		        	<td>{$address}</td>
	                <td>{$store_phone}</td>
	                <td>";
	            if($permissions2add)
	                echo "
	                    <input type='button' fi_id='$id' value='通過' class='pass_btn'>
	                    <input type='button' fi_id='$id' value='刪除' class='del_btn'>";
	            
	            //end img
	            echo "	<input type='hidden' id='data_{$id}' intro_phone='$intro_phone'>
	                </td>
	            </tr>";
			}
        }
		echo "</table>";
		die();
		break;
	case "pending_pass":
    	$fi_id			= $_POST["fi_id"];
    	$intro_phone	= $_POST["intro_phone"];
    	
		$sql = "update {$db_name} set fi_pending='1' where fi_id = '$fi_id';";
        $result = $dba->query($sql);

        if($result)
        {
        	$dba->query("insert into t_bonus_log (fi_type,fv_phone,fi_bonus,fv_memo)
        							values ('1','$intro_phone','10','介紹 審核編號：{$fi_id} 店家');");
        	die("success");
        }
        
    break;
    case "pending_del":
    	$fi_id			= $_POST["fi_id"];
    	$intro_phone	= $_POST["intro_phone"];
    	
		$sql = "delete from  {$db_name} where fi_id = '$fi_id';";
        $result = $dba->query($sql);

        if($result)
        {
        	die("success");
        }
        
    break;
    case "export":

    	require_once "model/store_pending_export.php";
    	$export = new StorePending();
    	$result = $export->db2xls();
    	if(!$result){
    		die("export_error");
    	}else{
    		die("success");
    	}
    break;
}