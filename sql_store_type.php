<?php
session_start();
if(empty($_SESSION['admin']['login_user']) || !isset($_POST["query_type"]))header("Location:index.php");

require_once "library/dba.php";
$dba=new dba();

$admin_user	= $_SESSION["admin"]["login_user"];
$admin_id	= $_SESSION["admin"]["login_fi_id"];
$db_name	= "t_store_type";

$img_path = "images/store_type/";
$file_size_limit = 1024*1024*1;//1M   unit:byte
switch($_POST["query_type"]){

	case "store_type_add":
		$new_name	= $_POST["name"];
		$result = $dba->getAll("select max(fi_weights)+1 as fi_weights from $db_name ");
		$new_weights = is_numeric($result[0]["fi_weights"])?$result[0]["fi_weights"]:1;
		$result = $dba->query("insert into $db_name (`fv_name`,`fi_weights`) values('$new_name','$new_weights')");
        if($result)
        	die("success");
    break;
    case "store_type_edit":
    	
    	$fi_id				= $_POST["fi_id"];
    	$new_name			= $_POST["name"];
    	$new_active			= $_POST["active"];
    	$new_weights		= $_POST["weights"];
		$a_remove_file		= $_POST["a_remove_file"];
    	//圖片驗證
		$len = count($_FILES['file']['name']);
		for($i=0; $i<$len; $i++) {
			if( $_FILES['file']['error'][$i] > 0 ) {
				switch($_FILES['file']['error'][$i]){
                    case 1:
                        $error .= $_FILES['file']['name'][$i]."上傳超過伺服器規定大小<br>";break;
                    case 2:
                        $error .= $_FILES['file']['name'][$i]."上傳超過前台表單規定大小<br>";break;
                    case 3:
                        $error .= $_FILES['file']['name'][$i]."文件上傳不完整<br>";break;
                }
			}else{
				if( $_FILES['file']['size'][$i] > $file_size_limit ){
					$error .= " ";
					break;
				}
			}
		}
		if($error!=""){die($error);}
		//圖片上傳+改名
		for($i=0; $i<$len; $i++) {
			$mtime = explode(" ", microtime());
			list($width,$height,,) = getimagesize($_FILES['file']['tmp_name'][$i]);
			$ext = end(explode('.', $_FILES['file']['name'][$i]));
			$file_name =  date("ymdHis",$mtime[1])."_{$i}_{$width}x{$height}_".$fi_id.".{$ext}";
			move_uploaded_file($_FILES['file']['tmp_name'][$i], $img_path.$file_name);
			chmod($img_path.$file_name,0755);
			//因圖片改名，存原檔名歸類用
			//$file_source_name[] = $_FILES['file']['name'][$i];
			//$file_modify_name[] = $file_name;
		}
		//圖片刪除
		if(is_array($a_remove_file))
			foreach($a_remove_file as $remove_file)
				@unlink($img_path . $remove_file);
		
		if($len>0)
			$fv_logo = "fv_logo = '$file_name',";
        $result = $dba->query("update $db_name set
        						fv_name = '$new_name',
        						$fv_logo
        						fi_active='$new_active',
        						fi_weights='$new_weights'
        					 where fi_id='$fi_id'");
        if($result)
        	die("success");
    break;
    case "store_type_del":
    	$fi_id		= $_POST["fi_id"];
		$data	= $dba->getAll("select fv_logo from $db_name where fi_id='$fi_id'");
		@unlink($img_path . $data[0]["fv_logo"]);
        $result = $dba->query("delete from $db_name where fi_id='$fi_id'");
        if($result)
        	die("success");
    break;
}